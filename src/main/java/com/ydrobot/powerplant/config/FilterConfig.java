package com.ydrobot.powerplant.config;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ydrobot.powerplant.filter.AllowFilter;
import com.ydrobot.powerplant.filter.UiLoginFilter;

@Configuration
public class FilterConfig {
	@Bean
	public Filter UiLoginFilter() {
		return new UiLoginFilter();
	}

	@Bean
	public Filter AllowFilter() {
		return new AllowFilter();
	}

	@Bean
	public FilterRegistrationBean allowFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(AllowFilter());
		registration.addUrlPatterns("/ui/*");
		return registration;
	}

	@Bean
	public FilterRegistrationBean appFilterRegistration() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(UiLoginFilter());
		registrationBean.addUrlPatterns("/ui/*");
		registrationBean.setName("UiLoginFilter");
		return registrationBean;
	}
}
