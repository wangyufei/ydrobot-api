package com.ydrobot.powerplant.web;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.request.PointHistorySyncRequest;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.PathPlanningService;
import com.ydrobot.powerplant.service.PointAlarmHistoryService;
import com.ydrobot.powerplant.service.PointHistoryService;
import com.ydrobot.powerplant.service.PointService;
import com.ydrobot.powerplant.service.RouteService;
import com.ydrobot.powerplant.service.SysPointAlarmHistoryService;
import com.ydrobot.powerplant.service.TaskHistoryService;
import com.ydrobot.powerplant.service.TaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "数据同步模块")
@RestController
@RequestMapping("/ui/data-syncs")
public class DataSyncController extends BaseController {
	@Resource
	private PointHistoryService pointHistoryService;
	@Resource
	private PointAlarmHistoryService pointAlarmHistoryService;
	@Resource
	private TaskHistoryService taskHistoryService;
	@Resource
	private PathPlanningService pathPlanningService;
	@Resource
	private SysPointAlarmHistoryService sysPointAlarmHistoryService;
	@Resource
	private DataSyncService dataSyncService;
	@Resource
	private PointService pointService;
	@Resource
	private TaskService taskService;
	@Resource
	private RouteService routeService;

	@ApiOperation("更新任务信息")
	@PutMapping("/tasks/{id}")
	public Result updateTask(@PathVariable Integer id, @RequestBody Task body) {
		Task task = taskService.findById(id);
		if (task == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		BeanUtils.copyProperties(body, task);
		task.setId(id);
		taskService.update(task);
		return ResultGenerator.genSuccessResult(task);
	}

	@ApiOperation("更新道路信息")
	@PutMapping("/routes/{id}")
	public Result updateRoute(@PathVariable Integer id, @RequestBody Route body) {
		Route route = routeService.findById(id);
		if (route == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		BeanUtils.copyProperties(body, route);
		route.setId(id);
		routeService.update(route);
		return ResultGenerator.genSuccessResult(route);
	}

	@ApiOperation("更新点位信息")
	@PutMapping("/points/{id}")
	public Result updatePoint(@PathVariable Integer id, @RequestBody Point body) {
		Point point = pointService.findById(id);
		if (point == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		BeanUtils.copyProperties(body, point);
		point.setId(id);
		pointService.update(point);
		return ResultGenerator.genSuccessResult(point);
	}

	@ApiOperation("添加点位历史记录")
	@PostMapping("/point-historys")
	public Result addPointHistory(@RequestBody PointHistorySyncRequest body) {
		PointHistory pointHistory = new PointHistory();
		BeanUtils.copyProperties(body, pointHistory);
		pointHistoryService.save(pointHistory);
		dataSyncService.pointHistorySync(pointHistory);
		return ResultGenerator.genSuccessResult(pointHistory);
	}

	@ApiOperation("更新点位历史记录")
	@PutMapping("/point-historys/{id}")
	public Result updatePointHistory(@PathVariable Integer id, @RequestBody PointHistorySyncRequest body) {
		PointHistory pointHistory = pointHistoryService.findById(id);
		if (pointHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}

		BeanUtils.copyProperties(body, pointHistory);
		pointHistory.setId(id);
		pointHistoryService.update(pointHistory);
		dataSyncService.pointHistorySync(pointHistory);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("添加点位报警历史记录")
	@PostMapping("/point-alarm-historys")
	public Result addPointAlarmHistory(@RequestBody PointAlarmHistory body) {
		pointAlarmHistoryService.save(body);
		dataSyncService.pointAlarmHistorySync(body);
		return ResultGenerator.genSuccessResult(body);
	}

	@ApiOperation("更新点位报警历史记录")
	@PutMapping("/point-alarm-historys/{id}")
	public Result updatePointAlarmHistory(@PathVariable Integer id, @RequestBody PointAlarmHistory body) {
		PointAlarmHistory pointAlarmHistory = pointAlarmHistoryService.findById(id);
		if (pointAlarmHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}

		BeanUtils.copyProperties(body, pointAlarmHistory);
		pointAlarmHistory.setId(id);
		pointAlarmHistoryService.update(pointAlarmHistory);
		dataSyncService.pointAlarmHistorySync(pointAlarmHistory);
		return ResultGenerator.genSuccessResult(pointAlarmHistory);
	}

	@ApiOperation("添加任务执行历史记录")
	@PostMapping("/task-historys")
	public Result addTaskHistory(@RequestBody TaskHistory body) {
		taskHistoryService.save(body);
		dataSyncService.taskHistorySync(body);
		return ResultGenerator.genSuccessResult(body);
	}

	@ApiOperation("更新任务执行历史记录")
	@PutMapping("/task-historys/{id}")
	public Result updateTaskHistory(@PathVariable Integer id, @RequestBody TaskHistory body) {
		TaskHistory taskHistory = taskHistoryService.findById(id);
		if (taskHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		BeanUtils.copyProperties(body, taskHistory);
		taskHistory.setId(id);
		taskHistoryService.update(taskHistory);
		dataSyncService.taskHistorySync(taskHistory);
		return ResultGenerator.genSuccessResult(taskHistory);
	}

	@ApiOperation("添加路径规划记录")
	@PostMapping("/path_plannings")
	public Result addPathPlanning(@RequestBody PathPlanning body) {
		pathPlanningService.save(body);
		return ResultGenerator.genSuccessResult(body);
	}

	@ApiOperation("更新路径规划记录")
	@PutMapping("/path_plannings/{id}")
	public Result updatePathPlanning(@PathVariable Integer id, @RequestBody PathPlanning body) {
		PathPlanning pathPlanning = pathPlanningService.findById(id);
		if (pathPlanning == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		BeanUtils.copyProperties(body, pathPlanning);
		pathPlanning.setId(id);
		pathPlanningService.update(pathPlanning);
		return ResultGenerator.genSuccessResult(pathPlanning);
	}

	@ApiOperation("添加系统点位报警记录")
	@PostMapping("/sys-point-alarm-historys")
	public Result addSysPointAlarmHistory(@RequestBody SysPointAlarmHistory body) {
		sysPointAlarmHistoryService.save(body);
		return ResultGenerator.genSuccessResult(body);
	}

	@ApiOperation("更新系统点位报警记录")
	@PutMapping("/sys-point-alarm-historys/{id}")
	public Result updateSysPointAlarmHistory(@PathVariable Integer id, @RequestBody SysPointAlarmHistory body) {
		SysPointAlarmHistory sysPointAlarmHistory = sysPointAlarmHistoryService.findById(id);
		if (sysPointAlarmHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		BeanUtils.copyProperties(body, sysPointAlarmHistory);
		sysPointAlarmHistory.setId(id);
		sysPointAlarmHistoryService.update(sysPointAlarmHistory);
		return ResultGenerator.genSuccessResult(sysPointAlarmHistory);
	}

	@ApiOperation("对象同步")
	@PostMapping("/objects")
	public Result objectSync() {
		dataSyncService.objectSync();
		return ResultGenerator.genSuccessResult();
	}
}
