package com.ydrobot.powerplant.web;

import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.condition.PointAppraiseHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAppraiseHistoryResult;
import com.ydrobot.powerplant.service.PointAppraiseHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by Wyf on 2020/07/07.
*/
@Api(tags = "点位评价历史记录模块")
@RestController
@RequestMapping("/ui/point-appraise-historys")
public class PointAppraiseHistoryController {
    @Resource
    private PointAppraiseHistoryService pointAppraiseHistoryService;

    @ApiOperation("获取点位评价历史记录列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size, 
    		PointAppraiseHistoryQueryParam queryParam) {
        List<PointAppraiseHistoryResult> list = pointAppraiseHistoryService.listPointAppraiseHistory(queryParam,page,size);
        PageInfo<PointAppraiseHistoryResult> pageInfo = new PageInfo<PointAppraiseHistoryResult>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
