package com.ydrobot.powerplant.web;

import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.service.ObjectAlarmHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
* Created by Wyf on 2020/06/01.
*/
@Api(tags = "对象告警记录模块")
@RestController
@RequestMapping("/ui/object-alarm-historys")
public class ObjectAlarmHistoryController {
    @Resource
    private ObjectAlarmHistoryService objectAlarmHistoryService;
    
    @ApiOperation("生成对象告警记录")
    @PostMapping
    public Result add(@RequestBody ObjectAlarmHistory objectAlarmHistory) {
        objectAlarmHistoryService.saveObjectAlarmHistory(objectAlarmHistory);
        return ResultGenerator.genSuccessResult();
    }
}
