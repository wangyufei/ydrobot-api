package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.RobotParam;
import com.ydrobot.powerplant.model.condition.RobotParamQueryParam;
import com.ydrobot.powerplant.model.request.RobotParamBatchRequest;
import com.ydrobot.powerplant.service.RobotParamService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "机器人参数模块")
@RestController
@RequestMapping("/ui/robot-params")
public class RobotParamController {
    @Resource
    private RobotParamService robotParamService;

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            RobotParamQueryParam queryParam) {
    		List<RobotParam> robotParams = robotParamService.listRobotParam(queryParam, page, size);
        PageInfo<RobotParam> pageInfo = new PageInfo<>(robotParams);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("批量更新机器人参数")
    @PutMapping("/batch")
    public Result batchUpdate(@RequestBody RobotParamBatchRequest robotParamBatchRequest) {
        robotParamService.batchUpdate(robotParamBatchRequest);
        return ResultGenerator.genSuccessResult();
    }
}
