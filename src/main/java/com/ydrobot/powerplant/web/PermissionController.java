package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.condition.PermissionQueryParam;
import com.ydrobot.powerplant.model.request.PermissionRequest;
import com.ydrobot.powerplant.service.PermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by Wyf on 2019/04/12.
 */
@Api(tags = "权限模块")
@RestController
@RequestMapping("/ui/permissions")
public class PermissionController {
	@Resource
	private PermissionService permissionService;

	@ApiOperation("添加")
	@PostMapping
	public Result add(@RequestBody PermissionRequest body) {
		permissionService.savePermission(body);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("删除")
	@DeleteMapping("/{id}")
	public Result delete(@PathVariable Integer id) {
		permissionService.deleteById(id);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("更新")
	@PutMapping("/{id}")
	public Result update(@PathVariable Integer id, @RequestBody PermissionRequest body) {
		Permission permission = permissionService.findById(id);

		if (permission == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}
		permissionService.updatePermission(permission, body);
		return ResultGenerator.genSuccessResult();
	}

	@ApiOperation("详情")
	@GetMapping("/{id}")
	public Result detail(@PathVariable Integer id) {
		Permission permission = permissionService.findById(id);
		return ResultGenerator.genSuccessResult(permission);
	}

	@ApiOperation("列表")
	@GetMapping
	public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
			PermissionQueryParam queryParam) {
		List<Permission> permissions = permissionService.listPermission(queryParam, page, size);
		PageInfo<Permission> pageInfo = new PageInfo<>(permissions);
		return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
	}

	@ApiOperation("获取权限树")
	@GetMapping("/tree")
	public Result getPermissionTree() {
		List<Permission> permissions = permissionService.listPermissionTree();
		return ResultGenerator.genSuccessResult(permissions);
	}
}
