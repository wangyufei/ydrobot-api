package com.ydrobot.powerplant.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.core.service.redis.RedisService;
import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.WatchPoint;
import com.ydrobot.powerplant.model.condition.PointQueryParam;
import com.ydrobot.powerplant.model.condition.PointTreeQueryParam;
import com.ydrobot.powerplant.model.dto.AbnormalAppraisePointResult;
import com.ydrobot.powerplant.model.dto.PointResult;
import com.ydrobot.powerplant.model.request.PointRequest;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.MeterPointInfoService;
import com.ydrobot.powerplant.service.PointService;
import com.ydrobot.powerplant.service.PointVisibleLightService;
import com.ydrobot.powerplant.service.ReconModelSubService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "点位模块")
@RestController
@RequestMapping("/ui/points")
public class PointController {
    @Resource
    private PointService pointService;
    @Resource
    private MeterPointInfoService meterPointInfoService;
    @Resource
    private RedisService redisService;
    @Resource
    private PointVisibleLightService pointVisibleLightService;
    @Resource
    private ReconModelSubService reconModelSubService;
    @Resource
	private DataSyncService dataSyncService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody PointRequest body) {
        Integer pointId = pointService.savePoint(body);
        Map<String, Object> map = new HashMap<>();
        map.put("pointId", pointId);
        return ResultGenerator.genSuccessResult(map);
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        Point point = pointService.findById(id);
        if (point == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }
        point.setIsDelete(new Byte("1"));
        pointService.update(point);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody PointRequest body) {
        Point point = pointService.findById(id);
        if (point == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }
        pointService.updatePoint(point, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
    		PointResult pointDTO = pointService.getDetail(id);
        return ResultGenerator.genSuccessResult(pointDTO);
    }
    
    @ApiOperation("通过点位id获取观察点列表")
    @GetMapping("/{id}/watch-points")
    public Result listWatchPointByPointId(@PathVariable Integer id) {
    		List<WatchPoint> watchPoints = pointService.listWatchPointByPointId(id);
        return ResultGenerator.genSuccessResult(watchPoints);
    }

    @ApiOperation("获取点位列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            PointQueryParam queryParam) {
    		List<Point> points = pointService.listPoint(queryParam, page, size);
        PageInfo<Point> pageInfo = new PageInfo<>(points);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("获取点位树接口")
    @GetMapping("/tree")
    public Result getPointTree(PointTreeQueryParam queryParam) {
        long random = System.currentTimeMillis();
        List<Device> devices = pointService.listPointTree(random,queryParam);
        return ResultGenerator.genSuccessResult(devices);
    }

    @ApiOperation("获取异常点位树")
    @GetMapping("/abnormal-tree")
    public Result getAbnormalTree(PointTreeQueryParam queryParam) {
        long random = System.currentTimeMillis();
        List<Device> devices = pointService.listAbnormalPointTree(random,queryParam);
        return ResultGenerator.genSuccessResult(devices);
    }
    
    @ApiOperation("获取异常评价部件列表")
    @GetMapping("/abnormal-appraise")
    public Result getAbnormalAppraise() {
    		List<AbnormalAppraisePointResult> abnormalAppraisePointResults = pointService.listAbnormalAppraisePoint();
    		return ResultGenerator.genSuccessResult(abnormalAppraisePointResults);
    }
    
    @ApiOperation("点位同步")
	@PostMapping("/sync")
	public Result pointSync() {
		dataSyncService.pointSync();
		return ResultGenerator.genSuccessResult();
	}
}
