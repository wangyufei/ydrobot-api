package com.ydrobot.powerplant.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PointRelationSetting;
import com.ydrobot.powerplant.model.condition.PointRelationSettingQueryParam;
import com.ydrobot.powerplant.model.condition.PointRelationSettingListQueryParam;
import com.ydrobot.powerplant.model.request.PointRelationSettingBatchRequest;
import com.ydrobot.powerplant.service.PointRelationSettingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "点位关系模块")
@RestController
@RequestMapping("/ui/point-relation-settings")
public class PointRelationSettingController {
    @Resource
    private PointRelationSettingService pointRelationSettingService;

    @ApiOperation("获取关联点列表")
    @PostMapping
    public Result list(@RequestBody PointRelationSettingListQueryParam queryParam) {
    		List<PointRelationSetting> pointRelationSettings = pointRelationSettingService.listPointRelationSetting(queryParam);
        PageInfo<PointRelationSetting> pageInfo = new PageInfo<>(pointRelationSettings);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("更新点位关系")
    @PutMapping
    public Result update(@RequestBody PointRelationSettingBatchRequest body) {
        pointRelationSettingService.updatePointRelationSetting(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("获取某个点的关联点")
    @GetMapping("/relations")
    public Result getRelationPoint(PointRelationSettingQueryParam queryParam){
        if (queryParam.getPointId() == null) {
            throw new ServiceException(ResultCode.INVALID_PARAM);
        }

        if (queryParam.getType() == null) {
            throw new ServiceException(ResultCode.INVALID_PARAM);
        }

        List<Map<String, Object>> maps = pointRelationSettingService.listRelationPoint(queryParam);
        return ResultGenerator.genSuccessResult(maps);
    }
}
