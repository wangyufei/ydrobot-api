package com.ydrobot.powerplant.web;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.request.UpdatePointAppraiseStatusRequest;
import com.ydrobot.powerplant.service.PointAppraiseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "点位评价模块")
@RestController
@RequestMapping("/ui/point-appraise")
public class PointAppraiseController {
    @Resource
    private PointAppraiseService pointAppraiseService;

    @ApiOperation("更新点位评价状态")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody UpdatePointAppraiseStatusRequest body) {
        PointAppraise pointAppraise = pointAppraiseService.findById(id);
        if (pointAppraise == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }
        pointAppraiseService.updatePointAppraiseStatus(pointAppraise, body);
        return ResultGenerator.genSuccessResult();
    }
    
}
