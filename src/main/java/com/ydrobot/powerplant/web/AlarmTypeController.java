package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.service.AlarmTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "告警类型模块")
@RestController
@RequestMapping("/ui/alarm-types")
public class AlarmTypeController {
    @Resource
    private AlarmTypeService alarmTypeService;

    @ApiOperation("获取告警类型列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<AlarmType> alarmTypes = alarmTypeService.findAll();
        PageInfo<AlarmType> pageInfo = new PageInfo<>(alarmTypes);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
