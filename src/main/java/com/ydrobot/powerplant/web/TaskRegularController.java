package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.TaskRegular;
import com.ydrobot.powerplant.model.condition.TaskRegularQueryParam;
import com.ydrobot.powerplant.model.request.UpdateTaskRegularBatchRequest;
import com.ydrobot.powerplant.service.TaskRegularService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "任务定期执行模块")
@RestController
@RequestMapping("/ui/task-regulars")
public class TaskRegularController {
    @Resource
    private TaskRegularService taskRegularService;

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        taskRegularService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("批量更新")
    @PutMapping("/batch")
    public Result batchUpdate(@RequestBody UpdateTaskRegularBatchRequest body){
        taskRegularService.batchUpdate(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer size,
            TaskRegularQueryParam queryParam) {
    		List<TaskRegular> taskRegulars = taskRegularService.listTaskRegular(queryParam, page, size);
        PageInfo<TaskRegular> pageInfo = new PageInfo<>(taskRegulars);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
