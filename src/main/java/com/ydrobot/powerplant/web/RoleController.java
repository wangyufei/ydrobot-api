package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.Role;
import com.ydrobot.powerplant.model.dto.RoleResult;
import com.ydrobot.powerplant.model.request.RoleRequest;
import com.ydrobot.powerplant.model.request.UpdateRolePermissionRequest;
import com.ydrobot.powerplant.service.RoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "角色模块")
@RestController
@RequestMapping("/ui/roles")
public class RoleController {
    @Resource
    private RoleService roleService;

    @ApiOperation("添加")
    @PostMapping
    public Result add(@RequestBody RoleRequest body) {
        roleService.saveRole(body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        roleService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("更新")
    @PutMapping("/{id}")
    public Result update(@PathVariable Integer id, @RequestBody RoleRequest body) {
        Role role = roleService.findById(id);

        if (role == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }

        roleService.updateRole(role, body);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Role role = roleService.findById(id);
        return ResultGenerator.genSuccessResult(role);
    }

    @ApiOperation("列表")
    @GetMapping
    public Result listRole(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
    		List<RoleResult> roleResults = roleService.listRole(page, size);
        PageInfo<RoleResult> pageInfo = new PageInfo<>(roleResults);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }

    @ApiOperation("更新角色权限")
    @PutMapping("/{id}/Permissions")
    public Result updateRolePermission(@PathVariable Integer id, @RequestBody UpdateRolePermissionRequest body)
            throws ServiceException {
        Role role = roleService.findById(id);

        if (role == null) {
            throw new ServiceException(ResultCode.DATA_NOT_FOUND);
        }

        roleService.updateRolePermission(role, body);
        return ResultGenerator.genSuccessResult();
    }
}
