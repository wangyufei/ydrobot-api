package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.dto.ObjectPatrolInfoResult;
import com.ydrobot.powerplant.model.dto.ObjectResult;
import com.ydrobot.powerplant.model.dto.PieResult;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.ObjectService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2019/04/12.
*/
@Api(tags = "对象模块")
@RestController
@RequestMapping("/ui/objects")
public class ObjectController {
    @Resource
    private ObjectService objectService;
    @Resource
	private DataSyncService dataSyncService;

    @ApiOperation("获取对象列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        PageHelper.startPage(page, size);
        List<TObject> list = objectService.findAll();
        PageInfo<TObject> pageInfo = new PageInfo<TObject>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
    
    @ApiOperation("详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
    		ObjectResult object = objectService.getDetail(id);
        return ResultGenerator.genSuccessResult(object);
    }
    
    @ApiOperation("对象同步")
    @PostMapping("/sync")
    public Result objectSync() {
    		dataSyncService.objectSync();
    		return ResultGenerator.genSuccessResult();
    }
    
    @ApiOperation("获取某个对象的巡检信息")
    @GetMapping("/{id}/patrol-info")
    public Result getObjectPatrolInfo(@PathVariable Integer id) {
    		ObjectPatrolInfoResult objectPatrolInfoResult = objectService.getObjectPatrolInfo(id);
    		return ResultGenerator.genSuccessResult(objectPatrolInfoResult);
    }
    
    @ApiOperation("获取异常评价对象列表")
	@GetMapping("/abnormal-appraise-objects")
	public Result getAbnormalAppraiseObject() {
    		List<TObject> objects = objectService.listAbnormalAppraiseObject();
		return ResultGenerator.genSuccessResult(objects);
	}
    
    @ApiOperation("获取最近一次任务的告警对象列表")
    @GetMapping("/alarm-objects")
    public Result getAlarmObject() {
    		List<TObject> objects = objectService.listAlarmObject();
    		return ResultGenerator.genSuccessResult(objects);
    }
    
    @ApiOperation("获取某个对象的报警位置")
    @GetMapping("/{id}/alarm-position")
    public Result getObjectAlarmPosition(@PathVariable Integer id) {
    		List<PointHistory> pointHistories = objectService.listObjectAlarmPosition(id);
    		return ResultGenerator.genSuccessResult(pointHistories);
    }
    
    @ApiOperation("统计对象的运行状态")
    @GetMapping("/{id}/run-status")
    public Result countObjectRunStatus(@PathVariable Integer id) {
    		List<PieResult> pieResults = objectService.countObjectRunStatus(id);
    		return ResultGenerator.genSuccessResult(pieResults);
    }
}
