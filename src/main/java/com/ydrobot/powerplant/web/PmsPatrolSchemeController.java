package com.ydrobot.powerplant.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.PmsPatrolScheme;
import com.ydrobot.powerplant.service.PmsPatrolSchemeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* Created by Wyf on 2020/01/10.
*/
@Api(tags = "PMS巡检任务模块")
@RestController
@RequestMapping("/ui/pms-patrol-schemes")
public class PmsPatrolSchemeController {
    @Resource
    private PmsPatrolSchemeService pmsPatrolSchemeService;

    @ApiOperation("获取pms巡检任务列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<PmsPatrolScheme> pmsPatrolSchemes = pmsPatrolSchemeService.findAll();
        PageInfo<PmsPatrolScheme> pageInfo = new PageInfo<>(pmsPatrolSchemes);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
