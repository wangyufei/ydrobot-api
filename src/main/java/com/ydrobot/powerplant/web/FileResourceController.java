package com.ydrobot.powerplant.web;

import com.ydrobot.powerplant.common.CommonPage;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultGenerator;
import com.ydrobot.powerplant.model.FileResource;
import com.ydrobot.powerplant.model.condition.FileResourceQueryParam;
import com.ydrobot.powerplant.service.FileResourceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by Wyf on 2020/05/13.
*/
@Api(tags = "文件资源模块")
@RestController
@RequestMapping("/ui/file-resources")
public class FileResourceController {
    @Resource
    private FileResourceService fileResourceService;

    @ApiOperation("添加文件资源")
    @PostMapping
    public Result add(@RequestBody FileResource fileResource) {
        fileResourceService.save(fileResource);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("删除文件资源")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        fileResourceService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @ApiOperation("获取文件资源详情")
    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        FileResource fileResource = fileResourceService.findById(id);
        return ResultGenerator.genSuccessResult(fileResource);
    }

    @ApiOperation("获取文件资源列表")
    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size,FileResourceQueryParam queryParam) {
        PageHelper.startPage(page, size);
        List<FileResource> list = fileResourceService.listFileResource(queryParam,page,size);
        PageInfo<FileResource> pageInfo = new PageInfo<>(list);
        return ResultGenerator.genSuccessResult(CommonPage.restPage(pageInfo));
    }
}
