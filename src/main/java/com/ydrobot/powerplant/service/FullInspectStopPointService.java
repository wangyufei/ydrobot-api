package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.FullInspectStopPoint;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface FullInspectStopPointService extends Service<FullInspectStopPoint> {

}
