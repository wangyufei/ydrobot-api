package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PointAlarmConfirm;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/06/13.
 */
public interface PointAlarmConfirmService extends Service<PointAlarmConfirm> {

}
