package com.ydrobot.powerplant.service;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.ObjectType;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface ObjectTypeService extends Service<ObjectType> {
	
	/**
	 * 获取对象类型名称
	 * @param id
	 * @return
	 */
	public String getObjectTypeNameById(Integer id);
}
