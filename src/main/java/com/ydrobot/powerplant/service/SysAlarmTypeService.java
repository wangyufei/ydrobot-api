package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.SysAlarmType;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/04/30.
 */
public interface SysAlarmTypeService extends Service<SysAlarmType> {

}
