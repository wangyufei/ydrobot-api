package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PmsPatrolScheme;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/01/10.
 */
public interface PmsPatrolSchemeService extends Service<PmsPatrolScheme> {

}
