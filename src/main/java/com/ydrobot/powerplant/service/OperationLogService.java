package com.ydrobot.powerplant.service;

import java.util.List;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.OperationLog;
import com.ydrobot.powerplant.model.condition.OperationLogQueryParam;
import com.ydrobot.powerplant.model.dto.OperationLogResult;
import com.ydrobot.powerplant.model.request.OperationLogRequest;

/**
 * Created by Wyf on 2019/04/30.
 */
public interface OperationLogService extends Service<OperationLog> {

    /**
     * 保存操作日志
     * @param operationLogRequest
     * @return
     */
    public void saveOperationLog(OperationLogRequest operationLogRequest);
    
    /**
     * 获取操作日志列表
     * @param operationLogCondition
     * @param page
     * @param size
     * @return
     */
    public List<OperationLogResult> listOperationLog(OperationLogQueryParam queryParam,Integer page,Integer size);
}
