package com.ydrobot.powerplant.service;

import java.util.List;
import java.util.Map;

import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Robot;
import com.ydrobot.powerplant.model.dto.CurrentTaskInfoResult;
import com.ydrobot.powerplant.model.dto.RobotResult;
import com.ydrobot.powerplant.model.request.ControlAuthRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface RobotService extends Service<Robot> {

    /**
     * 获取机器人当前任务信息
     * @param id
     * @return
     * @throws ServiceException
     */
    public CurrentTaskInfoResult getRobotCurrentTask(Integer id) throws ServiceException;
    
    /**
     * 获取机器人列表
     * @param page
     * @param size
     * @return
     */
    public List<RobotResult> listRobot(Integer page,Integer size);

    /**
     * 获取控制权限
     * @param id
     * @return
     */
    public Map<String, Object> getControlAuth(Integer id);

    /**
     * 确认控制权限请求
     * @param id
     */
    public void handleControlAuth(Integer id, ControlAuthRequest controlAuthRequest);

    /**
     * 检查是否有控制权限
     * @param id
     * @return
     */
    public Map<String, Object> checkControlAuth(Integer id);

    /**
     * 获取一键返航
     * @param id
     * @return
     */
    public PathPlanning getTurnBack(Integer id);
}
