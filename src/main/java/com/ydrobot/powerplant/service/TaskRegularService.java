package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.TaskRegular;
import com.ydrobot.powerplant.model.condition.TaskRegularQueryParam;
import com.ydrobot.powerplant.model.request.UpdateTaskRegularBatchRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface TaskRegularService extends Service<TaskRegular> {

	/**
	 * 获取定期任务列表
	 * 
	 * @param queryParam
	 * @param page
	 * @param size
	 * @return
	 */
	public List<TaskRegular> listTaskRegular(TaskRegularQueryParam queryParam, Integer page, Integer size);

	/**
	 * 批量更新
	 * 
	 * @param updateTaskRegularBatchRequest
	 */
	public void batchUpdate(UpdateTaskRegularBatchRequest updateTaskRegularBatchRequest);
}
