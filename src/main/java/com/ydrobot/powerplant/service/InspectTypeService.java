package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.InspectType;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface InspectTypeService extends Service<InspectType> {

    /**
     * 获取巡检类型树
     * @return
     */
    public List<InspectType> listInspectTypeTree();
}
