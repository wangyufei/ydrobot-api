package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PointAppraiseHistoryMapper;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.condition.PointAppraiseHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAppraiseHistoryResult;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.PointAppraiseHistoryService;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/07/07.
 */
@Service
@Transactional
public class PointAppraiseHistoryServiceImpl extends AbstractService<PointAppraiseHistory> implements PointAppraiseHistoryService {
    @Resource
    private PointAppraiseHistoryMapper pointAppraiseHistoryMapper;
    @Resource
    private ObjectMapper objectMapper;
    @Resource
    private DataSyncService dataSyncService;

	@Override
	public List<PointAppraiseHistoryResult> listPointAppraiseHistory(PointAppraiseHistoryQueryParam queryParam, Integer page,
			Integer size) {
		PageHelper.startPage(page, size);
		return pointAppraiseHistoryMapper.getList(queryParam);
	}

	@Override
	public void savePointAppraiseHistory(PointAppraiseHistory pointAppraiseHistory) {
		// 更新对象异常运行时间
		updateObjectAbnormalRunTime(pointAppraiseHistory);
		
		pointAppraiseHistory.setAppraiseTime(new Date());
		pointAppraiseHistory.setId(null);
		save(pointAppraiseHistory);
		
		// 点位评价记录同步
		dataSyncService.pointAppraiseHistorySync(pointAppraiseHistory);
	}
	
	/**
	 * 更新对象异常运行时间
	 * @param pointAppraiseHistory
	 */
	private void updateObjectAbnormalRunTime(PointAppraiseHistory pointAppraiseHistory) {
		// 获取对象信息
		TObject tObject = objectMapper.geTObjectByPointId(pointAppraiseHistory.getPointId());
		if (tObject!=null) {
			// 1、获取该设备下所有点位的最后一条信息
			PointAppraiseHistory lastAbnormalPointAppraiseHistory = pointAppraiseHistoryMapper.getObjectLastPointAppraiseHistory(tObject.getId());
			if (lastAbnormalPointAppraiseHistory!=null && lastAbnormalPointAppraiseHistory.getAppraiseTime()!=null) {
				// 3、取最后一条异常评价记录时间与当前时间做对比，获取差值
				long betweenDay = DateUtil.between(lastAbnormalPointAppraiseHistory.getAppraiseTime(), new Date(), DateUnit.DAY);
				// 4、获取设备当前异常时间，加上这个差值
				Integer abnormalRunTime = tObject.getAbnormalRunTime() + (int) betweenDay;
				tObject.setAbnormalRunTime(abnormalRunTime);
				// 5、更新设备的异常时间
				objectMapper.updateByPrimaryKeySelective(tObject);
			}
		}
	}
}
