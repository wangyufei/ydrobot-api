package com.ydrobot.powerplant.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.core.utils.ProjectDateUtils;
import com.ydrobot.powerplant.core.utils.ProjectExcelUtils;
import com.ydrobot.powerplant.core.utils.ProjectObjectUtils;
import com.ydrobot.powerplant.core.utils.ProjectStatusUtils;
import com.ydrobot.powerplant.core.utils.ProjectUrlConvert;
import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PathPlanningMapper;
import com.ydrobot.powerplant.dao.PointAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.PointHistoryMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.ReconTypeMapper;
import com.ydrobot.powerplant.dao.TaskHistoryMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.dao.TaskPointMapper;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.ReconType;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.condition.TaskHistoryQueryParam;
import com.ydrobot.powerplant.model.condition.TaskMonthShowQueryParam;
import com.ydrobot.powerplant.model.dto.TaskHistoryResult;
import com.ydrobot.powerplant.model.request.TaskHistoryRequest;
import com.ydrobot.powerplant.service.PointAlarmHistoryService;
import com.ydrobot.powerplant.service.PointHistoryService;
import com.ydrobot.powerplant.service.TaskHistoryService;
import com.ydrobot.powerplant.service.TaskService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class TaskHistoryServiceImpl extends AbstractService<TaskHistory> implements TaskHistoryService {

	private static final Logger log = LoggerFactory.getLogger(TaskHistoryServiceImpl.class);

	@Value("${file.path}")
	private String excelPath;

	@Resource
	private TaskHistoryMapper taskHistoryMapper;
	@Resource
	private TaskPointMapper taskPointMapper;
	@Resource
	private PointAlarmHistoryMapper pointAlarmHistoryMapper;
	@Resource
	private PointHistoryMapper pointHistoryMapper;
	@Resource
	private TaskMapper taskMapper;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private ReconTypeMapper reconTypeMapper;
	@Resource
	private PointHistoryService pointHistoryService;
	@Resource
	private PointAlarmHistoryService pointAlarmHistoryService;
	@Resource
	private TaskService taskService;
	@Resource
	private ObjectMapper objectMapper;
	@Resource
	private PathPlanningMapper pathPlanningMapper;

	@Override
	public List<TaskHistoryResult> listTaskHistory(TaskHistoryQueryParam queryParam, Integer page, Integer size) {
		PageHelper.startPage(page,size);
		List<TaskHistoryResult> taskHistoryResults = taskHistoryMapper.getList(queryParam);
		for (TaskHistoryResult taskHistoryResult : taskHistoryResults) {
			//巡检点总数
			Integer pointTotalNum = pointHistoryService.countPointTotalNumByTaskHistoryId(taskHistoryResult.getId());
			//报警点数
			Integer alarmPointNum = pointAlarmHistoryMapper.getAlarmPointNum(taskHistoryResult.getId());
			//异常点数
			Integer abnormalPointNum = pointAlarmHistoryMapper.getAbnormalPointNum(taskHistoryResult.getId());
			//正常点数
			Integer normalPointNum = pointAlarmHistoryMapper.getNormalPointNum(taskHistoryResult.getId());
			
			taskHistoryResult.setPointCheckStatus(getPointCheckStatus(taskHistoryResult.getId()));
			taskHistoryResult.setPointTotalNum(pointTotalNum);
			taskHistoryResult.setAlarmPointNum(alarmPointNum);
			taskHistoryResult.setAbnormalPointNum(abnormalPointNum);
			taskHistoryResult.setNormalPointNum(normalPointNum);
		}
		
		return taskHistoryResults;
	}

	@Override
	public List<TaskPoint> listTaskPoint(Integer taskHistoryId) {
		List<TaskPoint> taskPoints = new ArrayList<>();
		TaskHistory taskHistory = taskHistoryMapper.selectByPrimaryKey(taskHistoryId);
		if (taskHistory == null) {
			return taskPoints;
		}

		TaskPoint record = new TaskPoint();
		record.setTaskId(taskHistory.getTaskId());
		taskPoints = taskPointMapper.select(record);
		return taskPoints;
	}

	@Override
	public void updateTaskHistory(TaskHistory taskHistory, TaskHistoryRequest taskHistoryRequest) {
		taskHistory.setCheckDesc(taskHistoryRequest.getCheckDesc());
		taskHistory.setOperaterId(ThreadCache.getUser().getId());
		taskHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
		taskHistory.setCheckTime(new Date());
		update(taskHistory);
	}

	@Override
	public List<Map<String, Object>> listTaskMonthShow(TaskMonthShowQueryParam taskMonthShowCondition) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		String startTime = taskMonthShowCondition.getExecuteTime();
		String endTime = "";
		try {
			Date endDate = formatter.parse(startTime);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(endDate);
			calendar.add(Calendar.MONTH, +1);

			endDate = calendar.getTime();
			endTime = formatter.format(endDate);
		} catch (ParseException e) {
			log.info(e.getMessage());
		}

		List<Map<String, Object>> objects = taskHistoryMapper.findByExecuteTime(taskMonthShowCondition.getRobotId(),
				startTime, endTime);
		List<Map<String, Object>> maps = new ArrayList<>();

		for (Map<String, Object> object : objects) {
			Map<String, Object> map = new HashMap<>();
			map.put("executeTime", object.get("groupDate"));
			map.put("count", object.get("count"));
			map.put("list", findTaskHistoryByTimeAndRobotId((String) object.get("groupDate"),
					taskMonthShowCondition.getRobotId()));
			maps.add(map);
		}

		return maps;
	}

	@Override
	public Integer getCurrentTaskAlarmNum(Integer robotId) {
		Integer currentTaskAlarNum = 0;
		TaskHistory taskHistory = taskHistoryMapper.getTaskHistoryByRobotId(robotId);
		if (taskHistory != null) {
			currentTaskAlarNum = pointAlarmHistoryMapper.getAllAlarmPointNum(taskHistory.getId());
		}
		return currentTaskAlarNum;
	}

	@Override
	public void exportReport(String taskHistoryIds, HttpServletResponse response) {
		List<Integer> ids = ProjectCommonUtils.getSplitValInt(taskHistoryIds, ",");
		Condition condition = new Condition(TaskHistory.class);
		Criteria criteria = condition.createCriteria();
		criteria.andIn("id", ids);
		List<TaskHistory> taskHistories = findByCondition(condition);

		List<String> fileNames = new ArrayList<String>();
		if (taskHistories.size() > 1) {
			for (TaskHistory taskHistory : taskHistories) {
				exportTaskHistory(taskHistory, response, fileNames, 0);
			}

			try {
				ProjectExcelUtils.exportZip(response, fileNames);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (taskHistories.size() == 1) {
			exportTaskHistory(taskHistories.get(0), response, fileNames, 1);
		}
	}

	@Override
	public TObject getLastPatrolObject(Integer taskHistoryId) {
		return objectMapper.getLastPatrolObject(taskHistoryId);
	}

	@Override
	public List<TObject> getPatrolObjects(Integer taskHistoryId) {
		// 获取机器人当前的路径规划
		PathPlanning pathPlanning = pathPlanningMapper.getRobotCurrentPath(taskHistoryId);
		String pointIds = "";
		List<TObject> tObjects = new ArrayList<>();
		// 通过路径规划获取部件巡检顺序
		if (pathPlanning != null) {
			String path = pathPlanning.getPath();
			if (path != "") {
				JSONObject pathJson = JSONObject.parseObject(path);
				JSONArray tasks = pathJson.getJSONArray("Tasks");
				for (Iterator<Object> iterator = tasks.iterator(); iterator.hasNext();) {
					JSONObject next = (JSONObject) iterator.next();
					String cameraPoses = next.getString("CameraPose");
					
					if (cameraPoses!=null && cameraPoses.length()!=0) {
						String[] cameraPoseArr = cameraPoses.split(";");
						for (int i = 0; i < cameraPoseArr.length; i++) {
							String[] strArr = cameraPoseArr[i].split(":");
							if (strArr[0] != "") {
								// 判断字符串中是否已经有这个部件
								if (pointIds.indexOf(","+strArr[0]+",") == -1) {
									pointIds += strArr[0] + ",";
								}
							}
						}
					}
				}
			}
		}

		// 通过部件巡检顺序获取设备信息
		if (pointIds!="") {
			pointIds = ProjectCommonUtils.removeExtraComma(pointIds);
			tObjects = objectMapper.getObjectByPointIds(pointIds);
		}
		return tObjects;
	}

	@Override
	public List<TObject> getAlreadyPatrolObjects(Integer taskHistoryId) {
		List<TObject> objects = objectMapper.getAlreadyPatrolObjects(taskHistoryId);
		return objects;
	}

	private void exportTaskHistory(TaskHistory taskHistory, HttpServletResponse response, List<String> fileNames,
			Integer status) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Task task = taskMapper.selectByPrimaryKey(taskHistory.getTaskId());
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(task.getName());
		String fileName = task.getName() + "_" + taskHistory.getId() + ".xls";

		sheet.setDisplayGridlines(false);

		HSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setWrapText(true);

		Integer pointTotalNum = pointHistoryService.countPointTotalNumByTaskHistoryId(taskHistory.getId());
		Integer alarmPointNum = pointAlarmHistoryMapper.getAlarmPointNum(taskHistory.getId());
		Integer abnormalPointNum = pointAlarmHistoryMapper.getAbnormalPointNum(taskHistory.getId());
		Integer normalPointNum = pointAlarmHistoryMapper.getNormalPointNum(taskHistory.getId());

		for (int i = 0; i < 6; i++) {
			sheet.autoSizeColumn(i, true);
			sheet.setColumnWidth(i, sheet.getColumnWidth(i) * 35 / 10);
			sheet.setDefaultColumnStyle(i, ProjectExcelUtils.excelStyle(workbook));
		}

		HSSFRow row = sheet.createRow(0);
		row.createCell(0).setCellValue("任务名称");
		row.createCell(1).setCellValue(task.getName());
		row.setHeightInPoints(40);

		HSSFRow row1 = sheet.createRow(1);
		row1.setHeightInPoints(40);
		row1.createCell(0).setCellValue("任务时间");
		row1.createCell(1)
				.setCellValue("任务开始时间：" + sdf.format(taskHistory.getTaskStartTime()) + ",任务结束时间："
						+ sdf.format(taskHistory.getTaskEndTime()) + ",总时长:"
						+ ProjectDateUtils.getHMS(taskHistory.getTaskStartTime(), taskHistory.getTaskEndTime()));

		HSSFRow row2 = sheet.createRow(2);
		row2.setHeightInPoints(40);
		row2.createCell(0).setCellValue("任务状态");
		row2.createCell(1).setCellValue(ProjectStatusUtils.getTaskStatus(taskHistory.getTaskStatus().intValue()));

		HSSFRow row3 = sheet.createRow(3);
		row3.setHeightInPoints(40);
		row3.createCell(0).setCellValue("巡检点");
		row3.createCell(1).setCellValue("本次任务共巡视" + pointTotalNum + "点位，正常点位" + normalPointNum + "个，告警点位"
				+ alarmPointNum + "个，识别异常点位" + abnormalPointNum + "个");

		HSSFRow row4 = sheet.createRow(4);
		row4.setHeightInPoints(40);
		row4.createCell(0).setCellValue("环境信息");
		row4.createCell(1).setCellValue("环境温度：" + taskHistory.getTemp() + "°C 环境湿度：" + taskHistory.getHum() + "% 环境风速："
				+ taskHistory.getWind() + "m/s");

		for (int i = 0; i < 5; i++) {
			sheet.addMergedRegion(new CellRangeAddress(i, i, 1, 5));
		}

		HSSFRow row5 = sheet.createRow(5);
		row5.setHeightInPoints(40);
		row5.createCell(0).setCellValue("告警点位");
		sheet.addMergedRegion(new CellRangeAddress(5, 5, 0, 5));

		HSSFRow row6 = sheet.createRow(6);
		row6.setHeightInPoints(40);
		row6.createCell(0).setCellValue("识别类型");
		row6.createCell(1).setCellValue("点位名称");
		row6.createCell(2).setCellValue("识别结果");
		row6.createCell(3).setCellValue("告警等级");
		row6.createCell(4).setCellValue("识别时间");
		row6.createCell(5).setCellValue("采集信息");

		int rowAlarmNum = 7;
		List<PointAlarmHistory> pointAlarmHistories = pointAlarmHistoryService
				.listPointAlarmHistoryByTaskHistoryId(taskHistory.getId());
		for (PointAlarmHistory pointAlarmHistory : pointAlarmHistories) {
			Point point = pointMapper.selectByPrimaryKey(pointAlarmHistory.getPointId());
			PointHistory pointHistory = pointHistoryMapper.selectByPrimaryKey(pointAlarmHistory.getPointHistoryId());

			if (point != null && pointHistory != null) {
				ReconType reconType = reconTypeMapper.selectByPrimaryKey(point.getReconTypeId());
				String reconTypeName = "未知";

				if (reconType != null) {
					reconTypeName = reconType.getName();
				}

				HSSFRow alarmRow = sheet.createRow(rowAlarmNum);
				alarmRow.createCell(0).setCellValue(reconTypeName);
				alarmRow.createCell(1).setCellValue(point.getName());
				alarmRow.createCell(2).setCellValue(pointHistory.getValue());
				alarmRow.createCell(3)
						.setCellValue(ProjectStatusUtils.getAlarmLevel(pointAlarmHistory.getAlarmLevel().intValue()));
				alarmRow.createCell(4).setCellValue(sdf.format(pointAlarmHistory.getCreateTime()));

				alarmRow.setHeightInPoints(170);

				HSSFCell cell = alarmRow.createCell(5);
				cell.setCellStyle(ProjectExcelUtils.setExcelLinkStyle(workbook));
				// 如果是红外 或者 红外+可见光
				if (point.getSaveTypeId().intValue() == 1 || point.getSaveTypeId().intValue() == 4) {
					String link = pointHistory.getFlirPic();
					ProjectExcelUtils.excelInsertImg(workbook, sheet, link, (short) 5, rowAlarmNum, (short) 6,
							rowAlarmNum + 1);
				} else if (point.getSaveTypeId().intValue() == 2) {
					String link = pointHistory.getCameraPic();
					ProjectExcelUtils.excelInsertImg(workbook, sheet, link, (short) 5, rowAlarmNum, (short) 6,
							rowAlarmNum + 1);
				} else {
					alarmRow.createCell(5).setCellValue("未知");
				}

				rowAlarmNum++;
			}
		}

		int rowAbnormalTitleNum = rowAlarmNum;
		HSSFRow abnormalTitle = sheet.createRow(rowAbnormalTitleNum);
		abnormalTitle.setHeightInPoints(40);
		abnormalTitle.createCell(0).setCellValue("识别异常点位");
		sheet.addMergedRegion(new CellRangeAddress(rowAbnormalTitleNum, rowAbnormalTitleNum, 0, 5));

		int rowAbnormalThNum = rowAbnormalTitleNum + 1;
		HSSFRow rowAbnormalTh = sheet.createRow(rowAbnormalThNum);
		rowAbnormalTh.setHeightInPoints(40);
		rowAbnormalTh.createCell(0).setCellValue("识别类型");
		rowAbnormalTh.createCell(1).setCellValue("点位名称");
		rowAbnormalTh.createCell(2).setCellValue("识别结果");
		rowAbnormalTh.createCell(3).setCellValue("审核结果");
		rowAbnormalTh.createCell(4).setCellValue("识别时间");
		rowAbnormalTh.createCell(5).setCellValue("采集信息");

		int rowAbnormalListNum = rowAbnormalThNum + 1;
		List<PointHistory> pointHistories = pointHistoryService.listPointHistoryByTaskHistoryId(taskHistory.getId());
		for (PointHistory pointHistory : pointHistories) {
			Point point = pointMapper.selectByPrimaryKey(pointHistory.getPointId());
			if (point != null) {
				ReconType reconType = reconTypeMapper.selectByPrimaryKey(point.getReconTypeId());
				String reconTypeName = "未知";
				if (reconType != null) {
					reconTypeName = reconType.getName();
				}

				HSSFRow abnormalNum = sheet.createRow(rowAbnormalListNum);
				abnormalNum.createCell(0).setCellValue(reconTypeName);
				abnormalNum.createCell(1).setCellValue(point.getName());
				abnormalNum.createCell(2).setCellValue(pointHistory.getValue());
				abnormalNum.createCell(3)
						.setCellValue(ProjectStatusUtils.getCheckStatus(pointHistory.getCheckStatus().intValue()));
				abnormalNum.createCell(4).setCellValue(sdf.format(pointHistory.getCreateTime()));

				abnormalNum.setHeightInPoints(40);

				HSSFCell cell = abnormalNum.createCell(5);
				cell.setCellStyle(ProjectExcelUtils.setExcelLinkStyle(workbook));
				// 如果是红外
				if (point.getSaveTypeId().intValue() == 1 || point.getSaveTypeId().intValue() == 4) {
					String link = ProjectUrlConvert.covert2HttpUrl(pointHistory.getFlirPic());
					String cellValue = "红外图片";
					cell.setCellFormula("HYPERLINK(\"" + link + "\",\"" + cellValue + "\")");
				} else if (point.getSaveTypeId().intValue() == 2) {
					String link = ProjectUrlConvert.covert2HttpUrl(pointHistory.getCameraPic());
					String cellValue = "可见光图片";
					cell.setCellFormula("HYPERLINK(\"" + link + "\",\"" + cellValue + "\")");
				} else {
					abnormalNum.createCell(5).setCellValue("未知");
				}

				rowAbnormalListNum++;
			}
		}

		Integer normalTitleNum = rowAbnormalListNum;
		HSSFRow normalTitle = sheet.createRow(normalTitleNum);
		normalTitle.createCell(0).setCellValue("正常点位");
		normalTitle.setHeightInPoints(40);
		sheet.addMergedRegion(new CellRangeAddress(normalTitleNum, normalTitleNum, 0, 5));

		int rowNormalThNum = normalTitleNum + 1;
		HSSFRow rowNormalTh = sheet.createRow(rowNormalThNum);
		rowNormalTh.setHeightInPoints(40);
		rowNormalTh.createCell(0).setCellValue("识别类型");
		rowNormalTh.createCell(1).setCellValue("点位名称");
		rowNormalTh.createCell(2).setCellValue("识别结果");
		rowNormalTh.createCell(3).setCellValue("告警等级");
		rowNormalTh.createCell(4).setCellValue("识别时间");
		rowNormalTh.createCell(5).setCellValue("采集信息");

		int rowNormalListNum = rowNormalThNum + 1;
		List<PointHistory> normalPointHistory = pointHistoryService
				.listNormalPointHistoryByTaskHistoryId(taskHistory.getId());
		for (PointHistory pointHistory : normalPointHistory) {
			Point point = pointMapper.selectByPrimaryKey(pointHistory.getPointId());
			if (point != null && pointHistory != null) {
				ReconType reconType = reconTypeMapper.selectByPrimaryKey(point.getReconTypeId());
				String reconTypeName = "未知";
				if (reconType != null) {
					reconTypeName = reconType.getName();
				}

				HSSFRow normalRow = sheet.createRow(rowNormalListNum);
				normalRow.createCell(0).setCellValue(reconTypeName);
				normalRow.createCell(1).setCellValue(point.getName());
				normalRow.createCell(2).setCellValue(pointHistory.getValue());
				normalRow.createCell(3).setCellValue(ProjectStatusUtils.getAlarmLevel(0));
				normalRow.createCell(4).setCellValue(sdf.format(pointHistory.getCreateTime()));

				normalRow.setHeightInPoints(40);

				HSSFCell cell = normalRow.createCell(5);
				cell.setCellStyle(ProjectExcelUtils.setExcelLinkStyle(workbook));
				// 如果是红外
				if (point.getSaveTypeId().intValue() == 1 || point.getSaveTypeId().intValue() == 4) {
					String link = ProjectUrlConvert.covert2HttpUrl(pointHistory.getFlirPic());
					String cellValue = "红外图片";
					cell.setCellFormula("HYPERLINK(\"" + link + "\",\"" + cellValue + "\")");
				} else if (point.getSaveTypeId().intValue() == 2) {
					String link = ProjectUrlConvert.covert2HttpUrl(pointHistory.getCameraPic());
					String cellValue = "可见光图片";
					cell.setCellFormula("HYPERLINK(\"" + link + "\",\"" + cellValue + "\")");
				} else {
					normalRow.createCell(5).setCellValue("未知");
				}

				rowNormalListNum++;
			}
		}

		// 0代表写入 1代表单条导出
		if (status == 0) {
			try {
				String fullFilePath = excelPath + File.separator + fileName;
				fileNames.add(fullFilePath);
				ProjectExcelUtils.write(workbook, fullFilePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			ProjectExcelUtils.down(response, fileName, workbook);
		}
	}

	/**
	 * 通过机器人id和某天的执行时间筛选任务历史数据
	 * 
	 * @param executeTime
	 * @param robotId
	 * @return
	 */
	private List<Map<String, Object>> findTaskHistoryByTimeAndRobotId(String executeTime, Integer robotId) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String startTime = executeTime;
		String endTime = "";
		try {
			Date endDate = formatter.parse(startTime);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(endDate);
			calendar.add(Calendar.DAY_OF_MONTH, +1);

			endDate = calendar.getTime();
			endTime = formatter.format(endDate);
		} catch (ParseException e) {
			log.info(e.getMessage());
		}

		List<TaskHistory> taskHistories = taskHistoryMapper.getTaskHistoryByRobotIdAndTaskStartDateBetween(robotId,
				startTime, endTime);
		List<Map<String, Object>> maps = new ArrayList<>();

		for (TaskHistory taskHistory : taskHistories) {
			Map<String, Object> map = ProjectObjectUtils.objectToMap(taskHistory);
			map.put("task", taskMapper.selectByPrimaryKey(taskHistory.getTaskId()));
			maps.add(map);
		}

		return maps;
	}

	/**
	 * 获取某个执行记录的点位审核状态
	 * 
	 * @param taskHistoryId
	 * @return
	 */
	private Integer getPointCheckStatus(Integer taskHistoryId) {
		int pointCheckStatus = 0;

		// 首先判断point_history表是否有未审核数据
		PointHistory record = new PointHistory();
		record.setTaskHistoryId(taskHistoryId);
		record.setCheckStatus(ProjectConsts.CHECK_STATUS_NO);
		Integer pointHistoryNotCheckNum = pointHistoryMapper.selectCount(record);

		if (pointHistoryNotCheckNum > 0) {
			pointCheckStatus = 1;
			return pointCheckStatus;
		}

		// 判断point_alarm_history表是否有未审核数据
		PointAlarmHistory pointAlarmHistoryCondition = new PointAlarmHistory();
		pointAlarmHistoryCondition.setTaskHistoryId(taskHistoryId);
		pointAlarmHistoryCondition.setCheckStatus(ProjectConsts.CHECK_STATUS_NO);
		Integer pointAlarmHistoryNotCheckNum = pointAlarmHistoryMapper.selectCount(pointAlarmHistoryCondition);

		if (pointAlarmHistoryNotCheckNum > 0) {
			pointCheckStatus = 2;
			return pointCheckStatus;
		}

		return pointCheckStatus;
	}
}
