package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PermissionRoleMapper;
import com.ydrobot.powerplant.model.PermissionRole;
import com.ydrobot.powerplant.service.PermissionRoleService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PermissionRoleServiceImpl extends AbstractService<PermissionRole> implements PermissionRoleService {
    @Resource
    private PermissionRoleMapper permissionRoleMapper;

}
