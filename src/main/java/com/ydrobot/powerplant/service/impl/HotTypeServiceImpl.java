package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.HotTypeMapper;
import com.ydrobot.powerplant.model.HotType;
import com.ydrobot.powerplant.service.HotTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class HotTypeServiceImpl extends AbstractService<HotType> implements HotTypeService {
    @Resource
    private HotTypeMapper hotTypeMapper;

}
