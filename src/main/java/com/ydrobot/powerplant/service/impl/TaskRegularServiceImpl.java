package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.TaskRegularMapper;
import com.ydrobot.powerplant.model.TaskRegular;
import com.ydrobot.powerplant.model.condition.TaskRegularQueryParam;
import com.ydrobot.powerplant.model.request.TaskRegularRequest;
import com.ydrobot.powerplant.model.request.UpdateTaskRegularBatchRequest;
import com.ydrobot.powerplant.service.TaskRegularService;
import com.ydrobot.powerplant.service.TaskService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class TaskRegularServiceImpl extends AbstractService<TaskRegular> implements TaskRegularService {
    @Resource
    private TaskRegularMapper taskRegularMapper;
    @Resource
    private TaskService taskService;

    @Override
	public List<TaskRegular> listTaskRegular(TaskRegularQueryParam queryParam, Integer page, Integer size) {

        PageHelper.startPage(page, size);
    		Condition condition = new Condition(TaskRegular.class);
        Criteria criteria = condition.createCriteria();

        if (queryParam.getTaskId() != null) {
            criteria.andEqualTo("taskId", queryParam.getTaskId());
        }
        
        condition.setOrderByClause("create_time desc");
        
        return findByCondition(condition);
	}

	@Override
    public void batchUpdate(UpdateTaskRegularBatchRequest updateTaskRegularBatchRequest) {
        List<TaskRegularRequest> taskRegularRequests = updateTaskRegularBatchRequest.getTaskRegularRequests();

        TaskRegular taskRegularCondition = new TaskRegular();
        taskRegularCondition.setTaskId(updateTaskRegularBatchRequest.getTaskId());
        taskRegularMapper.delete(taskRegularCondition);

        Integer level = taskService.getTaskLevel(updateTaskRegularBatchRequest.getTaskId());
        for (TaskRegularRequest taskRegularRequest : taskRegularRequests) {
            TaskRegular taskRegular = new TaskRegular();
            taskRegular.setRobotId(updateTaskRegularBatchRequest.getRobotId());
            taskRegular.setTaskId(updateTaskRegularBatchRequest.getTaskId());
            taskRegular.setTaskLevel(level);
            taskRegular.setExecuteTime(taskRegularRequest.getExecuteTime());
            save(taskRegular);
        }
    }
}
