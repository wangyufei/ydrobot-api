package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.SysPointMapper;
import com.ydrobot.powerplant.model.SysPoint;
import com.ydrobot.powerplant.service.SysPointService;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class SysPointServiceImpl extends AbstractService<SysPoint> implements SysPointService {
    @Resource
    private SysPointMapper sysPointMapper;

	@Override
	public void saveSysPoint(SysPoint sysPoint) {
		SysPoint record = new SysPoint();
		record.setName(sysPoint.getName());
		List<SysPoint> sysPoints = sysPointMapper.select(record);
		
		if (sysPoints.size()>0) {
			throw new ServiceException("该点位已存在，不可重复添加！");
		}
		
		save(sysPoint);
	}
    
}
