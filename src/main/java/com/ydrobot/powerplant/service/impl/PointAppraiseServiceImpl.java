package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PointAppraiseMapper;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.request.UpdatePointAppraiseStatusRequest;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.PointAppraiseHistoryService;
import com.ydrobot.powerplant.service.PointAppraiseService;
import com.ydrobot.powerplant.core.service.AbstractService;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/06/05.
 */
@Service
@Transactional
public class PointAppraiseServiceImpl extends AbstractService<PointAppraise> implements PointAppraiseService {
    @Resource
    private PointAppraiseMapper pointAppraiseMapper;
    @Resource
    private DataSyncService dataSyncService;
    @Resource
    private PointAppraiseHistoryService pointAppraiseHistoryService;

	@Override
	public void updatePointAppraiseStatus(PointAppraise pointAppraise, UpdatePointAppraiseStatusRequest request) {
		pointAppraise.setStatus(request.getStatus());
		
		// 判断是否处理完成，处理完成之后将评价状态恢复正常
		if (request.getStatus() == 2) {
			pointAppraise.setAppraiseStatus(0);
			
			//写入评价记录
			PointAppraiseHistory pointAppraiseHistory = new PointAppraiseHistory();
			BeanUtils.copyProperties(pointAppraise, pointAppraiseHistory);
			pointAppraiseHistoryService.save(pointAppraiseHistory);
		}
		
		update(pointAppraise);
		dataSyncService.pointAppraiseSync(pointAppraise);
	}
}
