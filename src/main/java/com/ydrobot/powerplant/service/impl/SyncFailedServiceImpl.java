package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.SyncFailedMapper;
import com.ydrobot.powerplant.model.SyncFailed;
import com.ydrobot.powerplant.service.SyncFailedService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

/**
 * Created by Wyf on 2020/07/15.
 */
@Service
@Transactional
public class SyncFailedServiceImpl extends AbstractService<SyncFailed> implements SyncFailedService {
	@Resource
	private SyncFailedMapper syncFailedMapper;

	@Override
	public void saveSyncFailed(String tableName, Integer dataId) {
		// 查询该数据是否已经存在
		SyncFailed record = new SyncFailed();
		record.setTableName(tableName);
		record.setDataId(dataId);
		List<SyncFailed> syncFaileds = syncFailedMapper.select(record);
		if (syncFaileds.isEmpty()) {
			// 将同步失败的数据写入到数据库
			SyncFailed syncFailed = new SyncFailed();
			syncFailed.setDataId(dataId);
			syncFailed.setTableName(tableName);
			save(syncFailed);
		}
	}

    
}
