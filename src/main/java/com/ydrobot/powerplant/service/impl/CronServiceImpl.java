package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.CronMapper;
import com.ydrobot.powerplant.model.Cron;
import com.ydrobot.powerplant.service.CronService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/07/15.
 */
@Service
@Transactional
public class CronServiceImpl extends AbstractService<Cron> implements CronService {
    @Resource
    private CronMapper cronMapper;

}
