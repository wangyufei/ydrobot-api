package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PmsPatrolSchemeMapper;
import com.ydrobot.powerplant.model.PmsPatrolScheme;
import com.ydrobot.powerplant.service.PmsPatrolSchemeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/01/10.
 */
@Service
@Transactional
public class PmsPatrolSchemeServiceImpl extends AbstractService<PmsPatrolScheme> implements PmsPatrolSchemeService {
    @Resource
    private PmsPatrolSchemeMapper pmsPatrolSchemeMapper;

}
