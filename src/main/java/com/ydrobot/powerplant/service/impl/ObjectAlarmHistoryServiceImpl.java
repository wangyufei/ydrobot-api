package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.ObjectAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.dao.TaskPointMapper;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.ObjectAlarmHistoryService;
import com.ydrobot.powerplant.service.TaskService;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.service.AbstractService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

/**
 * Created by Wyf on 2020/06/01.
 */
@Service
@Transactional
public class ObjectAlarmHistoryServiceImpl extends AbstractService<ObjectAlarmHistory>
		implements ObjectAlarmHistoryService {
	@Resource
	private ObjectAlarmHistoryMapper objectAlarmHistoryMapper;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private TaskMapper taskMapper;
	@Resource
	private TaskPointMapper taskPointMapper;
	@Resource
	private DataSyncService dataSyncService;
	@Resource
	private TaskService taskService;

	@Transactional
	@Override
	public void saveObjectAlarmHistory(ObjectAlarmHistory objectAlarmHistory) {

		if (objectAlarmHistory.getObjectId() == null) {
			throw new ServiceException("对象id不能为空");
		}

		// 获取该对象的组件列表
		Point pointRecord = new Point();
		pointRecord.setObjectId(objectAlarmHistory.getObjectId());
		List<Point> points = pointMapper.select(pointRecord);

		if (points.isEmpty()) {
			throw new ServiceException("该设备下无部件");
		}

		List<Integer> pointIds = new ArrayList<Integer>();
		for (Point point : points) {
			pointIds.add(point.getId());
		}

		Task task = taskService.saveEmergencyTask(pointIds, 6000);
		
		// 创建设备告警记录
		objectAlarmHistory.setTaskId(task.getId());
		objectAlarmHistory.setRobotId(1);
		objectAlarmHistoryMapper.insertSelective(objectAlarmHistory);

		// 同步设备告警记录数据
		dataSyncService.objectAlarmHistorySync(objectAlarmHistory);
		dataSyncService.TaskSync(task);
	}
}
