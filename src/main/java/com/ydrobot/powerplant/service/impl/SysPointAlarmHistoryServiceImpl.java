package com.ydrobot.powerplant.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.dao.AlarmTypeMapper;
import com.ydrobot.powerplant.dao.RobotMapper;
import com.ydrobot.powerplant.dao.SysPointAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.SysPointMapper;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.condition.SysPointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.SysPointAlarmHistoryResult;
import com.ydrobot.powerplant.model.request.SysPointAlarmHistoryBatchCheckRequest;
import com.ydrobot.powerplant.service.SysPointAlarmHistoryService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class SysPointAlarmHistoryServiceImpl extends AbstractService<SysPointAlarmHistory>
		implements SysPointAlarmHistoryService {
	@Resource
	private SysPointAlarmHistoryMapper sysPointAlarmHistoryMapper;
	@Resource
	private RobotMapper robotMapper;
	@Resource
	private SysPointMapper sysPointMapper;
	@Resource
	private AlarmTypeMapper alarmTypeMapper;

	@Override
	public List<SysPointAlarmHistoryResult> listSysPointAlarmHistory(SysPointAlarmHistoryQueryParam queryParam,
			Integer page, Integer size) {
		PageHelper.startPage(page, size);
		return sysPointAlarmHistoryMapper.getList(queryParam);
	}

	@Override
	public void batchCheck(SysPointAlarmHistoryBatchCheckRequest request) {
		List<Integer> idList = ProjectCommonUtils.getSplitValInt(request.getIds(), ",");

		for (Integer id : idList) {
			SysPointAlarmHistory sysPointAlarmHistory = findById(id);
			if (sysPointAlarmHistory != null) {
				sysPointAlarmHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
				sysPointAlarmHistory.setOperaterId(ThreadCache.getUser().getId());
				sysPointAlarmHistory.setCheckTime(new Date());
				update(sysPointAlarmHistory);
			}
		}
	}

	@Override
	public Integer getAlarmNum() {
		SysPointAlarmHistory record = new SysPointAlarmHistory();
		record.setCheckStatus(ProjectConsts.CHECK_STATUS_NO);
		return sysPointAlarmHistoryMapper.selectCount(record);
	}
	
	
}
