package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.FaceTypeMapper;
import com.ydrobot.powerplant.model.FaceType;
import com.ydrobot.powerplant.service.FaceTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class FaceTypeServiceImpl extends AbstractService<FaceType> implements FaceTypeService {
    @Resource
    private FaceTypeMapper faceTypeMapper;

}
