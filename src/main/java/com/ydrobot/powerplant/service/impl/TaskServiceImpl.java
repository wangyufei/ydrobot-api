package com.ydrobot.powerplant.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.core.utils.ProjectDateUtils;
import com.ydrobot.powerplant.dao.InspectTypeMapper;
import com.ydrobot.powerplant.dao.PathPlanningMapper;
import com.ydrobot.powerplant.dao.PointHistoryMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.StopPointMapper;
import com.ydrobot.powerplant.dao.TaskCycleMapper;
import com.ydrobot.powerplant.dao.TaskHistoryMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.dao.TaskPointMapper;
import com.ydrobot.powerplant.dao.TaskRegularMapper;
import com.ydrobot.powerplant.model.InspectType;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.StopPoint;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskCycle;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.TaskRegular;
import com.ydrobot.powerplant.model.condition.TaskQueryParam;
import com.ydrobot.powerplant.model.dto.TaskResult;
import com.ydrobot.powerplant.model.condition.TaskExecutePlanQueryParam;
import com.ydrobot.powerplant.model.condition.TaskMonthShowQueryParam;
import com.ydrobot.powerplant.model.request.DeleteExecutePlanBatchRequest;
import com.ydrobot.powerplant.model.request.DeleteExecutePlanRequest;
import com.ydrobot.powerplant.model.request.TaskPathRequest;
import com.ydrobot.powerplant.model.request.TaskRequest;
import com.ydrobot.powerplant.model.status.TaskStatus;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.TaskPointService;
import com.ydrobot.powerplant.service.TaskService;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class TaskServiceImpl extends AbstractService<Task> implements TaskService {

	private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

	@Resource
	private TaskMapper taskMapper;
	@Resource
	private TaskPointMapper taskPointMapper;
	@Resource
	private TaskPointService taskPointService;
	@Resource
	private TaskHistoryMapper taskHistoryMapper;
	@Resource
	private TaskRegularMapper taskRegularMapper;
	@Resource
	private TaskCycleMapper taskCycleMapper;
	@Resource
	private PathPlanningMapper pathPlanningMapper;
	@Resource
	private PointHistoryMapper pointHistoryMapper;
	@Resource
	private InspectTypeMapper inspectTypeMapper;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private StopPointMapper stopPointMapper;
	@Resource
	private DataSyncService dataSyncService;

	@Override
	public List<TaskResult> listTask(TaskQueryParam queryParam, Integer page, Integer size) {
		PageHelper.startPage(page, size);
		return taskMapper.getList(queryParam);
	}

	@Override
	public void deleteTask(Task task) {
		task.setIsDelete(ProjectConsts.DELETE_STATUS_YES);
		update(task);
		
		// 删除该任务关联的周期任务和定期任务
		TaskCycle taskCycleRecord = new TaskCycle();
		taskCycleRecord.setTaskId(task.getId());
		taskCycleMapper.delete(taskCycleRecord);
		
		TaskRegular taskRegular = new TaskRegular();
		taskRegular.setTaskId(task.getId());
		taskRegularMapper.delete(taskRegular);
	}

	@Override
	public List<TaskPoint> listTaskPoint(Integer taskId) {
		TaskPoint taskPoint = new TaskPoint();
		taskPoint.setTaskId(taskId);
		return taskPointMapper.select(taskPoint);
	}

	@Override
	public Task saveTask(TaskRequest taskRequest) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Task task = new Task();

		if (taskRequest.getIsCustom() != null && taskRequest.getIsCustom() == 0) {
			taskRequest.setName(taskRequest.getName() + sdf.format(new Date()));
		}
		BeanUtils.copyProperties(taskRequest, task);
		task.setIsUpdate(ProjectConsts.UPDATE_STATUS_YES);
		task.setOperaterId(ThreadCache.getUser().getId());
		task.setUpdateTime(new Date());
		save(task);

		if (StringUtils.isNotBlank(taskRequest.getPoints())) {
			updateTaskPoint(task, ProjectCommonUtils.getSplitValInt(taskRequest.getPoints(), ","));
		}

		dataSyncService.TaskSync(task);
		return task;
	}

	@Override
	public Task updateTask(Task task, TaskRequest taskRequest) {
		BeanUtils.copyProperties(taskRequest, task);
		task.setIsUpdate(ProjectConsts.UPDATE_STATUS_YES);
		task.setOperaterId(ThreadCache.getUser().getId());
		task.setUpdateTime(new Date());
		update(task);
		updateTaskPoint(task, ProjectCommonUtils.getSplitValInt(taskRequest.getPoints(), ","));
		dataSyncService.TaskSync(task);
		return task;
	}

	@Override
	public Boolean getEditStatus(Task task) {
		Boolean flag = true;
		List<TaskHistory> taskHistories = taskHistoryMapper.getTaskHistoryByStatus(task.getId(),
				TaskStatus.PAUSE.getValue(), TaskStatus.CURRENT_EXECUTE.getValue());
		if (!taskHistories.isEmpty()) {
			flag = false;
		}
		return flag;
	}

	@Override
	public PathPlanning getTaskCurrentPath(Integer taskHistoryId) {
		PathPlanning pathPlanning = pathPlanningMapper.getPathByTaskHistoryId(taskHistoryId);
		return pathPlanning;
	}

	@Override
	public List<Map<String, Object>> listTaskMonthShow(TaskMonthShowQueryParam queryParam) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String startTime = ProjectDateUtils.parse6(queryParam.getExecuteTime());
		String endTime = getEndTime(startTime);
		Integer robotId = queryParam.getRobotId();

		List<Map<String, Object>> maps = new ArrayList<>();

		List<String> dayBetweenDates = ProjectDateUtils.getDayBetweenDates(startTime, endTime);
		List<Map<String, Object>> mapTmp = getTaskShow(robotId, startTime, endTime);

		for (String dayBetweenDate : dayBetweenDates) {
			List<Map<String, Object>> list = new ArrayList<>();

			for (Map<String, Object> map : mapTmp) {
				String executTime = sdf.format((Date) map.get("executTime"));
				if (executTime.startsWith(dayBetweenDate)) {
					list.add(map);
				}
			}

			ProjectCommonUtils.listSortByTime(list);
			Map<String, Object> map = new HashMap<>();
			map.put("groupDate", dayBetweenDate);
			map.put("list", list);
			map.put("count", list.size());
			maps.add(map);
		}
		return maps;
	}

	@Override
	public List<Map<String, Object>> listTaskExecutePlan(TaskExecutePlanQueryParam queryParam) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (StringUtils.isBlank(queryParam.getEndTime())) {
			queryParam.setEndTime(sdf.format(new Date()));
		}

		if (StringUtils.isBlank(queryParam.getStartTime())) {
			queryParam.setStartTime(getStartTime(queryParam.getEndTime()));
		}

		String startTime = queryParam.getStartTime();
		String endTime = queryParam.getEndTime();

		if (startTime.length() > 10) {
			startTime = ProjectDateUtils.format4(ProjectDateUtils.parse4(startTime));
		} else {
			startTime = ProjectDateUtils.format10(ProjectDateUtils.parse10(startTime));
		}

		if (endTime.length() > 10) {
			endTime = ProjectDateUtils.format4(new Date(ProjectDateUtils.parse4(endTime).getTime() - 1000));
		} else {
			endTime = ProjectDateUtils.format10(ProjectDateUtils.parse10(endTime));
		}

		Integer robotId = queryParam.getRobotId();
		List<Map<String, Object>> maps = new ArrayList<>();
		List<Map<String, Object>> mapTmp = getTaskShow(robotId, startTime, endTime);

		Collections.sort(mapTmp, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				Date o1Date = (Date) o1.get("executTime");
				Date o2Date = (Date) o2.get("executTime");
				String o1Time = String.valueOf(o1Date.getTime());
				String o2Time = String.valueOf(o2Date.getTime());

				return o1Time.compareTo(o2Time);
			}
		});

		for (Map<String, Object> map : mapTmp) {
			// 获取某个任务的巡检点数
			if (map.get("taskId") != null && map.get("taskId") != "") {
				TaskPoint record = new TaskPoint();
				record.setTaskId(Integer.valueOf(map.get("taskId").toString()));
				Integer inspectPointNum = taskPointMapper.selectCount(record);
				map.put("inspectPointNum", inspectPointNum);
			}

			if (queryParam.getTaskName() == null && queryParam.getTaskStatus() == null) {
				maps.add(map);
			} else if (queryParam.getTaskName() != null && queryParam.getTaskStatus() != null) {
				String taskName = (String) map.get("taskName");
				String taskStatus = String.valueOf(map.get("taskStatus"));

				if (taskName.contains(queryParam.getTaskName()) && queryParam.getTaskStatus().contains(taskStatus)) {
					maps.add(map);
				}
			} else if (queryParam.getTaskName() != null && queryParam.getTaskStatus() == null) {
				String taskName = (String) map.get("taskName");
				if (taskName.contains(queryParam.getTaskName())) {
					maps.add(map);
				}
			} else if (queryParam.getTaskName() == null && queryParam.getTaskStatus() != null) {
				String taskStatus = String.valueOf(map.get("taskStatus"));
				if (queryParam.getTaskStatus().contains(taskStatus)) {
					maps.add(map);
				}
			}

		}

		return maps;
	}

	@Override
	public Integer getTaskLevel(Integer taskId) {
		Integer level = 0;
		Task task = findById(taskId);
		if (task != null) {
			InspectType inspectType = inspectTypeMapper.selectByPrimaryKey(task.getInspectTypeId());
			if (inspectType != null) {
				level = inspectType.getLevel();
			}
		}
		return level;
	}

	@Override
	public void deleteExecutePlan(DeleteExecutePlanBatchRequest deleteExecutePlanBatchRequest) {
		List<DeleteExecutePlanRequest> deleteExecutePlanRequests = deleteExecutePlanBatchRequest
				.getDeleteExecutePlanRequests();

		for (DeleteExecutePlanRequest deleteExecutePlanRequest : deleteExecutePlanRequests) {
			if (deleteExecutePlanRequest.getId() != null && deleteExecutePlanRequest.getTaskType() != null) {
				Integer id = deleteExecutePlanRequest.getId().intValue();
				Integer taskType = deleteExecutePlanRequest.getTaskType().intValue();
				if (taskType == ProjectConsts.TASK_TYPE_HISTORY) {
					TaskHistory taskHistory = taskHistoryMapper.selectByPrimaryKey(id);
					if (taskHistory != null) {
						taskHistoryMapper.delete(taskHistory);
					}
				} else if (taskType == ProjectConsts.TASK_TYPE_REGULAR) {
					TaskRegular taskRegular = taskRegularMapper.selectByPrimaryKey(id);
					if (taskRegular != null) {
						taskRegularMapper.delete(taskRegular);
					}

				} else if (taskType == ProjectConsts.TASK_TYPE_CYCLE || taskType == ProjectConsts.TASK_TYPE_INTERVAL) {
					TaskCycle taskCycle = taskCycleMapper.selectByPrimaryKey(id);
					if (taskCycle != null) {
						taskCycleMapper.delete(taskCycle);
					}
				}
			}
		}

	}

	@Override
	public boolean getTaskExecutStatus(Integer id) {
		boolean flag = true;

		Task task = findById(id);
		if (task != null) {
			Integer currentLevel = getTaskLevel(id);
			List<TaskHistory> taskHistories = taskHistoryMapper.getCurrentTaskByRobotId(task.getRobotId());
			for (TaskHistory taskHistory : taskHistories) {
				Integer level = getTaskLevel(taskHistory.getTaskId());
				if (currentLevel >= level) {
					flag = false;
					break;
				}
			}
		}

		return flag;
	}

	@Override
	public void updateTaskPath(Integer id, TaskPathRequest taskPathRequest) throws ServiceException {

		Task task = findById(id);
		if (task != null) {
			PathPlanning pathPlanning = pathPlanningMapper.getPathByTaskId(id, new Byte("0"));
			if (pathPlanning != null) {
				pathPlanning.setPath(taskPathRequest.getPath());
				pathPlanningMapper.updateByPrimaryKey(pathPlanning);
			} else {
				PathPlanning pathPlanningNew = new PathPlanning();
				pathPlanningNew.setTaskId(id);
				pathPlanningNew.setPath(taskPathRequest.getPath());
				pathPlanningNew.setFlag(new Byte("0"));
				pathPlanningNew.setRobotId(task.getRobotId());
				pathPlanningMapper.insert(pathPlanningNew);
			}
		}
	}

	@Override
	public PathPlanning getTaskPath(Integer id) {
		PathPlanning pathPlanning = pathPlanningMapper.getPathByTaskId(id, new Byte("0"));
		return pathPlanning;
	}

	@Override
	public List<Point> listTaskCheckPoints(Integer id) {
		return pointMapper.getTaskPoint(id);
	}

	@Override
	public List<StopPoint> listTaskStopPoint(Integer id) {
		return stopPointMapper.getTaskStopPoint(id);
	}

	@Override
	public List<Task> listTaskByInspectTypeIds(String inspectTypeIds) {
		List<Integer> inspectTypeIdList = ProjectCommonUtils.getSplitValInt(inspectTypeIds, ",");
		Condition condition = new Condition(Task.class);
		Criteria criteria = condition.createCriteria();
		criteria.andIn("inspectTypeId", inspectTypeIdList);
		return taskMapper.selectByCondition(condition);
	}

	@Override
	public List<Task> listTaskByName(String taskName) {
		Condition condition = new Condition(Task.class);
		Criteria criteria = condition.createCriteria();
		criteria.andLike("name", "%" + taskName + "%");
		return taskMapper.selectByCondition(condition);
	}

	@Transactional
	@Override
	public Task saveEmergencyTask(List<Integer> pointIds, Integer inspectTypeId) {
		// 6000:自动巡检 7000:手动巡检
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Task task = new Task();
		task.setInspectTypeId(inspectTypeId);
		task.setName("应急巡检" + sdf.format(new Date()));
		task.setIsCustom(new Byte("1"));
		task.setRobotId(1);
		task.setUpdateTime(new Date());
		taskMapper.insertSelective(task);

		List<TaskPoint> taskPoints = new ArrayList<>();

		for (Integer pointId : pointIds) {
			TaskPoint taskPoint = new TaskPoint();
			taskPoint.setTaskId(task.getId());
			taskPoint.setPointId(pointId);
			taskPoints.add(taskPoint);
		}
		taskPointMapper.insertList(taskPoints);
		return task;
	}

	@Override
	public void updateAllTaskUpdateStatus() {
		taskMapper.updateAllTaskIsUpdateStatus();
	}

	/**
	 * 更新任务巡检点
	 * 
	 * @param taskDef
	 * @param pointIds
	 */
	private void updateTaskPoint(Task task, List<Integer> pointIds) {
		TaskPoint taskPointCondition = new TaskPoint();
		taskPointCondition.setTaskId(task.getId());
		taskPointMapper.delete(taskPointCondition);
		for (Integer pointId : pointIds) {
			Point point = pointMapper.selectByPrimaryKey(pointId);
			if (point != null) {
				TaskPoint taskPoint = new TaskPoint();
				taskPoint.setTaskId(task.getId());
				taskPoint.setPointId(pointId);
				taskPointService.save(taskPoint);
			}
		}
	}

	/**
	 * 获取历史数据
	 * 
	 * @param robotId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	private List<Map<String, Object>> getHistoryData(Integer robotId, String startTime, String endTime) {
		List<TaskHistory> taskHistories = taskHistoryMapper.getTaskHistoryByRobotIdAndTaskStartDateBetween(robotId,
				startTime, endTime);
		List<Map<String, Object>> maps = new ArrayList<>();

		for (TaskHistory taskHistory : taskHistories) {
			Task task = taskMapper.selectByPrimaryKey(taskHistory.getTaskId());
			Map<String, Object> map = new HashMap<>();
			map.put("id", taskHistory.getId());
			map.put("taskId", taskHistory.getTaskId());
			map.put("executTime", taskHistory.getTaskStartTime());
			map.put("taskName", "");
			map.put("taskStatus", taskHistory.getTaskStatus());
			map.put("inspectTypeId", "");
			map.put("taskType", ProjectConsts.TASK_TYPE_HISTORY);
			if (task != null) {
				map.put("taskName", task.getName());
				map.put("inspectTypeId", task.getInspectTypeId());
			}
			maps.add(map);
		}

		return maps;
	}

	/**
	 * 获取定时任务状态列表
	 * 
	 * @param robotId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	private List<Map<String, Object>> findTaskRegular(Integer robotId, String startTime, String endTime) {
		List<Map<String, Object>> maps = new ArrayList<>();
		List<TaskRegular> taskRegulars = taskRegularMapper.findByRobotIdAndTaskStartDateBetween(robotId, startTime,
				endTime);
		for (TaskRegular taskRegular : taskRegulars) {
			Map<String, Object> map = new HashMap<>();
			map.put("id", taskRegular.getId());
			map.put("taskId", taskRegular.getTaskId());
			map.put("executTime", taskRegular.getExecuteTime());
			map.put("taskName", "");
			map.put("inspectTypeId", "");
			map.put("taskStatus", TaskStatus.UNEXECUTE.getValue());
			map.put("taskType", ProjectConsts.TASK_TYPE_REGULAR);
			Task task = taskMapper.selectByPrimaryKey(taskRegular.getTaskId());
			if (task != null) {
				map.put("taskName", task.getName());
				map.put("inspectTypeId", task.getInspectTypeId());
			}

			maps.add(map);
		}
		return maps;
	}

	/**
	 * 获取周期任务
	 * 
	 * @param robotId
	 * @param dayBetweenDates
	 * @return
	 */
	private List<Map<String, Object>> findTaskCycle(Integer robotId, List<String> dayBetweenDates) {
		List<Map<String, Object>> maps = new ArrayList<>();
		TaskCycle taskCycleCondition = new TaskCycle();
		taskCycleCondition.setRobotId(robotId);
		taskCycleCondition.setExecuteType(new Byte("0"));
		List<TaskCycle> taskCycles = taskCycleMapper.select(taskCycleCondition);

		for (String dayBetweenDate : dayBetweenDates) {
			Integer month = ProjectDateUtils.getMonth(dayBetweenDate);
			Integer week = ProjectDateUtils.getWeek(dayBetweenDate);
			week = week - 1;
			for (TaskCycle taskCycle : taskCycles) {
				List<Integer> taskMonthList = new ArrayList<>();
				List<Integer> taskWeekList = new ArrayList<>();
				if (StringUtils.isNotEmpty(taskCycle.getMonth())) {
					taskMonthList = ProjectCommonUtils.getSplitValInt(taskCycle.getMonth(), ",");
				}

				if (StringUtils.isNotBlank(taskCycle.getWeek())) {
					taskWeekList = ProjectCommonUtils.getSplitValInt(taskCycle.getWeek(), ",");
				}

				if (taskMonthList.contains(month) && taskWeekList.contains(week)) {
					Map<String, Object> map = new HashMap<>();
					map.put("id", taskCycle.getId());
					map.put("taskId", taskCycle.getTaskId());
					map.put("executTime", ProjectDateUtils.parse4(dayBetweenDate + " " + taskCycle.getExecuteTime()));
					map.put("taskName", "");
					map.put("inspectTypeId", "");
					map.put("taskStatus", TaskStatus.UNEXECUTE.getValue());
					map.put("taskType", ProjectConsts.TASK_TYPE_CYCLE);
					Task task = taskMapper.selectByPrimaryKey(taskCycle.getTaskId());
					if (task != null) {
						map.put("taskName", task.getName());
						map.put("inspectTypeId", task.getInspectTypeId());
					}
					maps.add(map);
				}
			}
		}

		return maps;
	}

	/**
	 * 获取间隔任务时间列表
	 * 
	 * @param robotId
	 * @param dayBetweenDates
	 * @return
	 */
	private List<Map<String, Object>> findTaskCycleByInterval(Integer robotId, String startTime,
			List<String> dayBetweenDates) {
		List<Map<String, Object>> maps = new ArrayList<>();

		Date startDate = null;
		if (startTime.length() > 10) {
			startDate = ProjectDateUtils.parse4(startTime);
		} else {
			startDate = ProjectDateUtils.parse10(startTime);
		}

		Condition condition = new Condition(TaskCycle.class);
		Criteria criteria = condition.createCriteria();
		criteria.andEqualTo("robotId", robotId);
		criteria.andEqualTo("executeType", 1);
		criteria.andGreaterThanOrEqualTo("endTime", startTime);
		List<TaskCycle> taskCycles = taskCycleMapper.selectByCondition(condition);

		for (TaskCycle taskCycle : taskCycles) {
			List<Date> dates = getIntervalExecuteTime(taskCycle);
			for (Date taskExecutDate : dates) {
				String taskExecutDateStr = ProjectDateUtils.format10(taskExecutDate);
				if (dayBetweenDates.contains(taskExecutDateStr) && taskExecutDate.getTime() > startDate.getTime()) {
					Task task = taskMapper.selectByPrimaryKey(taskCycle.getTaskId());
					Map<String, Object> map = new HashMap<>();
					map.put("id", taskCycle.getId());
					map.put("taskId", taskCycle.getTaskId());
					map.put("executTime", taskExecutDate);
					map.put("taskName", "");
					map.put("inspectTypeId", "");
					map.put("taskStatus", TaskStatus.UNEXECUTE.getValue());
					map.put("taskType", ProjectConsts.TASK_TYPE_INTERVAL);
					if (task != null) {
						map.put("taskName", task.getName());
						map.put("inspectTypeId", task.getInspectTypeId());
					}
					maps.add(map);
				}
			}
		}

		return maps;
	}

	/**
	 * 通过开始时间获取结束时间
	 * 
	 * @param startTime
	 * @return
	 */
	private String getEndTime(String startTime) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		String endTime = "";
		try {
			Date endDate = formatter.parse(startTime);

			Calendar year = Calendar.getInstance();
			Calendar month = Calendar.getInstance();

			year.setTime(endDate);
			month.setTime(endDate);

			endTime = ProjectDateUtils.getLastDayOfMonth(year.get(Calendar.YEAR), month.get(Calendar.MONTH) + 1);
		} catch (ParseException e) {

			System.out.println("Unparseable using " + formatter);
		}
		return endTime;
	}

	/**
	 * 获取开始时间
	 * 
	 * @param endTime
	 * @return
	 */
	private String getStartTime(String endTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startTime = null;
		try {
			Date startDate = sdf.parse(endTime);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			calendar.add(Calendar.MONTH, -1);
			startTime = sdf.format(calendar.getTime());
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return startTime;
	}

	/**
	 * 获取间隔执行时间列表
	 * 
	 * @param taskCycle
	 * @return
	 */
	private List<Date> getIntervalExecuteTime(TaskCycle taskCycle) {
		List<Date> executeTimeList = new ArrayList<>();
		executeTimeList.add(taskCycle.getStartTime());
		Date executeTime = taskCycle.getStartTime();
		while ((taskCycle.getEndTime().getTime() - executeTime.getTime()) > 0) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(executeTime);
			// 判断间隔任务的时间单位
			// 间隔单位 0:分钟 1:小时 2:天
			if (0 == taskCycle.getIntervalUnit().intValue()) {
				calendar.add(Calendar.MINUTE, taskCycle.getIntervalTime());
			}

			if (1 == taskCycle.getIntervalUnit().intValue()) {
				calendar.add(Calendar.HOUR, taskCycle.getIntervalTime());
			}

			if (2 == taskCycle.getIntervalUnit().intValue()) {
				calendar.add(Calendar.DATE, taskCycle.getIntervalTime());
			}
			executeTime = calendar.getTime();
			executeTimeList.add(calendar.getTime());
		}

		return executeTimeList;
	}

	/**
	 * 获取任务展示数据
	 * 
	 * @param robotId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	private List<Map<String, Object>> getTaskShow(Integer robotId, String startTime, String endTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfMonth = new SimpleDateFormat("yyyy-MM-dd");
		String currentTime = sdf.format(new Date());
		String currentDay = sdfMonth.format(new Date());
		List<Map<String, Object>> mapTmp = new ArrayList<>();
		List<String> dayBetweenDates = ProjectDateUtils.getDayBetweenDates(startTime, endTime.substring(0, 10));

		// 判断当前查询时间是否是当月
		if (dayBetweenDates.contains(currentDay)) {
			List<Map<String, Object>> historyData = getHistoryData(robotId, startTime, currentTime);
			List<Map<String, Object>> taskRegular = findTaskRegular(robotId, currentTime, endTime);
			List<Map<String, Object>> taskCycle = findTaskCycle(robotId, dayBetweenDates);
			List<Map<String, Object>> taskCycleByInterval = findTaskCycleByInterval(robotId, currentTime,
					dayBetweenDates);
			mapTmp.addAll(historyData);
			mapTmp.addAll(taskRegular);
			mapTmp.addAll(taskCycle);
			mapTmp.addAll(taskCycleByInterval);
		} else {
			Date startDate = ProjectDateUtils.parse10(startTime);
			if (startDate.getTime() > new Date().getTime()) {
				List<Map<String, Object>> taskRegular = findTaskRegular(robotId, startTime, endTime);
				List<Map<String, Object>> taskCycle = findTaskCycle(robotId, dayBetweenDates);
				List<Map<String, Object>> taskCycleByInterval = findTaskCycleByInterval(robotId, startTime,
						dayBetweenDates);
				mapTmp.addAll(taskRegular);
				mapTmp.addAll(taskCycle);
				mapTmp.addAll(taskCycleByInterval);
			} else {
				List<Map<String, Object>> historyData = getHistoryData(robotId, startTime, endTime);
				mapTmp.addAll(historyData);
			}
		}

		return mapTmp;
	}
}
