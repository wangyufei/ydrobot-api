package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.ReconModelMapper;
import com.ydrobot.powerplant.dao.ReconModelSubMapper;
import com.ydrobot.powerplant.model.ReconModel;
import com.ydrobot.powerplant.model.dto.ReconModelResult;
import com.ydrobot.powerplant.service.ReconModelService;

/**
 * Created by Wyf on 2019/05/27.
 */
@Service
@Transactional
public class ReconModelServiceImpl extends AbstractService<ReconModel> implements ReconModelService {
	@Resource
	private ReconModelMapper reconModelMapper;
	@Resource
	private ReconModelSubMapper reconModelSubMapper;

	@Override
	public List<ReconModelResult> listReconModel(Integer page, Integer size) {
		PageHelper.startPage(page, size);
		return reconModelMapper.getList();
	}

}
