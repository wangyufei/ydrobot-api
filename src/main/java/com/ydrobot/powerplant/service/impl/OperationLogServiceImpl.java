package com.ydrobot.powerplant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.DeviceMapper;
import com.ydrobot.powerplant.dao.OperationLogMapper;
import com.ydrobot.powerplant.dao.UserMapper;
import com.ydrobot.powerplant.model.OperationLog;
import com.ydrobot.powerplant.model.condition.OperationLogQueryParam;
import com.ydrobot.powerplant.model.dto.OperationLogResult;
import com.ydrobot.powerplant.model.request.OperationLogRequest;
import com.ydrobot.powerplant.service.OperationLogService;

/**
 * Created by Wyf on 2019/04/30.
 */
@Service
@Transactional
public class OperationLogServiceImpl extends AbstractService<OperationLog> implements OperationLogService {
    @Resource
    private OperationLogMapper operationLogMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private DeviceMapper deviceMapper;
    
    @Override
	public List<OperationLogResult> listOperationLog(OperationLogQueryParam queryParam, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return operationLogMapper.getList(queryParam);
        
	}

	@Override
    public void saveOperationLog(OperationLogRequest operationLogRequest) {
        OperationLog operationLog = new OperationLog();
        BeanUtils.copyProperties(operationLogRequest, operationLog);
        operationLog.setOperaterId(ThreadCache.getUser().getId());
        save(operationLog);
    }

}
