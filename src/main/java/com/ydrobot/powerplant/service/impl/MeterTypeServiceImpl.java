package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.MeterTypeMapper;
import com.ydrobot.powerplant.model.MeterType;
import com.ydrobot.powerplant.service.MeterTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class MeterTypeServiceImpl extends AbstractService<MeterType> implements MeterTypeService {
    @Resource
    private MeterTypeMapper meterTypeMapper;

}
