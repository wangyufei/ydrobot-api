package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.ReconTypeMapper;
import com.ydrobot.powerplant.model.ReconType;
import com.ydrobot.powerplant.service.ReconTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class ReconTypeServiceImpl extends AbstractService<ReconType> implements ReconTypeService {
    @Resource
    private ReconTypeMapper reconTypeMapper;

}
