package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.dao.PermissionMapper;
import com.ydrobot.powerplant.dao.PermissionRoleMapper;
import com.ydrobot.powerplant.dao.RoleMapper;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.PermissionRole;
import com.ydrobot.powerplant.model.Role;
import com.ydrobot.powerplant.model.dto.RoleResult;
import com.ydrobot.powerplant.model.request.RoleRequest;
import com.ydrobot.powerplant.model.request.UpdateRolePermissionRequest;
import com.ydrobot.powerplant.service.RoleService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class RoleServiceImpl extends AbstractService<Role> implements RoleService {
	@Resource
	private RoleMapper roleMapper;
	@Resource
	private PermissionRoleMapper permissionRoleMapper;
	@Resource
	private PermissionMapper permissionMapper;

	@Override
	public void updateRolePermission(Role role, UpdateRolePermissionRequest updateRolePermissionRequest) {
		List<Integer> permissionIds = new ArrayList<>();

		if (StringUtils.isNotBlank(updateRolePermissionRequest.getPermissionIds())) {
			permissionIds = ProjectCommonUtils.getSplitValInt(updateRolePermissionRequest.getPermissionIds(), ",");
		}
		updateRolePermission(role, permissionIds);
	}

	@Override
	public List<RoleResult> listRole(Integer page, Integer size) {
		PageHelper.startPage(page, size);
		return roleMapper.getList();
	}

	@Override
	public void saveRole(RoleRequest roleRequest) {
		Role role = new Role();
		BeanUtils.copyProperties(roleRequest, role);
		save(role);
		List<Integer> permissionIds = ProjectCommonUtils.getSplitValInt(roleRequest.getPermissionIds(), ",");
		updateRolePermission(role, permissionIds);
	}

	@Override
	public void updateRole(Role role, RoleRequest roleRequest) {
		BeanUtils.copyProperties(roleRequest, role);
		update(role);

		List<Integer> permissionIds = ProjectCommonUtils.getSplitValInt(roleRequest.getPermissionIds(), ",");
		updateRolePermission(role, permissionIds);
	}

	/**
	 * 更新角色权限
	 * 
	 * @param role
	 * @param permissionIds
	 */
	private void updateRolePermission(Role role, List<Integer> permissionIds) {
		PermissionRole record = new PermissionRole();
		record.setRoleId(role.getId());
		permissionRoleMapper.delete(record);
		for (Integer permissionId : permissionIds) {
			Permission permission = permissionMapper.selectByPrimaryKey(permissionId);
			if (permission != null) {
				PermissionRole permissionRole = new PermissionRole();
				permissionRole.setRoleId(role.getId());
				permissionRole.setPermissionId(permissionId);
				permissionRoleMapper.insert(permissionRole);
			}
		}
	}
}
