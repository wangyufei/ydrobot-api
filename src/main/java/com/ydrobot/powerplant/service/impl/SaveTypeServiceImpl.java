package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.SaveTypeMapper;
import com.ydrobot.powerplant.model.SaveType;
import com.ydrobot.powerplant.service.SaveTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class SaveTypeServiceImpl extends AbstractService<SaveType> implements SaveTypeService {
    @Resource
    private SaveTypeMapper saveTypeMapper;

}
