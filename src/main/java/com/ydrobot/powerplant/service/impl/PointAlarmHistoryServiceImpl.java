package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.dao.PointAlarmConfirmMapper;
import com.ydrobot.powerplant.dao.PointAlarmHistoryMapper;
import com.ydrobot.powerplant.model.PointAlarmConfirm;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.condition.PointAlarmHistoryQueryParam;
import com.ydrobot.powerplant.model.datasync.AlarmHistorySync;
import com.ydrobot.powerplant.model.dto.PointAlarmHistoryResult;
import com.ydrobot.powerplant.model.request.PointAlarmHistoryBatchRequest;
import com.ydrobot.powerplant.model.request.PointAlarmHistoryRequest;
import com.ydrobot.powerplant.model.request.PointHistoryRequest;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.PointAlarmHistoryService;
import com.ydrobot.powerplant.service.PointHistoryService;
import com.ydrobot.powerplant.service.PointService;
import com.ydrobot.powerplant.service.TaskService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PointAlarmHistoryServiceImpl extends AbstractService<PointAlarmHistory>
		implements PointAlarmHistoryService {
	@Resource
	private PointAlarmHistoryMapper pointAlarmHistoryMapper;
	@Resource
	private PointHistoryService pointHistoryService;
	@Resource
	private PointService pointService;
	@Resource
	private TaskService taskService;
	@Resource
	private PointAlarmConfirmMapper pointAlarmConfirmMapper;
	@Resource
	private DataSyncService dataSyncService;

	@Override
	public List<PointAlarmHistoryResult> listPointAlarmHistory(PointAlarmHistoryQueryParam queryParam) {
		PageHelper.startPage(queryParam.getPage(), queryParam.getSize());
		List<PointAlarmHistoryResult> pointAlarmHistoryResults = pointAlarmHistoryMapper.getList(queryParam);
		// 获取核查报告id
		for (PointAlarmHistoryResult pointAlarmHistoryResult : pointAlarmHistoryResults) {
			Integer twiceConfirmTaskHistoryId = pointAlarmHistoryMapper.getPointAlarmConfirmTaskHistoryId(pointAlarmHistoryResult.getId());
			Integer twiceConfirmTaskId = pointAlarmHistoryMapper.getPointAlarmConfirmTaskId(pointAlarmHistoryResult.getId());
			pointAlarmHistoryResult.setTwiceConfirmTaskHistoryId(twiceConfirmTaskHistoryId);
			pointAlarmHistoryResult.setTwiceConfirmTaskId(twiceConfirmTaskId);
		}
		return pointAlarmHistoryResults;
	}

	@Override
	public void updatePointAlarmHistory(PointAlarmHistory pointAlarmHistory,
			PointAlarmHistoryRequest pointAlarmHistoryRequest) {
		BeanUtils.copyProperties(pointAlarmHistoryRequest, pointAlarmHistory);
		pointAlarmHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
		pointAlarmHistory.setCheckTime(new Date());
		update(pointAlarmHistory);

		PointHistoryRequest pointHistoryRequest = new PointHistoryRequest();
		pointHistoryRequest.setModifyValue(pointAlarmHistoryRequest.getModifyValue());
		if (pointAlarmHistoryRequest.getReconStatus() != null) {
			pointHistoryRequest.setReconStatus(pointAlarmHistoryRequest.getReconStatus());
		}
		pointHistoryService.updatePointHistoryCheckStatus(pointHistoryRequest, pointAlarmHistory.getPointHistoryId());

		// 更新部件评价状态
		if (pointAlarmHistoryRequest.getAppraiseStatus() != null) {
			PointAppraise pointAppraise = new PointAppraise();
			pointAppraise.setPointId(pointAlarmHistory.getPointId());
			pointAppraise.setAppraiseStatus(pointAlarmHistoryRequest.getAppraiseStatus());
			pointAppraise.setAppraiseHistoryId(pointAlarmHistory.getPointHistoryId());
			pointAppraise.setUserName(ThreadCache.getUser().getName());
			pointService.updatePointAppraise(pointAppraise);
		}
		
		// 同步点位告警记录审核状态
		dataSyncService.pointAlarmHistorySync(pointAlarmHistory);
	}

	@Override
	public void batchConfirm(PointAlarmHistoryBatchRequest pointAlarmHistoryBatchRequest) {
		List<Integer> idList = ProjectCommonUtils.getSplitValInt(pointAlarmHistoryBatchRequest.getIds(), ",");

		for (Integer id : idList) {
			PointAlarmHistory pointAlarmHistory = findById(id);
			if (pointAlarmHistory != null) {
				pointAlarmHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
				pointAlarmHistory.setOperaterId(ThreadCache.getUser().getId());
				pointAlarmHistory.setCheckTime(new Date());
				update(pointAlarmHistory);

				PointHistoryRequest pointHistoryRequest = new PointHistoryRequest();
				pointHistoryService.updatePointHistoryCheckStatus(pointHistoryRequest,
						pointAlarmHistory.getPointHistoryId());
				
				// 同步点位告警记录审核状态
				dataSyncService.pointAlarmHistorySync(pointAlarmHistory);
			}
		}
	}

	@Override
	public List<PointAlarmHistory> listPointAlarmHistoryByTaskHistoryId(Integer taskHistoryId) {
		Condition condition = new Condition(PointAlarmHistory.class);
		Criteria criteria = condition.createCriteria();
		criteria.andEqualTo("taskHistoryId", taskHistoryId);
		criteria.andEqualTo("resultStatus", ProjectConsts.RESULT_STATUS_YES);
		return pointAlarmHistoryMapper.selectByCondition(condition);
	}
	
	@Override
	public Task alarmTwiceConfirm(Integer pointAlarmHistoryId) {
		PointAlarmHistory pointAlarmHistory = findById(pointAlarmHistoryId);
		if (pointAlarmHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}

		List<Integer> pointIds = new ArrayList<Integer>();
		pointIds.add(pointAlarmHistory.getPointId());
		Task task = taskService.saveEmergencyTask(pointIds,7000);

		PointAlarmConfirm pointAlarmConfirm= new PointAlarmConfirm();
		pointAlarmConfirm.setTaskId(task.getId());
		pointAlarmConfirm.setPointAlarmHistoryId(pointAlarmHistoryId);
		pointAlarmConfirm.setAlarmTypeId(pointAlarmHistory.getAlarmTypeId());
		pointAlarmConfirmMapper.insertSelective(pointAlarmConfirm);
		
		// 更新云端的告警记录
		AlarmHistorySync alarmHistorySync = new AlarmHistorySync();
		BeanUtils.copyProperties(pointAlarmHistory, alarmHistorySync);
		alarmHistorySync.setTwiceConfirmTaskId(task.getId());
		alarmHistorySync.setExtendId(pointAlarmHistory.getId());
		dataSyncService.twiceConfirmAlarmHistorySync(alarmHistorySync);
		return task;
	}
}
