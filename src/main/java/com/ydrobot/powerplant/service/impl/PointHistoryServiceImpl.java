package com.ydrobot.powerplant.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.ydrobot.powerplant.common.ThreadCache;
import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.result.ResultCode;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.core.utils.ProjectCommonUtils;
import com.ydrobot.powerplant.core.utils.ProjectObjectUtils;
import com.ydrobot.powerplant.dao.AlarmTypeMapper;
import com.ydrobot.powerplant.dao.ObjectAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PointAlarmConfirmMapper;
import com.ydrobot.powerplant.dao.PointAlarmHistoryMapper;
import com.ydrobot.powerplant.dao.PointHistoryConfirmMapper;
import com.ydrobot.powerplant.dao.PointHistoryMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.PointRelationSettingMapper;
import com.ydrobot.powerplant.dao.TaskHistoryMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAlarmConfirm;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.PointHistoryConfirm;
import com.ydrobot.powerplant.model.PointRelationSetting;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.condition.PointHistoryQueryParam;
import com.ydrobot.powerplant.model.condition.PointHistoryReportQueryParam;
import com.ydrobot.powerplant.model.condition.VerificationReportQueryParam;
import com.ydrobot.powerplant.model.dto.ObjectVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryContrastResult;
import com.ydrobot.powerplant.model.dto.PointHistoryCurveResult;
import com.ydrobot.powerplant.model.dto.PointHistoryReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryResult;
import com.ydrobot.powerplant.model.dto.PointHistoryVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointVerificationReportResult;
import com.ydrobot.powerplant.model.request.PointHistoryBatchRequest;
import com.ydrobot.powerplant.model.request.PointHistoryRequest;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.DeviceService;
import com.ydrobot.powerplant.service.ObjectTypeService;
import com.ydrobot.powerplant.service.PointHistoryService;
import com.ydrobot.powerplant.service.PointService;
import com.ydrobot.powerplant.service.TaskService;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PointHistoryServiceImpl extends AbstractService<PointHistory> implements PointHistoryService {

	private static final Logger log = LoggerFactory.getLogger(PointHistoryServiceImpl.class);

	@Resource
	private PointHistoryMapper pointHistoryMapper;
	@Resource
	private PointMapper pointMapper;
	@Resource
	private PointAlarmHistoryMapper pointAlarmHistoryMapper;
	@Resource
	private PointRelationSettingMapper pointRelationSettingMapper;
	@Resource
	private ObjectMapper objectMapper;
	@Resource
	private DeviceService deviceService;
	@Resource
	private ObjectTypeService objectTypeService;
	@Resource
	private TaskHistoryMapper taskHistoryMapper;
	@Resource
	private TaskMapper taskMapper;
	@Resource
	private ObjectAlarmHistoryMapper objectAlarmHistoryMapper;
	@Resource
	private AlarmTypeMapper alarmTypeMapper;
	@Resource
	private PointAlarmConfirmMapper pointAlarmConfirmMapper;
	@Resource
	private TaskService taskService;
	@Resource
	private PointService pointService;
	@Resource
	private PointHistoryConfirmMapper pointHistoryConfirmMapper;
	@Resource
	private DataSyncService dataSyncService;

	@Override
	public List<PointHistoryResult> listPointHistory(PointHistoryQueryParam queryParam) {

		if (queryParam.getPointIds() != null) {
			if (",".equals(queryParam.getPointIds().substring(queryParam.getPointIds().length() - 1))) {
				queryParam.setPointIds(queryParam.getPointIds().substring(0, queryParam.getPointIds().length() - 1));
			}
		}

		PageHelper.startPage(queryParam.getPage(), queryParam.getSize());
		List<PointHistoryResult> pointHistoryResults = pointHistoryMapper.getList(queryParam);

		for (PointHistoryResult pointHistoryResult : pointHistoryResults) {
			Integer twiceConfirmTaskHistoryId = pointHistoryMapper
					.getPointHistoryConfirmTaskHistoryId(pointHistoryResult.getId());
			Integer twiceConfirmTaskId = pointHistoryMapper.getPointHistoryConfirmTaskId(pointHistoryResult.getId());
			pointHistoryResult.setTwiceConfirmTaskHistoryId(twiceConfirmTaskHistoryId);
			pointHistoryResult.setTwiceConfirmTaskId(twiceConfirmTaskId);
		}

		return pointHistoryResults;
	}

	@Override
	public List<PointHistoryContrastResult> listPointHistoryContrast(PointHistoryQueryParam queryParam) {

		List<PointHistoryContrastResult> pointHistoryContrastResults = new ArrayList<>();
		if (queryParam.getPointIds() != null) {
			pointHistoryContrastResults = pointHistoryMapper.getPointHistoryContrastList(queryParam);
			for (PointHistoryContrastResult pointHistoryContrastResult : pointHistoryContrastResults) {
				setTime(queryParam);
				List<Map<String, Object>> pointHistory = pointHistoryMapper.getPointHistory(
						pointHistoryContrastResult.getPointId(), queryParam.getStartTime(), queryParam.getEndTime());
				pointHistoryContrastResult.setPointHistory(pointHistory);
			}
		}

		return pointHistoryContrastResults;
	}

	@Override
	public void updatePointHistory(PointHistory pointHistory, PointHistoryRequest pointHistoryRequest) {
		pointHistory.setReconStatus(pointHistoryRequest.getReconStatus());
		pointHistory.setModifyValue(pointHistoryRequest.getModifyValue());
		pointHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
		pointHistory.setOperaterId(ThreadCache.getUser().getId());
		update(pointHistory);

		// 同步点位报警历史记录状态
		Integer resultErrorPointNum = pointAlarmHistoryMapper.getResultErrorPointNum(pointHistory.getId());
		if (resultErrorPointNum == 0) {
			PointAlarmHistory record = new PointAlarmHistory();
			record.setPointHistoryId(pointHistory.getId());
			List<PointAlarmHistory> pointAlarmHistories = pointAlarmHistoryMapper.select(record);

			for (PointAlarmHistory pointAlarmHistory : pointAlarmHistories) {
				pointAlarmHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
				pointAlarmHistory.setCheckTime(new Date());
				pointAlarmHistory.setOperaterId(ThreadCache.getUser().getId());
				pointAlarmHistoryMapper.updateByPrimaryKey(pointAlarmHistory);
			}
		}

		// 更新部件评价状态
		if (pointHistoryRequest.getAppraiseStatus() != null) {
			PointAppraise pointAppraise = new PointAppraise();
			pointAppraise.setPointId(pointHistory.getPointId());
			pointAppraise.setAppraiseStatus(pointHistoryRequest.getAppraiseStatus());
			pointAppraise.setAppraiseHistoryId(pointHistory.getId());
			pointAppraise.setUserName(ThreadCache.getUser().getName());
			pointService.updatePointAppraise(pointAppraise);
		}

		// 同步点位状态
		updatePointStatus(pointHistory);

		// 同步点位记录审核状态
		dataSyncService.pointHistorySync(pointHistory);
	}

	@Override
	public void updatePointHistoryCheckStatus(PointHistoryRequest pointHistoryRequest, Integer pointHistoryId) {

		PointHistory pointHistory = findById(pointHistoryId);
		if (pointHistory != null) {
			pointHistory.setReconStatus(pointHistoryRequest.getReconStatus());
			pointHistory.setModifyValue(pointHistoryRequest.getModifyValue());

			PointAlarmHistory record = new PointAlarmHistory();
			record.setPointHistoryId(pointHistoryId);
			record.setCheckStatus(ProjectConsts.CHECK_STATUS_NO);
			Integer PointAlarmHistoryNotCheckNum = pointAlarmHistoryMapper.selectCount(record);

			if (PointAlarmHistoryNotCheckNum == 0) {
				pointHistory.setCheckStatus(ProjectConsts.CHECK_STATUS_YES);
				pointHistory.setOperaterId(ThreadCache.getUser().getId());
			}

			update(pointHistory);
			updatePointStatus(pointHistory);
		}

	}

	@Override
	public void updatePointStatus(PointHistory pointHistory) {

		// 获取最新的点位历史记录
		PointHistory lastPointHistory = pointHistoryMapper.getPointHistoryLast(pointHistory.getPointId());
		if (lastPointHistory != null && lastPointHistory.getId().intValue() == pointHistory.getId().intValue()) {

			PointAlarmHistory record = new PointAlarmHistory();
			record.setCheckStatus(ProjectConsts.CHECK_STATUS_NO);
			record.setPointHistoryId(pointHistory.getId());
			Integer PointAlarmHistoryNotCheckNum = pointAlarmHistoryMapper.selectCount(record);

			// 判断点位报警记录是否都已经审核
			if (PointAlarmHistoryNotCheckNum == 0) {
				Point point = pointMapper.selectByPrimaryKey(pointHistory.getPointId());
				if (point != null) {

					// 获取结果异常点位数
					int resultErrorPointNum = pointAlarmHistoryMapper.getResultErrorPointNum(pointHistory.getId());
					// 判断状态是否异常 abnormal 0:正常 1:异常
					if (pointHistory.getReconStatus().intValue() == 0 && resultErrorPointNum == 0) {
						point.setIsAbnormal(new Byte("0"));
					} else {
						point.setIsAbnormal(new Byte("1"));
					}

					if (resultErrorPointNum == 0) {
						// 恢复点位最大报警等级为正常
						point.setMaxAlarmLevel(new Byte("0"));
					} else {
						PointAlarmHistory pointAlarmHistory = pointAlarmHistoryMapper
								.getMaxAlarmLevel(pointHistory.getId());
						point.setMaxAlarmLevel(pointAlarmHistory.getAlarmLevel());
					}

					pointMapper.updateByPrimaryKey(point);
				}

			}
		}
	}

	@Override
	public List<PointHistory> listRelationPointHistory(Integer pointId, Integer taskHistoryId) {
		List<PointHistory> pointHistories = new ArrayList<>();
		PointRelationSetting pointRelationSettingCondition = new PointRelationSetting();
		pointRelationSettingCondition.setPointId(pointId);
		List<PointRelationSetting> pointRelationSettings = pointRelationSettingMapper
				.select(pointRelationSettingCondition);

		List<Integer> pointIds = new ArrayList<>();

		for (PointRelationSetting pointRelationSetting : pointRelationSettings) {
			pointIds.add(pointRelationSetting.getReferencePointId());
		}

		if (!pointIds.isEmpty()) {
			Condition condition = new Condition(PointHistory.class);
			Criteria criteria = condition.createCriteria();

			criteria.andIn("pointId", pointIds);
			criteria.andEqualTo("taskHistoryId", taskHistoryId);

			pointHistories = findByCondition(condition);

			for (PointHistory pointHistory : pointHistories) {
				pointHistory.setCameraPic(pointHistory.getCameraPic());
				pointHistory.setFlirPic(pointHistory.getFlirPic());
				pointHistory.setSound(pointHistory.getSound());
			}
		}

		return pointHistories;
	}

	@Override
	public List<Map<String, Object>> extendRelationPoint(List<PointHistory> pointHistories) {
		List<Map<String, Object>> maps = new ArrayList<>();

		for (PointHistory pointHistory : pointHistories) {
			Map<String, Object> map = ProjectObjectUtils.objectToMap(pointHistory);
			Point point = pointMapper.selectByPrimaryKey(pointHistory.getPointId());
			map.put("point", point);
			maps.add(map);
		}

		return maps;
	}

	@Override
	public void batchConfirm(PointHistoryBatchRequest body) {
		List<Integer> ids = ProjectCommonUtils.getSplitValInt(body.getIds(), ",");
		for (int i = 0; i < ids.size(); i++) {
			PointHistory pointHistory = pointHistoryMapper.selectByPrimaryKey(ids.get(i));
			pointHistory.setCheckStatus(new Byte("1"));
			update(pointHistory);
			// 同步点位报警历史记录状态
			Integer resultErrorPointNum = pointAlarmHistoryMapper.getResultErrorPointNum(pointHistory.getId());
			if (resultErrorPointNum == 0) {
				PointAlarmHistory pointAlarmHistoryCondition = new PointAlarmHistory();
				pointAlarmHistoryCondition.setPointHistoryId(pointHistory.getId());
				List<PointAlarmHistory> pointAlarmHistories = pointAlarmHistoryMapper
						.select(pointAlarmHistoryCondition);

				for (PointAlarmHistory pointAlarmHistory : pointAlarmHistories) {
					pointAlarmHistory.setCheckStatus(new Byte("1"));
					pointAlarmHistory.setCheckTime(new Date());
					pointAlarmHistoryMapper.updateByPrimaryKey(pointAlarmHistory);
				}
			}

			// 同步点位状态
			updatePointStatus(pointHistory);

			// 同步点位记录审核状态
			dataSyncService.pointHistorySync(pointHistory);
		}
	}

	@Override
	public List<PointHistory> listPointHistoryByTaskHistoryId(Integer taskHistoryId) {
		Condition condition = new Condition(PointHistory.class);
		Criteria criteria = condition.createCriteria();
		criteria.andEqualTo("taskHistoryId", taskHistoryId);
		criteria.andEqualTo("reconStatus", 1);
		return pointHistoryMapper.selectByCondition(condition);
	}

	@Override
	public List<PointHistory> listNormalPointHistoryByTaskHistoryId(Integer taskHistoryId) {
		return pointHistoryMapper.getNormalPointHistory(taskHistoryId);
	}

	@Override
	public int countPointTotalNumByTaskHistoryId(Integer taskHistoryId) {
		int pointTotalNum = 0;
		PointHistory pointHistoryCondition = new PointHistory();
		pointHistoryCondition.setTaskHistoryId(taskHistoryId);
		pointTotalNum = pointHistoryMapper.selectCount(pointHistoryCondition);
		return pointTotalNum;
	}

	@Override
	public List<PointHistoryReportResult> listPointHistoryReport(PointHistoryReportQueryParam queryParam) {
		PageHelper.startPage(queryParam.getPage(), queryParam.getSize());
		List<PointHistoryReportResult> pointHistoryReportResults = pointHistoryMapper
				.getPointHistoryReportList(queryParam);

		for (PointHistoryReportResult pointHistoryReportResult : pointHistoryReportResults) {
			if (pointHistoryReportResult.getPoint() != null) {

				TObject tObject = objectMapper.selectByPrimaryKey(pointHistoryReportResult.getPoint().getObjectId());
				if (tObject != null) {
					pointHistoryReportResult
							.setDeviceArea(deviceService.getDeviceAreaNameByDeviceId(tObject.getDeviceId()));
					pointHistoryReportResult
							.setInterval(deviceService.getDeviceIntervalNameByDeviceId(tObject.getDeviceId()));
					pointHistoryReportResult.setDeviceName(tObject.getName());
					pointHistoryReportResult
							.setDeviceType(objectTypeService.getObjectTypeNameById(tObject.getObjectTypeId()));
				}
			}
		}
		return pointHistoryReportResults;
	}

	@Override
	public List<PointVerificationReportResult> listPointVerificationReport(VerificationReportQueryParam queryParam) {
		// 首先获取任务执行记录下某个对象的部件列表
		List<PointVerificationReportResult> pointVerificationReportResults = pointHistoryMapper
				.getPointVerificationReportResult(queryParam);

		for (PointVerificationReportResult pointVerificationReportResult : pointVerificationReportResults) {
			PointAlarmHistory pointAlarmHistoryRecord = new PointAlarmHistory();
			pointAlarmHistoryRecord.setPointId(pointVerificationReportResult.getPointId());
			pointAlarmHistoryRecord.setTaskHistoryId(pointVerificationReportResult.getTaskHistoryId());
			Integer alarmNum = pointAlarmHistoryMapper.selectCount(pointAlarmHistoryRecord);

			// 核查状态 0:正常 1:异常
			if (alarmNum > 0) {
				pointVerificationReportResult.setVerificationStatus(1);
			} else {
				pointVerificationReportResult.setVerificationStatus(0);
			}

			List<PointHistoryVerificationReportResult> pointHistoryVerificationReportResults = pointHistoryMapper
					.getPointHistoryVerificationReportResult(pointVerificationReportResult.getPointId(),
							pointVerificationReportResult.getTaskHistoryId());
			pointVerificationReportResult
					.setPointHistoryVerificationReportResults(pointHistoryVerificationReportResults);
		}
		return pointVerificationReportResults;
	}

	@Override
	public ObjectVerificationReportResult getObjectVerificationReport(VerificationReportQueryParam queryParam) {
		// 通过任务记录id获取巡检对象
		TObject tObject = pointHistoryMapper.getObjectByTaskHistoryId(queryParam.getTaskHistoryId());

		if (tObject == null) {
			throw new ServiceException("对象不存在");
		}

		TaskHistory taskHistory = taskHistoryMapper.selectByPrimaryKey(queryParam.getTaskHistoryId());

		if (taskHistory == null) {
			throw new ServiceException("巡检记录不存在");
		}

		ObjectVerificationReportResult objectVerificationReportResult = new ObjectVerificationReportResult();
		objectVerificationReportResult.setObjectName(tObject.getName());
		objectVerificationReportResult.setObjectId(tObject.getId());
		objectVerificationReportResult.setInspectReason(getInspectReason(taskHistory.getTaskId()));
		objectVerificationReportResult.setInspectTime(taskHistory.getTaskStartTime());
		objectVerificationReportResult.setStartMode(getStartMode(taskHistory.getTaskId()));

		Integer alarmNum = pointHistoryMapper.getObjectAlarmNum(tObject.getId(), queryParam.getTaskHistoryId());
		if (alarmNum > 0) {
			objectVerificationReportResult.setAppraiseStatus("该设备存在缺陷");
		} else {
			objectVerificationReportResult.setAppraiseStatus("该设备状态正常");
		}

		return objectVerificationReportResult;
	}

	@Override
	public List<PointHistoryCurveResult> countPointHistoryCurve(Integer pointId) {
		DateTime startTime = DateUtil.offsetDay(new Date(), -7);
		String startTimeFormat = DateUtil.format(startTime, "yyyy-MM-dd HH:mm:ss");
		return pointHistoryMapper.countPointHistoryCurve(pointId, startTimeFormat);
	}

	@Override
	public Task twiceConfirm(Integer pointHistoryId) {
		PointHistory pointHistory = findById(pointHistoryId);
		if (pointHistory == null) {
			throw new ServiceException(ResultCode.DATA_NOT_FOUND);
		}

		List<Integer> pointIds = new ArrayList<Integer>();
		pointIds.add(pointHistory.getPointId());
		Task task = taskService.saveEmergencyTask(pointIds, 7000);

		PointHistoryConfirm pointHistoryConfirm = new PointHistoryConfirm();
		pointHistoryConfirm.setTaskId(task.getId());
		pointHistoryConfirm.setPointHistoryId(pointHistoryId);
		pointHistoryConfirm.setValue(pointHistory.getValue());
		pointHistoryConfirmMapper.insertSelective(pointHistoryConfirm);
		return task;
	}

	@Override
	public List<PointHistoryVerificationReportResult> getPointAppraiseReason(Integer pointHistoryId) {
		List<PointHistoryVerificationReportResult> pointHistoryVerificationReportResults = pointHistoryMapper
				.getPointAppraiseReason(pointHistoryId);
		return pointHistoryVerificationReportResults;
	}

	/**
	 * 设置默认查询条件时间为一个月
	 * 
	 * @param pointHistoryCondition
	 */
	private void setTime(PointHistoryQueryParam queryParam) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (queryParam.getEndTime() == null) {
			Date endDate = new Date();
			queryParam.setEndTime(sdf.format(endDate));
		}

		if (queryParam.getStartTime() == null) {
			try {
				Date endDate = sdf.parse(queryParam.getEndTime());
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(endDate);
				calendar.add(Calendar.MONTH, -1);
				queryParam.setStartTime(sdf.format(calendar.getTime()));
			} catch (ParseException e) {
				log.info(e.getMessage());
			}
		}
	}

	/**
	 * 获取启动模式
	 * 
	 * @param objectId
	 * @param taskHistoryId
	 * @return
	 */
	private String getStartMode(Integer taskId) {
		String startMode = "智能巡检";

		// 判断巡检类型是否为60000应急巡检
		ObjectAlarmHistory ObjectAlarmHistoryRecord = new ObjectAlarmHistory();
		ObjectAlarmHistoryRecord.setTaskId(taskId);
		List<ObjectAlarmHistory> objectAlarmHistories = objectAlarmHistoryMapper.select(ObjectAlarmHistoryRecord);

		if (!objectAlarmHistories.isEmpty()) {
			startMode = "自动应急巡检";
			return startMode;
		}

		PointAlarmConfirm pointAlarmConfirmRecord = new PointAlarmConfirm();
		pointAlarmConfirmRecord.setTaskId(taskId);
		List<PointAlarmConfirm> pointAlarmConfirms = pointAlarmConfirmMapper.select(pointAlarmConfirmRecord);

		PointHistoryConfirm pointHistoryConfirmRecord = new PointHistoryConfirm();
		pointHistoryConfirmRecord.setTaskId(taskId);
		List<PointHistoryConfirm> pointHistoryConfirms = pointHistoryConfirmMapper.select(pointHistoryConfirmRecord);

		if (!pointAlarmConfirms.isEmpty() || !pointHistoryConfirms.isEmpty()) {
			startMode = "手动应急巡检";
			return startMode;
		}

		return startMode;
	}

	/**
	 * 获取巡检原因
	 * 
	 * @param objectId
	 * @param taskHistoryId
	 * @return
	 */
	private String getInspectReason(Integer taskId) {

		String inspectReason = "";

		ObjectAlarmHistory ObjectAlarmHistoryRecord = new ObjectAlarmHistory();
		ObjectAlarmHistoryRecord.setTaskId(taskId);
		List<ObjectAlarmHistory> objectAlarmHistories = objectAlarmHistoryMapper.select(ObjectAlarmHistoryRecord);

		if (!objectAlarmHistories.isEmpty()) {
			AlarmType alarmType = alarmTypeMapper.selectByPrimaryKey(objectAlarmHistories.get(0).getAlarmTypeId());
			TObject object = objectMapper.selectByPrimaryKey(objectAlarmHistories.get(0).getObjectId());
			if (object != null) {
				inspectReason += object.getName();
			}

			if (alarmType != null) {
				inspectReason += alarmType.getName();
			}
		}

		PointAlarmConfirm pointAlarmConfirmRecord = new PointAlarmConfirm();
		pointAlarmConfirmRecord.setTaskId(taskId);
		List<PointAlarmConfirm> pointAlarmConfirms = pointAlarmConfirmMapper.select(pointAlarmConfirmRecord);

		if (!pointAlarmConfirms.isEmpty()) {
			AlarmType alarmType = alarmTypeMapper.selectByPrimaryKey(pointAlarmConfirms.get(0).getAlarmTypeId());
			Point point = pointAlarmHistoryMapper
					.getPointByPointAlarmHistoryId(pointAlarmConfirms.get(0).getPointAlarmHistoryId());

			if (point != null) {
				inspectReason += point.getName();
			}

			if (alarmType != null) {
				inspectReason += alarmType.getName();
			}
		}

		PointHistoryConfirm pointHistoryConfirmRecord = new PointHistoryConfirm();
		pointHistoryConfirmRecord.setTaskId(taskId);
		List<PointHistoryConfirm> pointHistoryConfirms = pointHistoryConfirmMapper.select(pointHistoryConfirmRecord);

		if (!pointHistoryConfirms.isEmpty()) {
			Point point = pointHistoryMapper.getPointByPointHistoryId(pointHistoryConfirms.get(0).getPointHistoryId());

			if (point != null) {
				inspectReason = point.getName() + pointHistoryConfirms.get(0).getValue() + point.getUnit();
			}
		}

		return inspectReason;
	}
}
