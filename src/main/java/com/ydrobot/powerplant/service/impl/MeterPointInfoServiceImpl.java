package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.MeterPointInfoMapper;
import com.ydrobot.powerplant.model.MeterPointInfo;
import com.ydrobot.powerplant.service.MeterPointInfoService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class MeterPointInfoServiceImpl extends AbstractService<MeterPointInfo> implements MeterPointInfoService {
    @Resource
    private MeterPointInfoMapper meterPointInfoMapper;

}
