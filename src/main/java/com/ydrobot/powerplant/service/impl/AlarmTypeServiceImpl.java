package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.AlarmTypeMapper;
import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.service.AlarmTypeService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class AlarmTypeServiceImpl extends AbstractService<AlarmType> implements AlarmTypeService {
    @Resource
    private AlarmTypeMapper alarmTypeMapper;

}
