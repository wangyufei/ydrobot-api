package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.RoutePos;
import com.ydrobot.powerplant.service.RoutePosService;
import com.ydrobot.powerplant.service.StopPointService;
import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.RouteMapper;
import com.ydrobot.powerplant.dao.RoutePosMapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class RoutePosServiceImpl extends AbstractService<RoutePos> implements RoutePosService {

	@Autowired
	private RouteMapper routeMapper;
	@Autowired
	private RoutePosMapper routePosMapper;
	@Autowired
	private StopPointService stopPointService;

	@Override
	public Integer saveRoutePos(List<String> point) {
		RoutePos routePos = new RoutePos();
		routePos.setX(Float.valueOf(point.get(1)));
		routePos.setY(Float.valueOf(point.get(2)));
		routePos.setZ(Float.valueOf(point.get(3)));
		routePos.setType(new Byte("1"));
		save(routePos);
		return routePos.getId();
	}

	@Override
	@Transactional
	public void deleteRoutePos(Integer id) {
		RoutePos routePos = findById(id);

		if (routePos == null) {
			throw new ServiceException("道路点不存在");
		}

		if (routePos.getType() == 0) {
			throw new ServiceException("原始点不能删除");
		}

		// 查询该点所关联的道路并更新
		Route record1 = new Route();
		record1.setEnd(routePos.getId());
		Route route1 = routeMapper.selectOne(record1);

		Route record2 = new Route();
		record2.setStart(routePos.getId());
		Route route2 = routeMapper.selectOne(record2);

		if (record1 == null || record2 == null) {
			throw new ServiceException("道路删除失败!");
		}

		route1.setEnd(route2.getEnd());
		routeMapper.updateByPrimaryKey(route1);
		routeMapper.delete(route2);
		deleteById(routePos.getId());

		// 更新道路关联的停靠点
		stopPointService.updateStopPoint(route2.getId(), route1.getId());
	}

	@Override
	public RoutePos getRoutePosByPoint(String[] point) {
		// 起始点x，y，z
		Float pointX = Float.valueOf(point[0]);
		Float pointY = Float.valueOf(point[1]);
		Float pointZ = Float.valueOf(point[2]);

		RoutePos record = new RoutePos();
		record.setX(pointX);
		record.setY(pointY);
		record.setZ(pointZ);

		// 查询道路点
		RoutePos routePos = routePosMapper.selectOne(record);

		if (routePos == null) {
			routePos = new RoutePos();
			routePos.setX(pointX);
			routePos.setY(pointY);
			routePos.setZ(pointZ);
			routePos.setType(new Byte("1"));
			save(routePos);
		}

		return routePos;
	}

}
