package com.ydrobot.powerplant.service.impl;

import com.ydrobot.powerplant.dao.PointHistoryConfirmMapper;
import com.ydrobot.powerplant.model.PointHistoryConfirm;
import com.ydrobot.powerplant.service.PointHistoryConfirmService;
import com.ydrobot.powerplant.core.service.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by Wyf on 2020/06/19.
 */
@Service
@Transactional
public class PointHistoryConfirmServiceImpl extends AbstractService<PointHistoryConfirm> implements PointHistoryConfirmService {
    @Resource
    private PointHistoryConfirmMapper pointHistoryConfirmMapper;

}
