package com.ydrobot.powerplant.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.TaskPointMapper;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.service.TaskPointService;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class TaskPointServiceImpl extends AbstractService<TaskPoint> implements TaskPointService {
    @Resource
    private TaskPointMapper taskPointMapper;

    @Override
    public void deleteByTaskId(Integer taskId) {
        TaskPoint record = new TaskPoint();
        record.setTaskId(taskId);
        taskPointMapper.delete(record);
    }

}
