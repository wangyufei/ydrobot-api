package com.ydrobot.powerplant.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.service.AbstractService;
import com.ydrobot.powerplant.dao.DeviceMapper;
import com.ydrobot.powerplant.dao.MeterPointInfoMapper;
import com.ydrobot.powerplant.dao.ObjectMapper;
import com.ydrobot.powerplant.dao.PointAppraiseMapper;
import com.ydrobot.powerplant.dao.PointMapper;
import com.ydrobot.powerplant.dao.PointVisibleLightMapper;
import com.ydrobot.powerplant.dao.PointWatchPointMapper;
import com.ydrobot.powerplant.dao.TaskMapper;
import com.ydrobot.powerplant.dao.WatchPointMapper;
import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.MeterPointInfo;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.PointVisibleLight;
import com.ydrobot.powerplant.model.PointWatchPoint;
import com.ydrobot.powerplant.model.WatchPoint;
import com.ydrobot.powerplant.model.condition.PointQueryParam;
import com.ydrobot.powerplant.model.condition.PointTreeQueryParam;
import com.ydrobot.powerplant.model.dto.AbnormalAppraisePointResult;
import com.ydrobot.powerplant.model.dto.ObjectTreeResult;
import com.ydrobot.powerplant.model.dto.PointResult;
import com.ydrobot.powerplant.model.dto.PointTreeInfoResult;
import com.ydrobot.powerplant.model.request.MeterPointInfoRequest;
import com.ydrobot.powerplant.model.request.PointRequest;
import com.ydrobot.powerplant.model.request.PointVisibleLightRequest;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.PointAppraiseHistoryService;
import com.ydrobot.powerplant.service.PointService;

import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by Wyf on 2019/04/12.
 */
@Service
@Transactional
public class PointServiceImpl extends AbstractService<Point> implements PointService {

	@Resource
	private PointMapper pointMapper;
	@Resource
	private DeviceMapper deviceMapper;
	@Resource
	private MeterPointInfoMapper meterPointInfoMapper;
	@Resource
	private PointVisibleLightMapper pointVisibleLightMapper;
	@Resource
	private ObjectMapper objectMapper;
	@Resource
	private PointWatchPointMapper pointWatchPointMapper;
	@Resource
	private WatchPointMapper watchPointMapper;
	@Resource
	private PointAppraiseMapper pointAppraiseMapper;
	@Resource
	private DataSyncService dataSyncService;
	@Resource
	private PointAppraiseHistoryService pointAppraiseHistoryService;
	@Resource
	private TaskMapper taskMapper;

	@Override
	public List<Point> listPoint(PointQueryParam queryParam, Integer page, Integer size) {
		Condition condition = new Condition(Point.class);
		Criteria criteria = condition.createCriteria();

		if (queryParam.getIsDelete() != null) {
			criteria.andEqualTo("isDelete", queryParam.getIsDelete());
		}

		if (queryParam.getObjectId() != null) {
			criteria.andEqualTo("objectId", queryParam.getObjectId());
		}

		return findByCondition(condition);
	}

	@Override
	@Cacheable(value = "pointTree")
	public List<Device> listPointTree(Long random,PointTreeQueryParam queryParam) {
		List<ObjectTreeResult> objectTreeResults = objectMapper.getObjectTree(queryParam);
		List<Point> points = pointMapper.getNormalPoint();
		List<Device> devices = deviceMapper.getStationTree();
		recombineDevice(devices, objectTreeResults);
		setDevicePoint(points, devices);
		List<Device> devicesTree = listToTree(devices);
		return devicesTree;
	}

	@Override
	public List<Device> listAbnormalPointTree(Long random,PointTreeQueryParam queryParam) {
		List<ObjectTreeResult> objectTreeResults = objectMapper.getObjectTree(queryParam);
		List<Point> points = pointMapper.getAbnormalPoint();
		List<Device> devices = deviceMapper.getStationTree();
		recombineDevice(devices, objectTreeResults);
		setDevicePoint(points, devices);
		List<Device> devicesTree = listToTree(devices);
		return devicesTree;
	}

	@Override
	@Transactional
	public Integer savePoint(PointRequest pointRequest) {
		Point point = new Point();
		BeanUtils.copyProperties(pointRequest, point);
		save(point);

		// 点位表计信息
		MeterPointInfoRequest meterPointInfoRequest = pointRequest.getMeterPointInfoRequest();
		if (meterPointInfoRequest != null) {
			MeterPointInfo meterPointInfo = new MeterPointInfo();
			BeanUtils.copyProperties(meterPointInfoRequest, meterPointInfo);
			meterPointInfo.setPointId(point.getId());
			meterPointInfoMapper.insert(meterPointInfo);
		}

		// 点位可见光信息
		PointVisibleLightRequest pointVisibleLightRequest = pointRequest.getPointVisibleLightRequest();
		if (pointVisibleLightRequest != null) {
			PointVisibleLight pointVisibleLight = new PointVisibleLight();
			BeanUtils.copyProperties(pointVisibleLightRequest, pointVisibleLight);
			pointVisibleLight.setPointId(point.getId());
			pointVisibleLightMapper.insert(pointVisibleLight);
		}

		// 添加组件和观察点的关系
		List<Integer> watchPointIds = pointRequest.getWatchPointIds();
		if (!watchPointIds.isEmpty()) {
			for (int i = 0; i < watchPointIds.size(); i++) {
				PointWatchPoint pointWatchPoint = new PointWatchPoint();
				pointWatchPoint.setPointId(point.getId());
				pointWatchPoint.setWatchPointId(watchPointIds.get(i));
				pointWatchPointMapper.insert(pointWatchPoint);
			}
		}

		return point.getId();
	}

	@Override
	@Transactional
	public void updatePoint(Point point, PointRequest pointRequest) {
		BeanUtils.copyProperties(pointRequest, point);
		update(point);

		// 更新点位表计信息
		MeterPointInfoRequest meterPointInfoRequest = pointRequest.getMeterPointInfoRequest();
		if (meterPointInfoRequest != null && meterPointInfoRequest.getId() != null) {
			MeterPointInfo meterPointInfo = meterPointInfoMapper.selectByPrimaryKey(meterPointInfoRequest.getId());
			if (meterPointInfo != null) {
				BeanUtils.copyProperties(meterPointInfoRequest, meterPointInfo);
				meterPointInfo.setPointId(point.getId());
				meterPointInfoMapper.updateByPrimaryKeySelective(meterPointInfo);
			}
		}

		if (meterPointInfoRequest != null && meterPointInfoRequest.getId() == null) {
			MeterPointInfo meterPointInfo = new MeterPointInfo();
			BeanUtils.copyProperties(meterPointInfoRequest, meterPointInfo);
			meterPointInfo.setPointId(point.getId());
			meterPointInfoMapper.insert(meterPointInfo);
		}

		// 更新点位可见光信息
		PointVisibleLightRequest pointVisibleLightRequest = pointRequest.getPointVisibleLightRequest();
		if (pointVisibleLightRequest != null && pointVisibleLightRequest.getId() != null) {
			PointVisibleLight pointVisibleLight = pointVisibleLightMapper
					.selectByPrimaryKey(pointVisibleLightRequest.getId());
			if (pointVisibleLight != null) {
				BeanUtils.copyProperties(pointVisibleLightRequest, pointVisibleLight);
				pointVisibleLight.setPointId(point.getId());
				pointVisibleLightMapper.updateByPrimaryKeySelective(pointVisibleLight);
			}
		}

		if (pointVisibleLightRequest != null && pointVisibleLightRequest.getId() == null) {
			PointVisibleLight pointVisibleLight = new PointVisibleLight();
			BeanUtils.copyProperties(pointVisibleLightRequest, pointVisibleLight);
			pointVisibleLight.setPointId(point.getId());
			pointVisibleLightMapper.insert(pointVisibleLight);
		}

		// 更新点位观察点信息
		List<Integer> watchPointIds = pointRequest.getWatchPointIds();
		if (watchPointIds != null) {
			PointWatchPoint pointWatchPointRecord = new PointWatchPoint();
			pointWatchPointRecord.setPointId(point.getId());
			pointWatchPointMapper.delete(pointWatchPointRecord);
			for (int i = 0; i < watchPointIds.size(); i++) {
				PointWatchPoint pointWatchPoint = new PointWatchPoint();
				pointWatchPoint.setPointId(point.getId());
				pointWatchPoint.setWatchPointId(watchPointIds.get(i));
				pointWatchPointMapper.insert(pointWatchPoint);
			}
		}
		
		// 更新包含该点的任务状态
		taskMapper.updateTaskIsUpdateStatus(point.getId());
	}

	@Override
	public PointResult getDetail(Integer pointId) {
		return pointMapper.getDetail(pointId);
	}

	@Override
	public List<AbnormalAppraisePointResult> listAbnormalAppraisePoint() {
		return pointMapper.getAbnormalAppraisePoint();
	}

	@Override
	public List<WatchPoint> listWatchPointByPointId(Integer pointId) {
		// 获取观察点信息
		List<WatchPoint> watchPoints = watchPointMapper.findByPointId(pointId);
		return watchPoints;
	}

	@Override
	public void updatePointAppraise(PointAppraise pointAppraise) {
		PointAppraise record = new PointAppraise();
		record.setPointId(pointAppraise.getPointId());
		PointAppraise pointAppraise2 = pointAppraiseMapper.selectOne(record);

		if (pointAppraise2 != null) {
			if (pointAppraise.getAppraiseStatus() >= pointAppraise2.getAppraiseStatus()) {
				pointAppraise2.setAppraiseStatus(pointAppraise.getAppraiseStatus());
				pointAppraise2.setAppraiseTime(new Date());
				pointAppraise2.setAppraiseHistoryId(pointAppraise.getAppraiseHistoryId());
				pointAppraiseMapper.updateByPrimaryKeySelective(pointAppraise2);
			}
		} else {
			pointAppraise2 = new PointAppraise();
			BeanUtils.copyProperties(pointAppraise, pointAppraise2);
			pointAppraise2.setAppraiseTime(new Date());
			pointAppraiseMapper.insertSelective(pointAppraise2);
		}
		
		// 写入评价历史记录信息
		PointAppraiseHistory pointAppraiseHistory = new PointAppraiseHistory();
		BeanUtils.copyProperties(pointAppraise, pointAppraiseHistory);
		pointAppraiseHistoryService.savePointAppraiseHistory(pointAppraiseHistory);
		
		// 同步评价信息
		dataSyncService.pointAppraiseSync(pointAppraise2);
	}

	/**
	 * 列表转树
	 * 
	 * @param devices
	 * @return
	 */
	private List<Device> listToTree(List<Device> devices) {
		List<Device> deviceTree = new ArrayList<Device>();
		for (Device device : devices) {
			// 层级为变电站，则是一级数据
			if (device.getModelLevel().intValue() == ProjectConsts.DEVICE_STAND_MODEL_LEVEL) {
				deviceTree.add(device);
			}

			// 循环出子级数据，添加到一级数据
			for (Device subDevice : devices) {
				if (subDevice.getParentId().intValue() == device.getId().intValue()) {
					if (device.getChildren() == null) {
						device.setChildren(new ArrayList<>());
					}
					device.getChildren().add(subDevice);
				}
			}
		}

		return deviceTree;
	}

	/**
	 * 设置设备点
	 * 
	 * @param points
	 * @param devices
	 */
	private void setDevicePoint(List<Point> points, List<Device> devices) {
		for (Device device : devices) {
			Integer alarmLevel = 0;
			List<Device> deviceTmps = new ArrayList<>();
			deviceTmps = getDeviceList(device, devices, deviceTmps);
			for (Point point : points) {
				if (device.getModelLevel().intValue() == ProjectConsts.DEVICE_MODEL_LEVEL) {
					point.setAlarmLevel(point.getMaxAlarmLevel());
					if (point.getObjectId().intValue() == (device.getId().intValue() - 10000)) {
						if (device.getPoints() == null) {
							device.setPoints(new ArrayList<>());
						}
						PointTreeInfoResult pointTreeInfoResult = new PointTreeInfoResult();
						pointTreeInfoResult.setId(point.getId());
						pointTreeInfoResult.setAlarmLevel(point.getMaxAlarmLevel());
						pointTreeInfoResult.setFaceTypeId(point.getFaceTypeId());
						pointTreeInfoResult.setIsUse(point.getIsUse());
						pointTreeInfoResult.setMeterTypeId(point.getMeterTypeId());
						pointTreeInfoResult.setName(point.getName());
						pointTreeInfoResult.setReconTypeId(point.getReconTypeId());
						pointTreeInfoResult.setParentId(device.getId());
						device.getPoints().add(pointTreeInfoResult);
					}
				}

				for (Device deviceTmp : deviceTmps) {
					if (point.getObjectId().intValue() == (deviceTmp.getId().intValue() - 10000)) {
						if (point.getMaxAlarmLevel() != null && point.getMaxAlarmLevel().intValue() > alarmLevel) {
							alarmLevel = point.getMaxAlarmLevel().intValue();
						}
					}
				}
			}

			device.setAlarmLevel(alarmLevel);
		}
	}

	/**
	 * 通过节点获取设备
	 * 
	 * @param deviceTmp
	 * @param devices
	 * @param deviceTmps
	 * @return
	 */
	private List<Device> getDeviceList(Device deviceTmp, List<Device> devices, List<Device> deviceTmps) {
		if (ProjectConsts.DEVICE_MODEL_LEVEL == deviceTmp.getModelLevel().intValue()) {
			deviceTmps.add(deviceTmp);
		}

		for (Device device : devices) {
			if (device.getParentId().intValue() == deviceTmp.getId().intValue()) {
				getDeviceList(device, devices, deviceTmps);
			}
		}

		return deviceTmps;
	}

	/**
	 * 重组设备树列表，将对象和对象类型组合进去
	 * 
	 * @param devices
	 * @param objectTreeVos
	 * @return
	 */
	private List<Device> recombineDevice(List<Device> devices, List<ObjectTreeResult> objectTreeResults) {
		List<String> objectTypeTemp = new ArrayList<>();
		for (ObjectTreeResult objectTreeResult : objectTreeResults) {
			if (!objectTypeTemp.contains(objectTreeResult.getDeviceId() + objectTreeResult.getObjectTypeName())) {
				Device objectType = new Device();
				objectType.setName(objectTreeResult.getObjectTypeName());
				objectType.setParentId(objectTreeResult.getDeviceId());
				// 由于设备表、对象表、组件表的id为自增的，但是树形结构id需要唯一，直接使用这些表的id字段可能会出现重复，所以对id进行重新生成。
				objectType.setId(1000 + objectTreeResult.getDeviceId() * 30 + objectTreeResult.getObjectTypeId());
				objectType.setModelLevel(ProjectConsts.DEVICE_TYPE_MODEL_LEVEL);
				objectType.setRealId(objectTreeResult.getObjectTypeId());
				devices.add(objectType);
				objectTypeTemp.add(objectTreeResult.getDeviceId() + objectTreeResult.getObjectTypeName());
			}

			Device object = new Device();
			object.setName(objectTreeResult.getObjectName());
			// 由于设备表、对象表、组件表的id为自增的，但是树形结构id需要唯一，直接使用这些表的id字段可能会出现重复，所以对id进行重新生成。
			object.setId(10000 + objectTreeResult.getObjectId());
			object.setModelLevel(ProjectConsts.DEVICE_MODEL_LEVEL);
			object.setParentId(1000 + objectTreeResult.getDeviceId() * 30 + objectTreeResult.getObjectTypeId());
			object.setRealId(objectTreeResult.getObjectId());
			devices.add(object);
		}
		return devices;
	}
}
