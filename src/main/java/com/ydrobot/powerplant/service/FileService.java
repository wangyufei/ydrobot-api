package com.ydrobot.powerplant.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {

	/**
	 * 上传文件
	 * 
	 * @param file
	 * @param extName
	 * @return
	 */
	public String uploadFile(MultipartFile file, String extName) throws IOException;

	/**
	 * 导入sql
	 * 
	 * @param file
	 */
	public void importSql(MultipartFile file);
}
