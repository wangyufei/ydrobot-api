package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.MeterPointInfo;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface MeterPointInfoService extends Service<MeterPointInfo> {

}
