package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.SysPointAlarmSetting;
import com.ydrobot.powerplant.model.condition.SysPointAlarmSettingQueryParam;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/04/27.
 */
public interface SysPointAlarmSettingService extends Service<SysPointAlarmSetting> {
	
	/**
	 * 获取系统点位报警设置列表
	 * @param queryParam
	 * @param page
	 * @param size
	 * @return
	 */
	public List<SysPointAlarmSetting> listSysPointAlarmSetting(SysPointAlarmSettingQueryParam queryParam, Integer page,Integer size);
}
