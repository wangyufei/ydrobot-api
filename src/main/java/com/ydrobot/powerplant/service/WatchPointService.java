package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.WatchPoint;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2019/04/12.
 */
public interface WatchPointService extends Service<WatchPoint> {
	
	/**
	 * 更新观察点信息
	 * @param id
	 * @param watchPoint
	 */
	void updateWatchPoint(Integer id,WatchPoint body);
}
