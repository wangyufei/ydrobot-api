package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PointHistoryConfirm;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/06/19.
 */
public interface PointHistoryConfirmService extends Service<PointHistoryConfirm> {

}
