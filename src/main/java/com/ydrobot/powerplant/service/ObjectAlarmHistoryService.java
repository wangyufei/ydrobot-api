package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/06/01.
 */
public interface ObjectAlarmHistoryService extends Service<ObjectAlarmHistory> {
	
	/**
	 * 添加对象告警记录
	 * @param objectAlarmHistory
	 */
	public void saveObjectAlarmHistory(ObjectAlarmHistory objectAlarmHistory);
}
