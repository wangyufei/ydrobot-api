package com.ydrobot.powerplant.service;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.condition.PointAppraiseHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.PointAppraiseHistoryResult;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;


/**
 * Created by Wyf on 2020/07/07.
 */
public interface PointAppraiseHistoryService extends Service<PointAppraiseHistory> {
	
	/**
	 * 获取点位评价记录列表
	 * @param queryParam
	 * @param page
	 * @param size
	 * @return
	 */
	public List<PointAppraiseHistoryResult> listPointAppraiseHistory(PointAppraiseHistoryQueryParam queryParam, Integer page,Integer size);
	
	/**
	 * 保存点位评价记录
	 * @param pointAppraiseHistory
	 */
	public void savePointAppraiseHistory(PointAppraiseHistory pointAppraiseHistory);
}
