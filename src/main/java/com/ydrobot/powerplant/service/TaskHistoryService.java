package com.ydrobot.powerplant.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.condition.TaskHistoryQueryParam;
import com.ydrobot.powerplant.model.condition.TaskMonthShowQueryParam;
import com.ydrobot.powerplant.model.dto.TaskHistoryResult;
import com.ydrobot.powerplant.model.request.TaskHistoryRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface TaskHistoryService extends Service<TaskHistory> {

	/**
	 * 获取任务执行记录列表
	 * 
	 * @param taskHistoryCondition
	 * @param page
	 * @param size
	 * @return
	 */
	public List<TaskHistoryResult> listTaskHistory(TaskHistoryQueryParam queryParam, Integer page, Integer size);

	/**
	 * 获取任务按月显示
	 * 
	 * @param taskMonthShowCondition
	 * @return
	 */
	public List<Map<String, Object>> listTaskMonthShow(TaskMonthShowQueryParam taskMonthShowCondition);

	/**
	 * 更新任务历史
	 * 
	 * @param taskHistory
	 * @param taskHistoryRequest
	 */
	public void updateTaskHistory(TaskHistory taskHistory, TaskHistoryRequest taskHistoryRequest);

	/**
	 * 巡检报告导出
	 * 
	 * @param taskHistoryIds
	 */
	public void exportReport(String taskHistoryIds, HttpServletResponse response);

	/**
	 * 获取当前任务报警数
	 * 
	 * @param robotId
	 * @return
	 */
	public Integer getCurrentTaskAlarmNum(Integer robotId);

	/**
	 * 获取当前任务的点位id列表
	 * 
	 * @param taskHistoryId
	 * @return
	 */
	public List<TaskPoint> listTaskPoint(Integer taskHistoryId);
	
	/**
	 * 获取某个任务最后巡检对象
	 * @param taskHistoryId
	 * @return
	 */
	public TObject getLastPatrolObject(Integer taskHistoryId);
	
	/**
	 * 获取某个任务的对象巡检顺序列表
	 * @param taskHistoryId
	 * @return
	 */
	public List<TObject> getPatrolObjects(Integer taskHistoryId);
	
	/**
	 * 获取某个任务已经巡检过的对象列表
	 * @param taskHistoryId
	 * @return
	 */
	public List<TObject> getAlreadyPatrolObjects(Integer taskHistoryId);

}
