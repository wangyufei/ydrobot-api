package com.ydrobot.powerplant.service;

import java.util.List;
import java.util.Map;

import com.ydrobot.powerplant.core.exception.ServiceException;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PathPlanning;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.StopPoint;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskPoint;
import com.ydrobot.powerplant.model.condition.TaskQueryParam;
import com.ydrobot.powerplant.model.dto.TaskResult;
import com.ydrobot.powerplant.model.condition.TaskExecutePlanQueryParam;
import com.ydrobot.powerplant.model.condition.TaskMonthShowQueryParam;
import com.ydrobot.powerplant.model.request.DeleteExecutePlanBatchRequest;
import com.ydrobot.powerplant.model.request.TaskPathRequest;
import com.ydrobot.powerplant.model.request.TaskRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface TaskService extends Service<Task> {

	/**
	 * 获取任务列表
	 * 
	 * @param taskCondition
	 * @param page
	 * @param size
	 * @return
	 */
	public List<TaskResult> listTask(TaskQueryParam queryParam, Integer page, Integer size);

	/**
	 * 获取任务点位
	 * 
	 * @param taskId
	 * @return
	 */
	public List<TaskPoint> listTaskPoint(Integer taskId);

	/**
	 * 保存任务
	 * 
	 * @param createTaskRequest
	 * @return
	 */
	public Task saveTask(TaskRequest taskRequest);

	/**
	 * 更新任务
	 * 
	 * @param task
	 * @param updateTaskRequest
	 * @return
	 */
	public Task updateTask(Task task, TaskRequest taskRequest);

	/**
	 * 获取当前任务是否可以编辑
	 * 
	 * @param task
	 * @return
	 */
	public Boolean getEditStatus(Task task);

	/**
	 * 获取任务按月显示
	 * 
	 * @param taskMonthShowCondition
	 * @return
	 */
	public List<Map<String, Object>> listTaskMonthShow(TaskMonthShowQueryParam queryParam);

	/**
	 * 获取任务执行计划列表
	 * 
	 * @param taskExecutePlanCondition
	 * @return
	 */
	public List<Map<String, Object>> listTaskExecutePlan(TaskExecutePlanQueryParam queryParam);

	/**
	 * 获取任务当前路径
	 * 
	 * @param task
	 * @return
	 */
	public PathPlanning getTaskCurrentPath(Integer taskHistoryId);

	/**
	 * 获取任务执行优先级
	 * 
	 * @param taskId
	 * @return
	 */
	public Integer getTaskLevel(Integer taskId);

	/**
	 * 删除执行计划
	 * 
	 * @param executePlanCondition
	 */
	public void deleteExecutePlan(DeleteExecutePlanBatchRequest deleteExecutePlanBatchRequest);

	/**
	 * 删除任务
	 * 
	 * @param taskId
	 */
	public void deleteTask(Task task);

	/**
	 * 获取任务执行状态
	 * 
	 * @param id
	 * @return
	 */
	public boolean getTaskExecutStatus(Integer id);

	/**
	 * 更新任务路径
	 * 
	 * @param id
	 * @param taskPathRequest
	 */
	public void updateTaskPath(Integer id, TaskPathRequest taskPathRequest) throws ServiceException;

	/**
	 * 获取任务路径
	 * 
	 * @param id
	 * @return
	 */
	public PathPlanning getTaskPath(Integer id);

	/**
	 * 获取任务巡检点
	 * 
	 * @param id
	 * @return
	 */
	public List<Point> listTaskCheckPoints(Integer id);

	/**
	 * 获取任务停靠点
	 * 
	 * @param id
	 * @return
	 */
	public List<StopPoint> listTaskStopPoint(Integer id);

	/**
	 * 通过巡检类型获取任务列表
	 * 
	 * @param inspectTypeIds
	 * @return
	 */
	public List<Task> listTaskByInspectTypeIds(String inspectTypeIds);

	/**
	 * 通过任务名模糊查询任务列表
	 * 
	 * @param taskName
	 * @return
	 */
	public List<Task> listTaskByName(String taskName);
	
	/**
	 * 创建应急任务
	 * @param pointIds
	 * @param inspectTypeId 6000:自动应急巡检 7000:手动应急巡检
	 * @return
	 */
	public Task saveEmergencyTask(List<Integer> pointIds,Integer inspectTypeId);
	
	/**
	 * 更新所有任务的更新状态
	 */
	public void updateAllTaskUpdateStatus();
}
