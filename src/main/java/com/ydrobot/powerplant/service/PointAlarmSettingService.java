package com.ydrobot.powerplant.service;

import java.util.List;
import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.PointAlarmSetting;
import com.ydrobot.powerplant.model.condition.PointAlarmSettingQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmSettingResult;
import com.ydrobot.powerplant.model.request.UpdatePointAlarmSettingRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface PointAlarmSettingService extends Service<PointAlarmSetting> {

    /**
     * 批量更新点位报警参数
     * @param updatePointAlarmSettingRequest
     */
    public void batchUpdate(UpdatePointAlarmSettingRequest updatePointAlarmSettingRequest);
    
    /**
     * 获取点位报警设置列表
     * @param queryParam
     * @param page
     * @param size
     * @return
     */
    public List<PointAlarmSettingResult> listPointAlarmSetting(PointAlarmSettingQueryParam queryParam,Integer page,Integer size);

    /**
     * 同步关联点三相温差和三相对比设置
     * @param pointId
     */
    public void syncReferencePointSetting(Integer pointId, Byte type);
}
