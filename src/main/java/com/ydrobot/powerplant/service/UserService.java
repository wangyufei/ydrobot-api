package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.Permission;
import com.ydrobot.powerplant.model.User;
import com.ydrobot.powerplant.model.condition.UserQueryParam;
import com.ydrobot.powerplant.model.dto.UserResult;
import com.ydrobot.powerplant.model.request.LoginRequest;
import com.ydrobot.powerplant.model.request.UserRequest;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface UserService extends Service<User> {
	
	/**
	 * 获取用户列表
	 * @param queryParam
	 * @param page
	 * @param size
	 * @return
	 */
	public List<UserResult> listUser(UserQueryParam queryParam,Integer page, Integer size);
	
	/**
	 * 通过组织id获取用户列表
	 * @param organizationId
	 * @return
	 */
	public List<User> listUserByOrganizationId(Integer organizationId);
	
    /**
     * 通过账号密码查找用户
     * @param loginForm
     * @return
     */
    public User getUserByAccountAndPasswod(LoginRequest loginRequest);

    /**
     * 检查账号是否存在
     * @param account
     * @return
     */
    public User getUserByAccount(String account);

    /**
     * 获取token
     * @param user
     * @return
     */
    public String getToken(User user);

    /**
     * 获取已经存在的token
     * @param user
     * @return
     */
    public String getExistToken(User user);

    /**
     * 设置用户登录缓存
     * @param user
     * @param token
     */
    public void setRedisUser(User user, String token);

    /**
     * 退出登录
     * @param token
     */
    public void logout(String token);

    /**
     * 通过token获取用户信息
     * @param token
     * @return
     */
    public User getRedisUserByToken(String token);

    /**
     * 创建用户
     * @param createUserRequest
     */
    public void saveUser(UserRequest userRequest);

    /**
     * 更新用户
     * @param user
     * @param updateUserRequest
     */
    public void updateUser(User user, UserRequest userRequest);

    /**
     * 获取用户权限
     * @param user
     * @return
     */
    public List<Permission> listUserPermission(User user);
}
