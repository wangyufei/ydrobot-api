package com.ydrobot.powerplant.service;

import com.ydrobot.powerplant.model.Route;
import com.ydrobot.powerplant.model.dto.RouteResult;
import com.ydrobot.powerplant.model.request.RouteMaintainRequest;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface RouteService extends Service<Route> {

	/**
	 * 获取道路和道路点
	 * 
	 * @return
	 */
	public List<RouteResult> listRouteWithRoutePos();

	/**
	 * 更新道路状态
	 * 
	 * @param id
	 * @param route
	 */
	public void updateRoute(Integer id, Route route);

	/**
	 * 添加道路维修信息
	 * 
	 * @param routeMaintainRequest
	 */
	public void saveRouteMaintain(List<RouteMaintainRequest> routeMaintainRequests);
}
