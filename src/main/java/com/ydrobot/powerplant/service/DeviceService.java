package com.ydrobot.powerplant.service;

import java.util.List;

import com.ydrobot.powerplant.core.service.Service;
import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.condition.DeviceIntervalQueryParam;

/**
 * Created by Wyf on 2019/04/12.
 */
public interface DeviceService extends Service<Device> {

	/**
	 * 获取区域列表
	 * 
	 * @return
	 */
	public List<Device> listArea();

	/**
	 * 获取间隔列表
	 * 
	 * @param areaId
	 * @return
	 */
	public List<Device> listInterval(DeviceIntervalQueryParam queryParam);

	/**
	 * 获取某个间隔下的所有设备
	 * 
	 * @param intervalId
	 * @return
	 */
	public List<Device> listDevicesByIntervalId(Integer intervalId);

	/**
	 * 通过设备id获取设备区域名称
	 * 
	 * @param id
	 * @return
	 */
	public String getDeviceAreaNameByDeviceId(Integer id);

	/**
	 * 通过设备id获取设备间隔名称
	 * 
	 * @param id
	 * @return
	 */
	public String getDeviceIntervalNameByDeviceId(Integer id);
	
	/**
	 * 获取树
	 * @return
	 */
	public List<Device> getDeviceTree();
}
