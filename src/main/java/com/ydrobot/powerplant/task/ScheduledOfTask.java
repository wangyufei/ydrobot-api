package com.ydrobot.powerplant.task;

import com.ydrobot.powerplant.core.utils.ProjectSpringUtils;
import com.ydrobot.powerplant.model.Cron;
import com.ydrobot.powerplant.model.status.StatusEnum;
import com.ydrobot.powerplant.service.CronService;

public interface ScheduledOfTask extends Runnable{
	
	/**
     * 定时任务方法
     */
    void execute();

    /**
     * 实现控制定时任务启用或禁用的功能
     */
    @Override
    default void run() {
    		CronService cronService = ProjectSpringUtils.getBean(CronService.class);
        Cron cron = cronService.findBy("cronKey", this.getClass().getName());
        if (StatusEnum.DISABLED.getCode().equals(cron.getStatus())) {
            return;
        }
        execute();
    }
}
