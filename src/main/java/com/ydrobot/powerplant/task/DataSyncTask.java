package com.ydrobot.powerplant.task;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ydrobot.powerplant.core.constant.TableNameConsts;
import com.ydrobot.powerplant.model.ObjectAlarmHistory;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointAppraise;
import com.ydrobot.powerplant.model.PointAppraiseHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.SyncFailed;
import com.ydrobot.powerplant.model.SysPointAlarmHistory;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.service.DataSyncService;
import com.ydrobot.powerplant.service.ObjectAlarmHistoryService;
import com.ydrobot.powerplant.service.PointAlarmHistoryService;
import com.ydrobot.powerplant.service.PointAppraiseHistoryService;
import com.ydrobot.powerplant.service.PointAppraiseService;
import com.ydrobot.powerplant.service.PointHistoryService;
import com.ydrobot.powerplant.service.SyncFailedService;
import com.ydrobot.powerplant.service.SysPointAlarmHistoryService;
import com.ydrobot.powerplant.service.TaskHistoryService;
import com.ydrobot.powerplant.service.TaskService;

@Component
public class DataSyncTask implements ScheduledOfTask {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private DataSyncService dataSyncService;
	@Resource
	private SyncFailedService syncFailedService;
	@Resource
	private PointAppraiseService pointAppraiseService;
	@Resource
	private PointAppraiseHistoryService pointAppraiseHistoryService;
	@Resource
	private PointAlarmHistoryService pointAlarmHistoryService;
	@Resource
	private PointHistoryService pointHistoryService;
	@Resource
	private TaskService taskService;
	@Resource
	private TaskHistoryService taskHistoryService;
	@Resource
	private ObjectAlarmHistoryService objectAlarmHistoryService;
	@Resource
	private SysPointAlarmHistoryService sysPointAlarmHistoryService;

	@Override
	public void execute() {
		List<SyncFailed> syncFaileds = syncFailedService.findAll();

		for (SyncFailed syncFailed : syncFaileds) {
			switch (syncFailed.getTableName()) {
			case TableNameConsts.POINT_APPRAISE:
				pointAppraiseSync(syncFailed);
				break;
			case TableNameConsts.POINT_APPRAISE_HISTORY:
				pointAppraiseHistorySync(syncFailed);
				break;
			case TableNameConsts.POINT_HISTORY:
				pointHistorySync(syncFailed);
				break;
			case TableNameConsts.POINT_ALARM_HISTORY:
				pointAlarmHistorySync(syncFailed);
				break;
			case TableNameConsts.TASK:
				taskSync(syncFailed);
				break;
			case TableNameConsts.TASK_HISTORY:
				taskHistorySync(syncFailed);
				break;
			case TableNameConsts.OBJECT_ALARM_HISTORY:
				objectAlarmHistorySync(syncFailed);
				break;
			case TableNameConsts.SYS_POINT_ALARM_HISTORY:
				sysPointAlarmHistorySync(syncFailed);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 点位评价状态同步
	 * 
	 * @param syncFailed
	 */
	private void pointAppraiseSync(SyncFailed syncFailed) {
		PointAppraise pointAppraise = pointAppraiseService.findById(syncFailed.getDataId());
		if (pointAppraise != null) {
			String result = dataSyncService.pointAppraiseSync(pointAppraise);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		} else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}

	/**
	 * 点位评价记录同步
	 * 
	 * @param syncFailed
	 */
	private void pointAppraiseHistorySync(SyncFailed syncFailed) {
		PointAppraiseHistory pointAppraiseHistory = pointAppraiseHistoryService.findById(syncFailed.getDataId());
		if (pointAppraiseHistory != null) {
			String result = dataSyncService.pointAppraiseHistorySync(pointAppraiseHistory);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		} else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}

	/**
	 * 点位报警记录同步
	 * 
	 * @param syncFailed
	 */
	private void pointAlarmHistorySync(SyncFailed syncFailed) {
		PointAlarmHistory pointAlarmHistory = pointAlarmHistoryService.findById(syncFailed.getDataId());
		if (pointAlarmHistory != null) {
			String result = dataSyncService.pointAlarmHistorySync(pointAlarmHistory);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		} else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}
	
	/**
	 * 系统点位报警记录同步
	 * 
	 * @param syncFailed
	 */
	private void sysPointAlarmHistorySync(SyncFailed syncFailed) {
		SysPointAlarmHistory sysPointAlarmHistory = sysPointAlarmHistoryService.findById(syncFailed.getDataId());
		if (sysPointAlarmHistory != null) {
			String result = dataSyncService.sysPointAlarmHistorySync(sysPointAlarmHistory);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		} else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}

	/**
	 * 点位记录同步
	 * @param syncFailed
	 */
	private void pointHistorySync(SyncFailed syncFailed) {
		PointHistory pointHistory = pointHistoryService.findById(syncFailed.getDataId());
		if (pointHistory != null) {
			String result = dataSyncService.pointHistorySync(pointHistory);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		} else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}
	
	/**
	 * 任务同步
	 * @param syncFailed
	 */
	private void taskSync(SyncFailed syncFailed) {
		Task task = taskService.findById(syncFailed.getDataId());
		if (task!=null) {
			String result = dataSyncService.TaskSync(task);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		}else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}
	
	/**
	 * 任务记录同步
	 * @param syncFailed
	 */
	private void taskHistorySync(SyncFailed syncFailed) {
		TaskHistory taskHistory = taskHistoryService.findById(syncFailed.getId());
		if (taskHistory!=null) {
			String result = dataSyncService.taskHistorySync(taskHistory);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		}else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}
	
	/**
	 * 对象报警记录同步
	 * @param syncFailed
	 */
	private void objectAlarmHistorySync(SyncFailed syncFailed) {
		ObjectAlarmHistory objectAlarmHistory = objectAlarmHistoryService.findById(syncFailed.getDataId());
		if (objectAlarmHistory!=null) {
			String result = dataSyncService.objectAlarmHistorySync(objectAlarmHistory);
			if ("1".equals(result)) {
				syncFailedService.deleteById(syncFailed.getId());
			}
		}else {
			syncFailedService.deleteById(syncFailed.getId());
		}
	}
}
