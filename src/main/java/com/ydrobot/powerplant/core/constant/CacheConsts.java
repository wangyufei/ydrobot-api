package com.ydrobot.powerplant.core.constant;

public class CacheConsts {

	// tonken过期时间
	public static final int TOKEN_TIMEOUT = 6 * 60 * 60;

	// token
	public static final String TOKEN_MAP = "tokenMap:";

	// 用户前缀
	public static final String USER_PREFIX = "user:";

	// 用户token前缀
	public static final String USER_TOKEN_PREFIX = "userToken:";
}
