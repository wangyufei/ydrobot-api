package com.ydrobot.powerplant.core.utils;

public class ProjectStatusUtils {

	/**
	 * 获取任务状态
	 * 
	 * @param taskStatus
	 * @return
	 */
	public static String getTaskStatus(Integer taskStatus) {
		String status = "未知";
		switch (taskStatus) {
		case 0:
			status = "已执行";
			break;
		case 1:
			status = "终止";
			break;
		case 2:
			status = "暂停";
			break;
		case 3:
			status = "正在执行";
			break;
		case 4:
			status = "未执行";
			break;
		case 5:
			status = "超期";
			break;
		default:
			break;
		}

		return status;
	}

	/**
	 * 获取报警等级
	 * 
	 * @param alarmLevel
	 * @return
	 */
	public static String getAlarmLevel(Integer alarmLevel) {
		// 告警等级 0:正常 1:预警 2:一般告警 3:严重告警 4:危机告警
		String level = "未知";
		switch (alarmLevel) {
		case 0:
			level = "正常";
			break;
		case 1:
			level = "预警";
			break;
		case 2:
			level = "一般告警";
			break;
		case 3:
			level = "严重告警";
			break;
		case 4:
			level = "危机告警";
			break;
		default:
			break;
		}
		return level;
	}

	/**
	 * 获取审核状态
	 * 
	 * @param checkStatus
	 * @return
	 */
	public static String getCheckStatus(Integer checkStatus) {
		String status = "未知";
		switch (checkStatus) {
		case 0:
			status = "未审核";
			break;
		case 1:
			status = "已审核";
			break;
		default:
			break;
		}
		return status;
	}
}
