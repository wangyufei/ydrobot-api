package com.ydrobot.powerplant.core.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProjectUrlConvert {

	public static String webDomain;

	@Value("${web.domain}")
	public void setDomain(String domain) {
		webDomain = domain;
	}

	public static String covert2HttpUrl(String sourceUrl) {
		if (StringUtils.isNotBlank(sourceUrl)) {
			if (sourceUrl.indexOf("http") < 0) {
				if (webDomain != null) {
					return webDomain + sourceUrl;
				}
			}
		}

		return sourceUrl;
	}

	public static String covert2HttpUrl(String sourceUrl, String prifix) {
		if (StringUtils.isNotBlank(sourceUrl)) {

			// 该判断主要解决资源在保存时会带http的问题
			if (sourceUrl.indexOf("&&") == 0) {
				return sourceUrl.substring(2);
			}
			
			if (sourceUrl.indexOf("http") < 0) {
				if (webDomain != null) {
					return webDomain + sourceUrl;
				}
			}
		}

		return sourceUrl;
	}

	public static String covert2LocalUrl(String sourceUrl, String prifix) {
		if (StringUtils.isNotBlank(sourceUrl)) {
			int pos = sourceUrl.indexOf(prifix);
			if (pos > -1) {
				return "/" + sourceUrl.substring(pos);
			}
		}
		return sourceUrl;
	}
}
