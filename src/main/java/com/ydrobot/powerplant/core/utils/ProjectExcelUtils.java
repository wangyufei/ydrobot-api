package com.ydrobot.powerplant.core.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.factory.annotation.Value;

/**
 * excel工具类
 * 
 * @author wangyufei
 *
 */
public class ProjectExcelUtils {
	
	@Value("${file.path}")
	private static String excelPath;

	/**
	 * 导出zip包
	 * 
	 * @param response
	 * @param fileNames
	 * @throws IOException
	 */
	public static void exportZip(HttpServletResponse response, List<String> fileNames) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// 导出压缩文件的全路径
		String zipFilePath = excelPath + File.separator + sdf.format(new Date()) + ".zip";
		// 导出zip
		File zip = new File(zipFilePath);
		// 将excel文件生成压缩文件
		File srcfile[] = new File[fileNames.size()];
		for (int j = 0, n1 = fileNames.size(); j < n1; j++) {
			srcfile[j] = new File(fileNames.get(j));
		}
		ZipFiles(srcfile, zip);
		response.setContentType("application/zip");
		response.setHeader("Location", zip.getName());
		response.setHeader("Content-Disposition", "attachment; filename=" + zip.getName());
		OutputStream outputStream = response.getOutputStream();
		InputStream inputStream = new FileInputStream(zipFilePath);
		byte[] buffer = new byte[1024];
		int i = -1;
		while ((i = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, i);
		}
		outputStream.flush();
		outputStream.close();
		inputStream.close();
		outputStream = null;

		try {
			String filePath = excelPath + File.separator;
			delAllFile(filePath); // 删除完里面所有内容
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 压缩文件
	 * 
	 * @param srcfile
	 * @param zipfile
	 */
	public static void ZipFiles(File[] srcfile, File zipfile) {
		byte[] buf = new byte[1024];
		try {
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
			for (int i = 0; i < srcfile.length; i++) {
				FileInputStream in = new FileInputStream(srcfile[i]);
				out.putNextEntry(new ZipEntry(srcfile[i].getName()));
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.closeEntry();
				in.close();
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/***
	 * 删除指定文件夹下所有文件
	 *
	 * @param path
	 *            文件夹完整绝对路径
	 * @return
	 */
	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * excel 插入图片
	 * 
	 * @param workbook
	 * @param sheet
	 * @param imgurl
	 * @param col1
	 * @param row1
	 * @param col2
	 * @param row2
	 */
	public static void excelInsertImg(HSSFWorkbook workbook, HSSFSheet sheet, String img, short col1, int row1,
			short col2, int row2) {
		BufferedImage bufferImg = null;

		try {
			ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
			URL url = new URL(img);
			bufferImg = ImageIO.read(url);
			ImageIO.write(bufferImg, "png", byteArrayOut);
			HSSFClientAnchor anchor = new HSSFClientAnchor(5, 5, 0, 0, col1, row1, col2, row2);
			HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
			patriarch.createPicture(anchor,
					workbook.addPicture(byteArrayOut.toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * excel写入
	 * 
	 * @param workbook
	 * @param fullFilePath
	 * @throws IOException
	 */
	public static void write(HSSFWorkbook workbook, String fullFilePath) throws IOException {
		FileOutputStream os = null;
		os = new FileOutputStream(fullFilePath);
		workbook.write(os);

		// 清空流缓冲区数据
		os.flush();
		// 关闭流
		os.close();
	}

	/**
	 * 下载Excel
	 * 
	 * @param response
	 * @param fileName
	 */
	public static void down(HttpServletResponse response, String fileName, HSSFWorkbook wb) {
		try {
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
			response.flushBuffer();
			wb.write(response.getOutputStream());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 设置excel样式
	 * 
	 * @param workbook
	 */
	public static HSSFCellStyle excelStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = workbook.createCellStyle();

		// 设置边框
		style.setBottomBorderColor(IndexedColors.BLACK.index);
		style.setTopBorderColor(IndexedColors.BLACK.index);
		style.setLeftBorderColor(IndexedColors.BLACK.index);
		style.setRightBorderColor(IndexedColors.BLACK.index);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);

		// 设置居中显示
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);

		// 设置字体
		Font font = workbook.createFont();
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setFontHeightInPoints((short) 16);
		style.setFont(font);

		style.setWrapText(true);

		return style;
	}

	/**
	 * 设置表格链接样式
	 * 
	 * @param workbook
	 * @return
	 */
	public static HSSFCellStyle setExcelLinkStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = workbook.createCellStyle();
		HSSFFont cellFont = workbook.createFont();
		cellFont.setUnderline((byte) 1);
		cellFont.setColor(IndexedColors.BLUE.index);
		style.setFont(cellFont);

		// 设置边框
		style.setBottomBorderColor(IndexedColors.BLACK.index);
		style.setTopBorderColor(IndexedColors.BLACK.index);
		style.setLeftBorderColor(IndexedColors.BLACK.index);
		style.setRightBorderColor(IndexedColors.BLACK.index);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);

		// 设置居中显示
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);

		// 设置字体
		Font font = workbook.createFont();
		font.setColor(IndexedColors.BLUE.index);
		font.setFontHeightInPoints((short) 16);
		style.setFont(font);

		return style;
	}
}
