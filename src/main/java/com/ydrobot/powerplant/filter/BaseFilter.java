package com.ydrobot.powerplant.filter;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.ydrobot.powerplant.core.result.Result;
import com.ydrobot.powerplant.core.result.ResultCode;

/**
 * @Auther: wangyufei
 * @Date: 23/07/2018 19:12
 * @Description:
 */
public class BaseFilter {

    protected void writeExpirie(HttpServletResponse response, String msg) throws IOException {
        Result result = new Result();
        if (msg == null) {
            result.setCode(ResultCode.UNAUTHORIZED);
        } else {
            result.setCode(ResultCode.INTERNAL_SERVER_ERROR);
        }

        writePage(response, JSON.toJSONString(result), true);
    }

    protected void writeFail(HttpServletResponse response, String msg) throws IOException {
        Result result = new Result();
        result.setCode(ResultCode.FAIL);
        if (msg != null) {
            result.setMessage(msg);
        }
        writePage(response, JSON.toJSONString(result), true);
    }

    protected void writePage(HttpServletResponse response, String content, boolean json) throws IOException {
        if (json) {
            response.setContentType("application/json;charset=utf-8");
        } else {
            response.setContentType("text/html;charset=utf-8");
        }

        response.getWriter().println(content);
        response.getWriter().close();
    }
}
