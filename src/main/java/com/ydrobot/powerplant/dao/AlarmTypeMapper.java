package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.AlarmType;

public interface AlarmTypeMapper extends Mapper<AlarmType> {
}