package com.ydrobot.powerplant.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.TaskHistory;
import com.ydrobot.powerplant.model.condition.TaskHistoryQueryParam;
import com.ydrobot.powerplant.model.dto.CurrentTaskInfoResult;
import com.ydrobot.powerplant.model.dto.TaskHistoryResult;

public interface TaskHistoryMapper extends Mapper<TaskHistory> {
	
	/**
	 * 获取任务执行记录列表
	 * @param queryParam
	 * @return
	 */
	List<TaskHistoryResult> getList(@Param("queryParam") TaskHistoryQueryParam queryParam);
	
	/**
	 * 通过机器人id获取正在执行任务
	 * @param robotId
	 * @return
	 */
    @Select("select * from task_history where robot_id = #{robotId} and (task_status = 3 or task_status = 2) order by create_time desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.TaskHistoryMapper.BaseResultMap")
    TaskHistory getTaskHistoryByRobotId(@Param("robotId") Integer robotId);
    
    /**
     * 获取机器人当前正在执行的任务
     * @param robotId
     * @return
     */
    @Select("select id,task_id, task_status,task_start_time,"
    		+ "total_run_time,cumulative_run_time,restart_start_time "
    		+ "from task_history "
    		+ "where robot_id = #{robotId} and (task_status = 3 or task_status = 2) "
    		+ "order by create_time desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.TaskHistoryMapper.RobotCurrentExecutTaskMap")
    CurrentTaskInfoResult getRobotCurrentExecutTask(@Param("robotId") Integer robotId);

    /**
     * 通过机器人id获取当前任务
     * @param robotId
     * @return
     */
    @Select("select * from task_history where robot_id = #{robotId} and (task_status = 3 or task_status = 2 or task_status = 6)")
    @ResultMap("com.ydrobot.powerplant.dao.TaskHistoryMapper.BaseResultMap")
    List<TaskHistory> getCurrentTaskByRobotId(@Param("robotId") Integer robotId);

    /**
     * 通过时间获取任务历史分组
     * @param robotId
     * @param startTime
     * @param endTime
     * @return
     */
    @Select("select DATE_FORMAT(task_start_time,'%Y-%m-%d') as groupDate,count(id) as count from task_history where robot_id = #{robotId} and task_start_time >= #{startTime} and task_start_time < #{endTime} group by groupDate")
    List<Map<String, Object>> findByExecuteTime(@Param("robotId") Integer robotId, @Param("startTime") String startTime,
            @Param("endTime") String endTime);

    /**
     * 通过机器人id和时间获取任务历史信息
     * @param robotId
     * @param startTime
     * @param endTime
     * @return
     */
    @Select("select * from task_history where robot_id = #{robotId} and task_start_time >= #{startTime} and task_start_time < #{endTime} and task_status!=6")
    @ResultMap("com.ydrobot.powerplant.dao.TaskHistoryMapper.BaseResultMap")
    List<TaskHistory> getTaskHistoryByRobotIdAndTaskStartDateBetween(@Param("robotId") Integer robotId,
            @Param("startTime") String startTime, @Param("endTime") String endTime);

    /**
     * 查找暂停和正在执行的状态任务
     * @param taskId
     * @param pauseStatus
     * @param currentExecuteStatus
     * @return
     */
    @Select("select * from task_history where task_id = #{taskId} and (task_status = #{pauseStatus} or task_status = #{currentExecuteStatus})")
    @ResultMap("com.ydrobot.powerplant.dao.TaskHistoryMapper.BaseResultMap")
    List<TaskHistory> getTaskHistoryByStatus(@Param("taskId") Integer taskId,
            @Param("pauseStatus") Integer pauseStatus, @Param("currentExecuteStatus") Integer currentExecuteStatus);
    
    /**
     * 通过任务记录id获取巡检类型id
     * @param taskHistoryId
     * @return
     */
    @Select("select t.inspect_type_id from task as t left join task_history as th on t.id = th.task_id where th.id = #{taskHistoryId}")
    Integer getInspectTypeIdByTaskHistoryId(@Param("taskHistoryId") Integer taskHistoryId);
    
    /**
     * 通过对象id获取最近的一次任务执行记录
     * @param objectId
     * @return
     */
    @Select("select th.* from task_history as th "
    		+ "left join point_history as ph on th.id = ph.task_history_id "
    		+ "left join point as p on p.id=ph.point_id"
    		+ " where p.object_id = #{objectId} order by th.create_time desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.TaskHistoryMapper.BaseResultMap")
    TaskHistory getLastTaskHistoryByObjectId(@Param("objectId") Integer objectId);
    
}