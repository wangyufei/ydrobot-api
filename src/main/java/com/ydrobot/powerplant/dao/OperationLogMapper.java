package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.OperationLog;
import com.ydrobot.powerplant.model.condition.OperationLogQueryParam;
import com.ydrobot.powerplant.model.dto.OperationLogResult;

public interface OperationLogMapper extends Mapper<OperationLog> {
	
	/**
	 * 获取操作日志列表
	 * @param queryParam
	 * @return
	 */
	public List<OperationLogResult> getList(@Param("queryParam") OperationLogQueryParam queryParam);
}