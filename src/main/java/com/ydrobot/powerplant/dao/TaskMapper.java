package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.condition.TaskQueryParam;
import com.ydrobot.powerplant.model.dto.TaskResult;

public interface TaskMapper extends Mapper<Task> {
	
	/**
	 * 获取任务列表
	 * @param queryParam
	 * @return
	 */
	List<TaskResult> getList(@Param("queryParam") TaskQueryParam queryParam);
	
	/**
	 * 获取应急任务
	 * @return
	 */
	@Select("select * from task where inspect_type_id = 6000 limit 0,1 ")
	@ResultMap("com.ydrobot.powerplant.dao.TaskMapper.BaseResultMap")
	Task getEmergencyTask();
	
	/**
	 * 通过任务记录id获取任务信息
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select t.* from task as t left join task_history as th on t.id = th.task_id where th.id = #{taskHistoryId}")
	@ResultMap("com.ydrobot.powerplant.dao.TaskMapper.BaseResultMap")
	Task getTaskByTaskHistoryId(@Param("taskHistoryId") Integer taskHistoryId);
	
	/**
	 * 更新任务的isupdate状态
	 * @param taskHistoryId
	 * @return
	 */
	@Select("update task set is_update = 1 where id in (select task_id from task_point where point_id = #{pointId})")
	void updateTaskIsUpdateStatus(@Param("pointId") Integer pointId);
	
	/**
	 * 更新所有任务的isupdate状态为1
	 */
	@Select("update task set is_update = 1")
	void updateAllTaskIsUpdateStatus();
}