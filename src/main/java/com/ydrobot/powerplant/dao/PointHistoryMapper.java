package com.ydrobot.powerplant.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.condition.PointHistoryQueryParam;
import com.ydrobot.powerplant.model.condition.PointHistoryReportQueryParam;
import com.ydrobot.powerplant.model.condition.VerificationReportQueryParam;
import com.ydrobot.powerplant.model.dto.PointHistoryContrastResult;
import com.ydrobot.powerplant.model.dto.PointHistoryCurveResult;
import com.ydrobot.powerplant.model.dto.PointHistoryReportResult;
import com.ydrobot.powerplant.model.dto.PointHistoryResult;
import com.ydrobot.powerplant.model.dto.PointHistoryVerificationReportResult;
import com.ydrobot.powerplant.model.dto.PointPatrolResult;
import com.ydrobot.powerplant.model.dto.PointVerificationReportResult;

public interface PointHistoryMapper extends Mapper<PointHistory> {
	
	/**
	 * 获取点位巡检记录列表
	 * @param queryParam
	 * @return
	 */
	List<PointHistoryResult> getList(@Param("queryParam") PointHistoryQueryParam queryParam);
	
    /**
     * 通过任务执行记录id获取点位历史
     * @param taskHistoryId
     * @return
     */
    @Select("select * from point_history where task_history_id = #{taskHistoryId} order by create_time desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PointHistoryMapper.BaseResultMap")
    PointHistory getPointHistoryByTaskHistoryId(@Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取某个点的最后一条历史记录
     * @param pointId
     * @return
     */
    @Select("select * from point_history where point_id = #{pointId} order by create_time desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PointHistoryMapper.BaseResultMap")
    PointHistory getPointHistoryLast(@Param("pointId") Integer pointId);

    /**
     * 获取多个点的最后一条历史记录
     * @param pointIds
     * @return
     */
    @Select("select * from (select * from point_history where point_id in (${pointIds}) order by create_time desc) as t group by t.point_id")
    @ResultMap("com.ydrobot.powerplant.dao.PointHistoryMapper.BaseResultMap")
    List<PointHistory> getManyPointHistoryLast(@Param("pointIds") String pointIds);

    /**
     * 获取某个点位的历史数据
     * @param pointId
     * @param startTime
     * @param endTime
     * @return
     */
    @Select("select value,DATE_FORMAT(create_time,'%Y-%c-%d %h:%i') as createTime  from point_history where point_id = #{pointId} and create_time >= #{startTime} and create_time < #{endTime}")
    List<Map<String, Object>> getPointHistory(@Param("pointId") Integer pointId, @Param("startTime") String startTime,
            @Param("endTime") String endTime);

    /**
     * 获取正常点位
     * @param taskHistoryId
     * @return
     */
    @Select("select * from point_history where task_history_id = #{taskHistoryId} and recon_status = 0 and id not in (select distinct point_history_id from point_alarm_history where task_history_id = #{taskHistoryId} and result_status = 1)")
    @ResultMap("com.ydrobot.powerplant.dao.PointHistoryMapper.BaseResultMap")
    List<PointHistory> getNormalPointHistory(@Param("taskHistoryId") Integer taskHistoryId);
    
    /**
     * 获取巡检记录报表
     * @param queryParam
     * @return
     */
    List<PointHistoryReportResult> getPointHistoryReportList(@Param("queryParam") PointHistoryReportQueryParam queryParam);
    
    /**
     * 获取点位历史对比数据
     * @param queryParam
     * @return
     */
    List<PointHistoryContrastResult> getPointHistoryContrastList(@Param("queryParam") PointHistoryQueryParam queryParam);
    
    /**
     * 获取部件核查报告列表
     * @param queryParam
     * @return
     */
    List<PointVerificationReportResult> getPointVerificationReportResult(@Param("queryParam") VerificationReportQueryParam queryParam);
    
    /**
     * 获取点位历史核查报告
     * @param pointId
     * @param taskHistoryId
     * @return
     */
    List<PointHistoryVerificationReportResult> getPointHistoryVerificationReportResult(@Param("pointId") Integer pointId, @Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取设备报警数
     * @param objectId
     * @param taskHistoryId
     * @return
     */
    Integer getObjectAlarmNum(@Param("objectId") Integer objectId,@Param("taskHistoryId") Integer taskHistoryId);
    
    /**
     * 获取某个部件的历史曲线
     * @param pointId
     * @param startTime
     * @return
     */
    List<PointHistoryCurveResult> countPointHistoryCurve(@Param("pointId") Integer pointId,@Param("startTime") String startTime);
    
    /**
     * 通过任务记录id获取对象信息
     * @param taskHistoryId
     * @return
     */
    @Select("select o.* from object as o left join point as p on o.id = p.object_id left join point_history as ph on ph.point_id = p.id where ph.task_history_id = #{taskHistoryId} limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
    TObject getObjectByTaskHistoryId(@Param("taskHistoryId") Integer taskHistoryId);
    
    /**
     * 获取二次确认任务执行记录id
     * @param pointHistoryId
     * @return
     */
    @Select("select th.id from point_history_confirm as phc left join task_history as th on phc.task_id = th.task_id where phc.point_history_id = #{pointHistoryId} order by phc.id desc limit 1")
    Integer getPointHistoryConfirmTaskHistoryId(@Param("pointHistoryId") Integer pointHistoryId);
    
    /**
     * 获取二次确认任务id
     * @param pointHistoryId
     * @return
     */
    @Select("select task_id from point_history_confirm where point_history_id = #{pointHistoryId} order by id desc limit 1")
    Integer getPointHistoryConfirmTaskId(@Param("pointHistoryId") Integer pointHistoryId);
    
    /**
     * 通过记录id获取部件信息
     * @param pointAlarmHistoryId
     * @return
     */
    @Select("select p.* from point_history as ph left join point as p on ph.point_id = p.id where ph.id = #{pointHistoryId}")
    @ResultMap("com.ydrobot.powerplant.dao.PointMapper.BaseResultMap")
    Point getPointByPointHistoryId(@Param("pointHistoryId") Integer pointHistoryId);
    
    /**
     * 通过任务记录id获取部件巡检结果
     * @param taskHistoryId
     * @return
     */
    List<PointPatrolResult> getPointPatrolResult(@Param("taskHistoryId") Integer taskHistoryId,@Param("objectId") Integer objectId);
    
    /**
     * 获取最后一个点位的巡检时间
     * @param taskHistoryId
     * @return
     */
    @Select("select create_time from point_history as ph "
    		+ "left join point as p on ph.point_id = p.id "
    		+ "where ph.task_history_id = #{taskHistoryId} and p.object_id = #{objectId} "
    		+ "order by ph.create_time desc limit 1")
    Date getPatrolTimeByTaskHistory(@Param("taskHistoryId") Integer taskHistoryId,@Param("objectId") Integer objectId);
    
    /**
     * 通过对象id和任务记录id获取报警的点位记录列表
     * @param taskHistoryId
     * @param objectId
     * @return
     */
    @Select("select ph.* from point_history as ph "
    		+ "left join point_alarm_history as pah on ph.id=pah.point_history_id "
    		+ "left join point as p on ph.point_id = p.id "
    		+ "where pah.task_history_id = #{taskHistoryId} and p.object_id = #{objectId} "
    		+ "group by ph.id")
    @ResultMap("com.ydrobot.powerplant.dao.PointHistoryMapper.BaseResultMap")
    List<PointHistory> getPointHistoryByObjectIdAndTaskHistoryId(@Param("taskHistoryId") Integer taskHistoryId, @Param("objectId") Integer objectId);
    
    /**
     * 获取点位评价原因
     * @param pointHistoryId
     * @return
     */
    List<PointHistoryVerificationReportResult> getPointAppraiseReason(@Param("pointHistoryId") Integer pointHistoryId);
    
    /**
     * 获取已巡检点数
     * @param taskHistoryId
     * @return
     */
    @Select("select count(DISTINCT point_id) as num from point_history where task_history_id = #{taskHistoryId}")
    Integer getPassPointNum(@Param("taskHistoryId") Integer taskHistoryId);
}