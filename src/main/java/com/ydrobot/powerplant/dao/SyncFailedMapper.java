package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SyncFailed;

public interface SyncFailedMapper extends Mapper<SyncFailed> {
}