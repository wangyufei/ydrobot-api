package com.ydrobot.powerplant.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PathPlanning;

public interface PathPlanningMapper extends Mapper<PathPlanning> {

    /**
     * 通过任务id查找路径
     * @param taskId
     * @param flag
     * @return
     */
    @Select("select * from path_planning where task_id = #{taskId} and flag = #{flag} order by id desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PathPlanningMapper.BaseResultMap")
    PathPlanning getPathByTaskId(@Param("taskId") Integer taskId, @Param("flag") Byte flag);

    /**
     * 通过任务执行记录id查找路径
     * @param taskHistoryId
     * @param flag
     * @return
     */
    @Select("select * from path_planning where task_history_id = #{taskHistoryId} order by id desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PathPlanningMapper.BaseResultMap")
    PathPlanning getPathByTaskHistoryId(@Param("taskHistoryId") Integer taskHistoryId);

    /**
     * 获取机器人一键返航
     * @param robotId
     * @return
     */
    @Select("select * from path_planning where robot_id = #{robotId} and flag = 2 order by id desc limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PathPlanningMapper.BaseResultMap")
    PathPlanning getBackPathByRobotId(@Param("robotId") Integer robotId);
    
    /**
     * 获取机器人当前路径
     * @param robotId
     * @return
     */
    @Select("select * from path_planning where task_history_id = #{taskHistoryId} limit 1")
    @ResultMap("com.ydrobot.powerplant.dao.PathPlanningMapper.BaseResultMap")
    PathPlanning getRobotCurrentPath(@Param("taskHistoryId") Integer taskHistoryId);
}