package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.SaveType;

public interface SaveTypeMapper extends Mapper<SaveType> {
}