package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointAlarmSetting;
import com.ydrobot.powerplant.model.condition.PointAlarmSettingQueryParam;
import com.ydrobot.powerplant.model.dto.PointAlarmSettingResult;

public interface PointAlarmSettingMapper extends Mapper<PointAlarmSetting> {

	/**
	 * 获取点位报警设置列表
	 * @param queryParam
	 * @return
	 */
	List<PointAlarmSettingResult> getList(@Param("queryParam") PointAlarmSettingQueryParam queryParam);
	
    /**
     * 获取某点的报警配置
     * @param pointId
     * @param alarmTypeId
     * @return
     */
    @Select("select * from point_alarm_setting where point_id = #{pointId}  and alarm_type_id = #{alarmTypeId}")
    @ResultMap("com.ydrobot.powerplant.dao.PointAlarmSettingMapper.BaseResultMap")
    List<PointAlarmSetting> getTriphaseByPointId(@Param("pointId") Integer pointId,
            @Param("alarmTypeId") Integer alarmTypeId);

    /**
     * 删除某点的报警配置
     * @param pointId
     * @param alarmTypeId
     */
    @Select("delete from point_alarm_setting where point_id = #{pointId}  and alarm_type_id = #{alarmTypeId}")
    @ResultMap("com.ydrobot.powerplant.dao.PointAlarmSettingMapper.BaseResultMap")
    void deleteTriphaseByPointId(@Param("pointId") Integer pointId, @Param("alarmTypeId") Integer alarmTypeId);
}