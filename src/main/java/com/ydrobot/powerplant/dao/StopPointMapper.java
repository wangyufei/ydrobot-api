package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.StopPoint;

public interface StopPointMapper extends Mapper<StopPoint> {
	
	/**
	 * 通过任务id获取停靠点
	 * @param taskId
	 * @return
	 */
	@Select("select sp.* from stop_point as sp "
			+ "left join watch_point as wp on wp.stop_point_id = sp.id "
			+ "left join point_watch_point as pwp on wp.id = pwp.watch_point_id "
			+ "left join point as p on pwp.point_id=p.id "
			+ "left join task_point as tp on tp.point_id = p.id "
			+ "where tp.task_id = #{taskId}")
	@ResultMap("com.ydrobot.powerplant.dao.StopPointMapper.BaseResultMap")
	List<StopPoint> getTaskStopPoint(@Param("taskId") Integer taskId);
	
	/**
	 * 通过道路id获取停靠点
	 * @param routeIds
	 * @return
	 */
	@Select("select * from stop_point where route_id in (${routeIds})")
	@ResultMap("com.ydrobot.powerplant.dao.StopPointMapper.BaseResultMap")
	List<StopPoint> getStopPointByRouteId(@Param("routeIds") String routeIds);
}