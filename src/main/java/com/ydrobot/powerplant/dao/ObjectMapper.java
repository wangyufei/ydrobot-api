package com.ydrobot.powerplant.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.condition.PointTreeQueryParam;
import com.ydrobot.powerplant.model.dto.ObjectResult;
import com.ydrobot.powerplant.model.dto.ObjectTreeResult;

public interface ObjectMapper extends Mapper<TObject> {
	
	/**
	 * 获取对象树
	 * @return
	 */
	List<ObjectTreeResult> getObjectTree(@Param("queryParam") PointTreeQueryParam queryParam);
	
	/**
	 * 获取对象详情
	 * @param objectId
	 * @return
	 */
	ObjectResult getDetail(@Param("objectId") Integer objectId);
	
	/**
	 * 通过部件id按顺序获取对象列表
	 * @param pointIds
	 * @return
	 */
	@Select("select o.* from point as p "
			+ "left join object as o on p.object_id = o.id "
			+ "where p.id in (${pointIds}) "
			+ "order by field(p.id,${pointIds})")
	@ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
	List<TObject> getObjectByPointIds(@Param("pointIds") String pointIds);
	
	/**
	 * 获取异常评价对象列表
	 * @return
	 */
	@Select("select o.* from object as o "
			+ "left join point as p on o.id=p.object_id "
			+ "left join point_appraise as pa on p.id = pa.point_id "
			+ "where pa.appraise_status>0 group by o.id")
	@ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
	List<TObject> getAbnormalAppraiseObject();
	
	@Select("select o.* from object as o "
			+ "left join point as p on o.id = p.object_id "
			+ "left join point_alarm_history as pah on pah.point_id = p.id "
			+ "where task_history_id = (select id from task_history order by create_time desc limit 1) "
			+ "group by o.id")
	@ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
	List<TObject> getAlarmObject();
	
	/**
	 * 获取最后一次巡检对象
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select o.* from object as o "
			+ "left join point as p on o.id = p.object_id "
			+ "left join point_history as ph on ph.point_id = p.id "
			+ "where ph.task_history_id=#{taskHistoryId} "
			+ "order by create_time desc limit 1")
	@ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
	TObject getLastPatrolObject(@Param("taskHistoryId") Integer taskHistoryId);
	
	/**
	 * 获取已经巡检过的对象列表
	 * @param taskHistoryId
	 * @return
	 */
	@Select("select o.* from object as o "
			+ "left join point as p on o.id = p.object_id "
			+ "left join point_history as ph on ph.point_id = p.id "
			+ "where ph.task_history_id = #{taskHistoryId} "
			+ "group by o.id")
	@ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
	List<TObject> getAlreadyPatrolObjects(@Param("taskHistoryId") Integer taskHistoryId);
	
	/**
	 * 通过点位id获取对象信息
	 * @param pointId
	 * @return
	 */
	@Select("select o.* from object as o "
			+ "left join point as p on p.object_Id = o.id "
			+ "where p.id = #{pointId}")
	@ResultMap("com.ydrobot.powerplant.dao.ObjectMapper.BaseResultMap")
	TObject geTObjectByPointId(@Param("pointId") Integer pointId);
	
}