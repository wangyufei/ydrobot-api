package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.PointVisibleLight;

public interface PointVisibleLightMapper extends Mapper<PointVisibleLight> {
}