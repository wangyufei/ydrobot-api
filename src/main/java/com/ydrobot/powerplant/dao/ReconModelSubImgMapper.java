package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.ReconModelSubImg;

public interface ReconModelSubImgMapper extends Mapper<ReconModelSubImg> {
}