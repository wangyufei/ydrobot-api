package com.ydrobot.powerplant.dao;

import com.ydrobot.powerplant.core.mapper.Mapper;
import com.ydrobot.powerplant.model.Organization;

public interface OrganizationMapper extends Mapper<Organization> {
}