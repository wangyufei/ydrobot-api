package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "route_pos")
public class RoutePos {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * x坐标
     */
    private Float x;

    /**
     * y坐标
     */
    private Float y;

    /**
     * z坐标
     */
    private Float z;

    /**
     * 俯仰角
     */
    @Column(name = "angle_pitch")
    private Integer anglePitch;

    /**
     * 水平角
     */
    @Column(name = "angle_plane")
    private Integer anglePlane;

    /**
     * 变倍
     */
    private Integer zoom;

    /**
     * 是否运行掉头 0:否 1:是
     */
    @Column(name = "is_allow_turn")
    private Byte isAllowTurn;
    
    /**
     * 道路点类型 0:原始点 1:新增点
     */
    @Column(name = "type")
    private Byte type;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取x坐标
     *
     * @return x - x坐标
     */
    public Float getX() {
        return x;
    }

    /**
     * 设置x坐标
     *
     * @param x x坐标
     */
    public void setX(Float x) {
        this.x = x;
    }

    /**
     * 获取y坐标
     *
     * @return y - y坐标
     */
    public Float getY() {
        return y;
    }

    /**
     * 设置y坐标
     *
     * @param y y坐标
     */
    public void setY(Float y) {
        this.y = y;
    }

    /**
     * 获取z坐标
     *
     * @return z - z坐标
     */
    public Float getZ() {
        return z;
    }

    /**
     * 设置z坐标
     *
     * @param z z坐标
     */
    public void setZ(Float z) {
        this.z = z;
    }

    /**
     * 获取俯仰角
     *
     * @return angle_pitch - 俯仰角
     */
    public Integer getAnglePitch() {
        return anglePitch;
    }

    /**
     * 设置俯仰角
     *
     * @param anglePitch 俯仰角
     */
    public void setAnglePitch(Integer anglePitch) {
        this.anglePitch = anglePitch;
    }

    /**
     * 获取水平角
     *
     * @return angle_plane - 水平角
     */
    public Integer getAnglePlane() {
        return anglePlane;
    }

    /**
     * 设置水平角
     *
     * @param anglePlane 水平角
     */
    public void setAnglePlane(Integer anglePlane) {
        this.anglePlane = anglePlane;
    }

    /**
     * 获取变倍
     *
     * @return zoom - 变倍
     */
    public Integer getZoom() {
        return zoom;
    }

    /**
     * 设置变倍
     *
     * @param zoom 变倍
     */
    public void setZoom(Integer zoom) {
        this.zoom = zoom;
    }

    /**
     * 获取是否运行掉头 0:否 1:是
     *
     * @return is_allow_turn - 是否运行掉头 0:否 1:是
     */
    public Byte getIsAllowTurn() {
        return isAllowTurn;
    }

    /**
     * 设置是否运行掉头 0:否 1:是
     *
     * @param isAllowTurn 是否运行掉头 0:否 1:是
     */
    public void setIsAllowTurn(Byte isAllowTurn) {
        this.isAllowTurn = isAllowTurn;
    }

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}
}