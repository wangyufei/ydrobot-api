package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointRelationSettingQueryParam {

    @ApiModelProperty(name = "pointId", value = "点位id")
    private Integer pointId;

    @ApiModelProperty(name = "type", value = "类型 0:三相温差 1:三相对比")
    private Byte type;

    public Integer getPointId() {
        return pointId;
    }

    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

}
