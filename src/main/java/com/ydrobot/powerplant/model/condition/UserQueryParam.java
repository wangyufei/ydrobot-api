package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class UserQueryParam {

    @ApiModelProperty(name = "organizationId", value = "机构id")
    private Integer organizationId;

    @ApiModelProperty(name = "sn", value = "工号")
    private String sn;

    @ApiModelProperty(name = "name", value = "姓名")
    private String name;

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
