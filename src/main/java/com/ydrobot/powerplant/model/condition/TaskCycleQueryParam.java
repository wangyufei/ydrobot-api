package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class TaskCycleQueryParam {

    @ApiModelProperty(name = "taskId", value = "任务id")
    private Integer taskId;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

}
