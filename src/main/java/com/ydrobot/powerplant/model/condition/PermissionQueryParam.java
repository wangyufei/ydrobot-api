package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PermissionQueryParam {

    @ApiModelProperty(name = "roleId", value = "角色id")
    private Integer roleId;

    @ApiModelProperty(name = "displayName", value = "显示名称")
    private String displayName;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
