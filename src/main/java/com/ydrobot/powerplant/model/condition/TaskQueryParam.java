package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class TaskQueryParam {

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "inspectTypeId", value = "巡检类型id")
    private Integer inspectTypeId;

    @ApiModelProperty(name = "custom", value = "是否自定义任务 1:自定义")
    private Integer custom;

    @ApiModelProperty(name = "taskName", value = "任务名称")
    private String taskName;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public Integer getInspectTypeId() {
        return inspectTypeId;
    }

    public void setInspectTypeId(Integer inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

    public Integer getCustom() {
        return custom;
    }

    public void setCustom(Integer custom) {
        this.custom = custom;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

}
