package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class TaskMonthShowQueryParam {

    @ApiModelProperty(name = "robotId", value = "机器人id", required = true)
    private Integer robotId;

    @ApiModelProperty(name = "executeTime", value = "执行时间")
    private String executeTime;

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public String getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

}
