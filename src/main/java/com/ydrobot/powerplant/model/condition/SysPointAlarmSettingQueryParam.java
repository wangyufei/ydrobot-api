package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class SysPointAlarmSettingQueryParam {
	
	@ApiModelProperty(name = "sysPointId", value = "系统点位id")
	public Integer sysPointId;

	public Integer getSysPointId() {
		return sysPointId;
	}

	public void setSysPointId(Integer sysPointId) {
		this.sysPointId = sysPointId;
	}

}
