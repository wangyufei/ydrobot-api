package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointAlarmSettingQueryParam {

    @ApiModelProperty(name = "pointId", value = "点位id")
    private Integer pointId;

    @ApiModelProperty(name = "alarmTypeId", value = "告警类型id")
    private Integer alarmTypeId;

    public Integer getPointId() {
        return pointId;
    }

    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    public Integer getAlarmTypeId() {
        return alarmTypeId;
    }

    public void setAlarmTypeId(Integer alarmTypeId) {
        this.alarmTypeId = alarmTypeId;
    }

}
