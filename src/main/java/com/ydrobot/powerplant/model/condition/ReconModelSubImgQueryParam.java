package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class ReconModelSubImgQueryParam {

    @ApiModelProperty(name = "reconModelSubId", value = "识别模型子类id")
    private Integer reconModelSubId;

    public Integer getReconModelSubId() {
        return reconModelSubId;
    }

    public void setReconModelSubId(Integer reconModelSubId) {
        this.reconModelSubId = reconModelSubId;
    }
}
