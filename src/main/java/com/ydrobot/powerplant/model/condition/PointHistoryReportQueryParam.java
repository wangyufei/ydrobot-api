package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointHistoryReportQueryParam {

    @ApiModelProperty(name = "page", value = "当前页")
    private Integer page = 1;

    @ApiModelProperty(name = "size", value = "每页显示数量")
    private Integer size = 10;

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "pointIds", value = "点位集合。多个逗号分隔")
    private String pointIds;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public String getPointIds() {
        return pointIds;
    }

    public void setPointIds(String pointIds) {
        this.pointIds = pointIds;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
