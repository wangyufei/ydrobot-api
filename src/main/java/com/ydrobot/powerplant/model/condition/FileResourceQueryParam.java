package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class FileResourceQueryParam {

	@ApiModelProperty(name = "type", value = "资源类型 0:可见光 1:红外")
    private Integer type;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
