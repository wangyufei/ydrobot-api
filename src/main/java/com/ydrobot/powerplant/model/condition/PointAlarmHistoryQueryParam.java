package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointAlarmHistoryQueryParam {

    @ApiModelProperty(name = "page", value = "当前页")
    private Integer page = 1;

    @ApiModelProperty(name = "size", value = "每页显示数量")
    private Integer size = 10;

    @ApiModelProperty(name = "taskHistoryId", value = "任务执行记录id")
    private Integer taskHistoryId;

    @ApiModelProperty(name = "robotId", value = "机器人id")
    private Integer robotId;

    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;

    @ApiModelProperty(name = "checkStatus", value = "审核状态 0:未审核 1:已审核")
    private Integer checkStatus;

    @ApiModelProperty(name = "resultStatus", value = "结果状态 0:正常 1:异常")
    private Integer resultStatus;

    @ApiModelProperty(name = "alarmLevel", value = "告警等级,多个用逗号分隔 0:正常 1:预警 2:一般告警 3:严重告警 4:危急告警")
    private String alarmLevel;

    @ApiModelProperty(name = "pointIds", value = "点位id集合，多个逗号分隔")
    private String pointIds;
    
    @ApiModelProperty(name = "pointHistoryId", value = "点位记录id")
    private Integer pointHistoryId;
    
    @ApiModelProperty(name = "pointAlarmHistoryId", value = "点位告警记录id")
    private Integer pointAlarmHistoryId;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    public Integer getRobotId() {
        return robotId;
    }

    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Integer resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(String alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public String getPointIds() {
        return pointIds;
    }

    public void setPointIds(String pointIds) {
        this.pointIds = pointIds;
    }

	public Integer getPointHistoryId() {
		return pointHistoryId;
	}

	public void setPointHistoryId(Integer pointHistoryId) {
		this.pointHistoryId = pointHistoryId;
	}

	public Integer getPointAlarmHistoryId() {
		return pointAlarmHistoryId;
	}

	public void setPointAlarmHistoryId(Integer pointAlarmHistoryId) {
		this.pointAlarmHistoryId = pointAlarmHistoryId;
	}
}
