package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointTreeQueryParam {
    
    @ApiModelProperty(name = "objectIds", value = "对象id")
    private String objectIds;

	public String getObjectIds() {
		return objectIds;
	}

	public void setObjectIds(String objectIds) {
		this.objectIds = objectIds;
	}
    
}
