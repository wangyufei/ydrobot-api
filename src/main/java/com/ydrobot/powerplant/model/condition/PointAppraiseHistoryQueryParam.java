package com.ydrobot.powerplant.model.condition;

import io.swagger.annotations.ApiModelProperty;

public class PointAppraiseHistoryQueryParam {
	
	@ApiModelProperty(name = "startTime", value = "开始时间")
	private String startTime;
	
	@ApiModelProperty(name = "endTime", value = "结束时间")
	private String endTime;
	
	@ApiModelProperty(name = "objectId", value = "对象id")
	private Integer objectId;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
}
