package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "pms_device")
public class PmsDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * pms设备id
     */
    @Column(name = "device_id")
    private String deviceId;

    /**
     * 设备编号
     */
    @Column(name = "device_sn")
    private String deviceSn;

    /**
     * 变电站名称
     */
    @Column(name = "station_name")
    private String stationName;

    /**
     * 间隔单元
     */
    @Column(name = "Interval_unit")
    private String intervalUnit;

    /**
     * 设备名称
     */
    @Column(name = "device_name")
    private String deviceName;

    /**
     * 电压等级
     */
    @Column(name = "voltage_grade")
    private String voltageGrade;

    /**
     * 设备类型
     */
    @Column(name = "device_type")
    private String deviceType;

    /**
     * 运行编号
     */
    @Column(name = "run_number")
    private String runNumber;

    /**
     * 设备状态
     */
    @Column(name = "device_status")
    private String deviceStatus;

    /**
     * 相别
     */
    private String phase;

    /**
     * 设备主人
     */
    @Column(name = "device_owner")
    private String deviceOwner;

    /**
     * 设备型号
     */
    @Column(name = "device_model")
    private String deviceModel;

    /**
     * 生产厂家
     */
    private String manufacturer;

    /**
     * 投运日期
     */
    @Column(name = "commissioning_time")
    private Date commissioningTime;

    /**
     * 所属电站电压等级
     */
    @Column(name = "station_voltage_level")
    private String stationVoltageLevel;

    /**
     * 维护班组
     */
    @Column(name = "maintenance_team")
    private String maintenanceTeam;

    /**
     * 所属地市
     */
    @Column(name = "local_city")
    private String localCity;

    /**
     * 登记时间
     */
    @Column(name = "registration_time")
    private Date registrationTime;

    /**
     * 制造国家
     */
    @Column(name = "manufacturing_country")
    private String manufacturingCountry;

    /**
     * 资产性质
     */
    @Column(name = "nature_assets")
    private String natureAssets;

    /**
     * 型号
     */
    private String model;

    /**
     * 运维单位
     */
    @Column(name = "maintenance_unit")
    private String maintenanceUnit;

    /**
     * 出厂编号
     */
    @Column(name = "factory_number")
    private String factoryNumber;

    /**
     * 出厂日期
     */
    @Column(name = "production_date")
    private String productionDate;

    /**
     * 资产单位
     */
    @Column(name = "asset_unit")
    private String assetUnit;

    /**
     * 工程编号
     */
    @Column(name = "project_number")
    private String projectNumber;

    /**
     * 工程名称
     */
    @Column(name = "project_name")
    private String projectName;

    /**
     * 站线名称
     */
    @Column(name = "station_line_name")
    private String stationLineName;

    /**
     * 专业分类
     */
    @Column(name = "major_classification")
    private String majorClassification;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取pms设备id
     *
     * @return device_id - pms设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 设置pms设备id
     *
     * @param deviceId pms设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 获取设备编号
     *
     * @return device_sn - 设备编号
     */
    public String getDeviceSn() {
        return deviceSn;
    }

    /**
     * 设置设备编号
     *
     * @param deviceSn 设备编号
     */
    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    /**
     * 获取变电站名称
     *
     * @return station_name - 变电站名称
     */
    public String getStationName() {
        return stationName;
    }

    /**
     * 设置变电站名称
     *
     * @param stationName 变电站名称
     */
    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    /**
     * 获取间隔单元
     *
     * @return Interval_unit - 间隔单元
     */
    public String getIntervalUnit() {
        return intervalUnit;
    }

    /**
     * 设置间隔单元
     *
     * @param intervalUnit 间隔单元
     */
    public void setIntervalUnit(String intervalUnit) {
        this.intervalUnit = intervalUnit;
    }

    /**
     * 获取设备名称
     *
     * @return device_name - 设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 设置设备名称
     *
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * 获取电压等级
     *
     * @return voltage_grade - 电压等级
     */
    public String getVoltageGrade() {
        return voltageGrade;
    }

    /**
     * 设置电压等级
     *
     * @param voltageGrade 电压等级
     */
    public void setVoltageGrade(String voltageGrade) {
        this.voltageGrade = voltageGrade;
    }

    /**
     * 获取设备类型
     *
     * @return device_type - 设备类型
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * 设置设备类型
     *
     * @param deviceType 设备类型
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * 获取运行编号
     *
     * @return run_number - 运行编号
     */
    public String getRunNumber() {
        return runNumber;
    }

    /**
     * 设置运行编号
     *
     * @param runNumber 运行编号
     */
    public void setRunNumber(String runNumber) {
        this.runNumber = runNumber;
    }

    /**
     * 获取设备状态
     *
     * @return device_status - 设备状态
     */
    public String getDeviceStatus() {
        return deviceStatus;
    }

    /**
     * 设置设备状态
     *
     * @param deviceStatus 设备状态
     */
    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    /**
     * 获取相别
     *
     * @return phase - 相别
     */
    public String getPhase() {
        return phase;
    }

    /**
     * 设置相别
     *
     * @param phase 相别
     */
    public void setPhase(String phase) {
        this.phase = phase;
    }

    /**
     * 获取设备主人
     *
     * @return device_owner - 设备主人
     */
    public String getDeviceOwner() {
        return deviceOwner;
    }

    /**
     * 设置设备主人
     *
     * @param deviceOwner 设备主人
     */
    public void setDeviceOwner(String deviceOwner) {
        this.deviceOwner = deviceOwner;
    }

    /**
     * 获取设备型号
     *
     * @return device_model - 设备型号
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * 设置设备型号
     *
     * @param deviceModel 设备型号
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /**
     * 获取生产厂家
     *
     * @return manufacturer - 生产厂家
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * 设置生产厂家
     *
     * @param manufacturer 生产厂家
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * 获取投运日期
     *
     * @return commissioning_time - 投运日期
     */
    public Date getCommissioningTime() {
        return commissioningTime;
    }

    /**
     * 设置投运日期
     *
     * @param commissioningTime 投运日期
     */
    public void setCommissioningTime(Date commissioningTime) {
        this.commissioningTime = commissioningTime;
    }

    /**
     * 获取所属电站电压等级
     *
     * @return station_voltage_level - 所属电站电压等级
     */
    public String getStationVoltageLevel() {
        return stationVoltageLevel;
    }

    /**
     * 设置所属电站电压等级
     *
     * @param stationVoltageLevel 所属电站电压等级
     */
    public void setStationVoltageLevel(String stationVoltageLevel) {
        this.stationVoltageLevel = stationVoltageLevel;
    }

    /**
     * 获取维护班组
     *
     * @return maintenance_team - 维护班组
     */
    public String getMaintenanceTeam() {
        return maintenanceTeam;
    }

    /**
     * 设置维护班组
     *
     * @param maintenanceTeam 维护班组
     */
    public void setMaintenanceTeam(String maintenanceTeam) {
        this.maintenanceTeam = maintenanceTeam;
    }

    /**
     * 获取所属地市
     *
     * @return local_city - 所属地市
     */
    public String getLocalCity() {
        return localCity;
    }

    /**
     * 设置所属地市
     *
     * @param localCity 所属地市
     */
    public void setLocalCity(String localCity) {
        this.localCity = localCity;
    }

    /**
     * 获取登记时间
     *
     * @return registration_time - 登记时间
     */
    public Date getRegistrationTime() {
        return registrationTime;
    }

    /**
     * 设置登记时间
     *
     * @param registrationTime 登记时间
     */
    public void setRegistrationTime(Date registrationTime) {
        this.registrationTime = registrationTime;
    }

    /**
     * 获取制造国家
     *
     * @return manufacturing_country - 制造国家
     */
    public String getManufacturingCountry() {
        return manufacturingCountry;
    }

    /**
     * 设置制造国家
     *
     * @param manufacturingCountry 制造国家
     */
    public void setManufacturingCountry(String manufacturingCountry) {
        this.manufacturingCountry = manufacturingCountry;
    }

    /**
     * 获取资产性质
     *
     * @return nature_assets - 资产性质
     */
    public String getNatureAssets() {
        return natureAssets;
    }

    /**
     * 设置资产性质
     *
     * @param natureAssets 资产性质
     */
    public void setNatureAssets(String natureAssets) {
        this.natureAssets = natureAssets;
    }

    /**
     * 获取型号
     *
     * @return model - 型号
     */
    public String getModel() {
        return model;
    }

    /**
     * 设置型号
     *
     * @param model 型号
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * 获取运维单位
     *
     * @return maintenance_unit - 运维单位
     */
    public String getMaintenanceUnit() {
        return maintenanceUnit;
    }

    /**
     * 设置运维单位
     *
     * @param maintenanceUnit 运维单位
     */
    public void setMaintenanceUnit(String maintenanceUnit) {
        this.maintenanceUnit = maintenanceUnit;
    }

    /**
     * 获取出厂编号
     *
     * @return factory_number - 出厂编号
     */
    public String getFactoryNumber() {
        return factoryNumber;
    }

    /**
     * 设置出厂编号
     *
     * @param factoryNumber 出厂编号
     */
    public void setFactoryNumber(String factoryNumber) {
        this.factoryNumber = factoryNumber;
    }

    /**
     * 获取出厂日期
     *
     * @return production_date - 出厂日期
     */
    public String getProductionDate() {
        return productionDate;
    }

    /**
     * 设置出厂日期
     *
     * @param productionDate 出厂日期
     */
    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    /**
     * 获取资产单位
     *
     * @return asset_unit - 资产单位
     */
    public String getAssetUnit() {
        return assetUnit;
    }

    /**
     * 设置资产单位
     *
     * @param assetUnit 资产单位
     */
    public void setAssetUnit(String assetUnit) {
        this.assetUnit = assetUnit;
    }

    /**
     * 获取工程编号
     *
     * @return project_number - 工程编号
     */
    public String getProjectNumber() {
        return projectNumber;
    }

    /**
     * 设置工程编号
     *
     * @param projectNumber 工程编号
     */
    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    /**
     * 获取工程名称
     *
     * @return project_name - 工程名称
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * 设置工程名称
     *
     * @param projectName 工程名称
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * 获取站线名称
     *
     * @return station_line_name - 站线名称
     */
    public String getStationLineName() {
        return stationLineName;
    }

    /**
     * 设置站线名称
     *
     * @param stationLineName 站线名称
     */
    public void setStationLineName(String stationLineName) {
        this.stationLineName = stationLineName;
    }

    /**
     * 获取专业分类
     *
     * @return major_classification - 专业分类
     */
    public String getMajorClassification() {
        return majorClassification;
    }

    /**
     * 设置专业分类
     *
     * @param majorClassification 专业分类
     */
    public void setMajorClassification(String majorClassification) {
        this.majorClassification = majorClassification;
    }
}