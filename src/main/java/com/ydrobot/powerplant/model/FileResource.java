package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.utils.ProjectUrlConvert;

@Table(name = "file_resource")
public class FileResource {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 资源类型 0:可见光图片
     */
    private Byte type;

    /**
     * 资源地址
     */
    private String url;

    /**
     * 添加时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取资源类型 0:可见光图片
     *
     * @return type - 资源类型 0:可见光图片
     */
    public Byte getType() {
        return type;
    }

    /**
     * 设置资源类型 0:可见光图片
     *
     * @param type 资源类型 0:可见光图片
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 获取资源地址
     *
     * @return url - 资源地址
     */
    public String getUrl() {
        return ProjectUrlConvert.covert2HttpUrl(url,ProjectConsts.PREFIX_FILE);
    }

    /**
     * 设置资源地址
     *
     * @param url 资源地址
     */
    public void setUrl(String url) {
        this.url = ProjectUrlConvert.covert2LocalUrl(url, ProjectConsts.PREFIX_FILE);
    }

    /**
     * 获取添加时间
     *
     * @return create_time - 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置添加时间
     *
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}