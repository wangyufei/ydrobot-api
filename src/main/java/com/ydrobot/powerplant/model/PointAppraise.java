package com.ydrobot.powerplant.model;

import java.util.Date;

import javax.persistence.*;

@Table(name = "point_appraise")
public class PointAppraise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 部件id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     */
    @Column(name = "appraise_status")
    private Integer appraiseStatus;

    /**
     * 评价状态对应记录id
     */
    @Column(name = "appraise_history_id")
    private Integer appraiseHistoryId;
    
    /**
     * 评价时间
     */
    @Column(name = "appraise_time")
    private Date appraiseTime;
    
    /**
     * 评价人
     */
    @Column(name = "user_name")
    private String userName;
    
    /**
     * 处理状态
     */
    private Integer status;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取部件id
     *
     * @return point_id - 部件id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置部件id
     *
     * @param pointId 部件id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     *
     * @return appraise_status - 评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     */
    public Integer getAppraiseStatus() {
        return appraiseStatus;
    }

    /**
     * 设置评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     *
     * @param appraiseStatus 评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     */
    public void setAppraiseStatus(Integer appraiseStatus) {
        this.appraiseStatus = appraiseStatus;
    }

    /**
     * 获取评价状态对应记录id
     *
     * @return appraise_history_id - 评价状态对应记录id
     */
    public Integer getAppraiseHistoryId() {
        return appraiseHistoryId;
    }

    /**
     * 设置评价状态对应记录id
     *
     * @param appraiseHistoryId 评价状态对应记录id
     */
    public void setAppraiseHistoryId(Integer appraiseHistoryId) {
        this.appraiseHistoryId = appraiseHistoryId;
    }

	public Date getAppraiseTime() {
		return appraiseTime;
	}

	public void setAppraiseTime(Date appraiseTime) {
		this.appraiseTime = appraiseTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}