package com.ydrobot.powerplant.model.datasync;

import com.ydrobot.powerplant.model.TObject;

public class ObjectSync extends TObject{
	
	/**
	 * 站点id
	 */
	private Integer stationId;

	/**
	 * 扩展id
	 */
	private Integer extendId;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Integer getExtendId() {
		return extendId;
	}

	public void setExtendId(Integer extendId) {
		this.extendId = extendId;
	}
}
