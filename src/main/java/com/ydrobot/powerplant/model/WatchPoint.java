package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "watch_point")
public class WatchPoint {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 停靠位id
     */
    @Column(name = "stop_point_id")
    private Integer stopPointId;

    /**
     * 默认值
     */
    @Column(name = "default_value")
    private Float defaultValue;

    /**
     * x坐标
     */
    private Float x;

    /**
     * y坐标
     */
    private Float y;

    /**
     * z坐标
     */
    private Float z;

    /**
     * 针对相机对准是否计算对准还是直接调用预制值对准的开关量 0:自动对准  1:预制值 2:预制位
     */
    @Column(name = "calc_on_off")
    private Byte calcOnOff;

    /**
     * 正向水平角预置量
     */
    @Column(name = "f_h_angle")
    private Float fHAngle;

    /**
     * 正向俯仰角预置量
     */
    @Column(name = "f_p_angle")
    private Float fPAngle;

    /**
     * 正向倍率预置量
     */
    @Column(name = "f_mult")
    private Float fMult;

    /**
     * 反向水平角预置量
     */
    @Column(name = "b_h_angle")
    private Float bHAngle;

    /**
     * 反向俯仰角预置量
     */
    @Column(name = "b_p_angle")
    private Float bPAngle;

    /**
     * 正向预置位号
     */
    @Column(name = "f_preset_no")
    private Float fPresetNo;

    /**
     * 反向倍率预置量
     */
    @Column(name = "b_mult")
    private Float bMult;

    /**
     * 反向预置位号
     */
    @Column(name = "b_preset_no")
    private Float bPresetNo;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取停靠位id
     *
     * @return stop_point_id - 停靠位id
     */
    public Integer getStopPointId() {
        return stopPointId;
    }

    /**
     * 设置停靠位id
     *
     * @param stopPointId 停靠位id
     */
    public void setStopPointId(Integer stopPointId) {
        this.stopPointId = stopPointId;
    }

    /**
     * 获取默认值
     *
     * @return default_value - 默认值
     */
    public Float getDefaultValue() {
        return defaultValue;
    }

    /**
     * 设置默认值
     *
     * @param defaultValue 默认值
     */
    public void setDefaultValue(Float defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * 获取x坐标
     *
     * @return x - x坐标
     */
    public Float getX() {
        return x;
    }

    /**
     * 设置x坐标
     *
     * @param x x坐标
     */
    public void setX(Float x) {
        this.x = x;
    }

    /**
     * 获取y坐标
     *
     * @return y - y坐标
     */
    public Float getY() {
        return y;
    }

    /**
     * 设置y坐标
     *
     * @param y y坐标
     */
    public void setY(Float y) {
        this.y = y;
    }

    /**
     * 获取z坐标
     *
     * @return z - z坐标
     */
    public Float getZ() {
        return z;
    }

    /**
     * 设置z坐标
     *
     * @param z z坐标
     */
    public void setZ(Float z) {
        this.z = z;
    }

    /**
     * 获取针对相机对准是否计算对准还是直接调用预制值对准的开关量 0:自动对准  1:预制值 2:预制位
     *
     * @return calc_on_off - 针对相机对准是否计算对准还是直接调用预制值对准的开关量 0:自动对准  1:预制值 2:预制位
     */
    public Byte getCalcOnOff() {
        return calcOnOff;
    }

    /**
     * 设置针对相机对准是否计算对准还是直接调用预制值对准的开关量 0:自动对准  1:预制值 2:预制位
     *
     * @param calcOnOff 针对相机对准是否计算对准还是直接调用预制值对准的开关量 0:自动对准  1:预制值 2:预制位
     */
    public void setCalcOnOff(Byte calcOnOff) {
        this.calcOnOff = calcOnOff;
    }

    /**
     * 获取正向水平角预置量
     *
     * @return f_h_angle - 正向水平角预置量
     */
    public Float getfHAngle() {
        return fHAngle;
    }

    /**
     * 设置正向水平角预置量
     *
     * @param fHAngle 正向水平角预置量
     */
    public void setfHAngle(Float fHAngle) {
        this.fHAngle = fHAngle;
    }

    /**
     * 获取正向俯仰角预置量
     *
     * @return f_p_angle - 正向俯仰角预置量
     */
    public Float getfPAngle() {
        return fPAngle;
    }

    /**
     * 设置正向俯仰角预置量
     *
     * @param fPAngle 正向俯仰角预置量
     */
    public void setfPAngle(Float fPAngle) {
        this.fPAngle = fPAngle;
    }

    /**
     * 获取正向倍率预置量
     *
     * @return f_mult - 正向倍率预置量
     */
    public Float getfMult() {
        return fMult;
    }

    /**
     * 设置正向倍率预置量
     *
     * @param fMult 正向倍率预置量
     */
    public void setfMult(Float fMult) {
        this.fMult = fMult;
    }

    /**
     * 获取反向水平角预置量
     *
     * @return b_h_angle - 反向水平角预置量
     */
    public Float getbHAngle() {
        return bHAngle;
    }

    /**
     * 设置反向水平角预置量
     *
     * @param bHAngle 反向水平角预置量
     */
    public void setbHAngle(Float bHAngle) {
        this.bHAngle = bHAngle;
    }

    /**
     * 获取反向俯仰角预置量
     *
     * @return b_p_angle - 反向俯仰角预置量
     */
    public Float getbPAngle() {
        return bPAngle;
    }

    /**
     * 设置反向俯仰角预置量
     *
     * @param bPAngle 反向俯仰角预置量
     */
    public void setbPAngle(Float bPAngle) {
        this.bPAngle = bPAngle;
    }

    /**
     * 获取正向预置位号
     *
     * @return f_preset_no - 正向预置位号
     */
    public Float getfPresetNo() {
        return fPresetNo;
    }

    /**
     * 设置正向预置位号
     *
     * @param fPresetNo 正向预置位号
     */
    public void setfPresetNo(Float fPresetNo) {
        this.fPresetNo = fPresetNo;
    }

    /**
     * 获取反向倍率预置量
     *
     * @return b_mult - 反向倍率预置量
     */
    public Float getbMult() {
        return bMult;
    }

    /**
     * 设置反向倍率预置量
     *
     * @param bMult 反向倍率预置量
     */
    public void setbMult(Float bMult) {
        this.bMult = bMult;
    }

    /**
     * 获取反向预置位号
     *
     * @return b_preset_no - 反向预置位号
     */
    public Float getbPresetNo() {
        return bPresetNo;
    }

    /**
     * 设置反向预置位号
     *
     * @param bPresetNo 反向预置位号
     */
    public void setbPresetNo(Float bPresetNo) {
        this.bPresetNo = bPresetNo;
    }
}