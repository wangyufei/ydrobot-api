package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "task_cycle")
public class TaskCycle {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 任务id
     */
    @Column(name = "task_id")
    private Integer taskId;

    /**
     * 任务优先级
     */
    @Column(name = "task_level")
    private Integer taskLevel;

    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 执行类型 0:周期执行 1:间隔执行
     */
    @Column(name = "execute_type")
    private Byte executeType;

    /**
     * 月，多个以逗号分隔
     */
    private String month;

    /**
     * 周，多个以逗号分隔
     */
    private String week;

    /**
     * 周期执行时间
     */
    @Column(name = "execute_time")
    private String executeTime;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 间隔时间
     */
    @Column(name = "interval_time")
    private Integer intervalTime;

    /**
     * 间隔单位  0:分钟 1:小时 2:天
     */
    @Column(name = "interval_unit")
    private Byte intervalUnit;

    /**
     * 任务周期名称
     */
    private String name;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取任务id
     *
     * @return task_id - 任务id
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 设置任务id
     *
     * @param taskId 任务id
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取任务优先级
     *
     * @return task_level - 任务优先级
     */
    public Integer getTaskLevel() {
        return taskLevel;
    }

    /**
     * 设置任务优先级
     *
     * @param taskLevel 任务优先级
     */
    public void setTaskLevel(Integer taskLevel) {
        this.taskLevel = taskLevel;
    }

    /**
     * @return robot_id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * @param robotId
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取执行类型 0:周期执行 1:间隔执行
     *
     * @return execute_type - 执行类型 0:周期执行 1:间隔执行
     */
    public Byte getExecuteType() {
        return executeType;
    }

    /**
     * 设置执行类型 0:周期执行 1:间隔执行
     *
     * @param executeType 执行类型 0:周期执行 1:间隔执行
     */
    public void setExecuteType(Byte executeType) {
        this.executeType = executeType;
    }

    /**
     * 获取月，多个以逗号分隔
     *
     * @return month - 月，多个以逗号分隔
     */
    public String getMonth() {
        return month;
    }

    /**
     * 设置月，多个以逗号分隔
     *
     * @param month 月，多个以逗号分隔
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * 获取周，多个以逗号分隔
     *
     * @return week - 周，多个以逗号分隔
     */
    public String getWeek() {
        return week;
    }

    /**
     * 设置周，多个以逗号分隔
     *
     * @param week 周，多个以逗号分隔
     */
    public void setWeek(String week) {
        this.week = week;
    }

    /**
     * 获取周期执行时间
     *
     * @return execute_time - 周期执行时间
     */
    public String getExecuteTime() {
        return executeTime;
    }

    /**
     * 设置周期执行时间
     *
     * @param executeTime 周期执行时间
     */
    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    /**
     * 获取开始时间
     *
     * @return start_time - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取间隔时间
     *
     * @return interval_time - 间隔时间
     */
    public Integer getIntervalTime() {
        return intervalTime;
    }

    /**
     * 设置间隔时间
     *
     * @param intervalTime 间隔时间
     */
    public void setIntervalTime(Integer intervalTime) {
        this.intervalTime = intervalTime;
    }

    /**
     * 获取间隔单位  0:分钟 1:小时 2:天
     *
     * @return interval_unit - 间隔单位  0:分钟 1:小时 2:天
     */
    public Byte getIntervalUnit() {
        return intervalUnit;
    }

    /**
     * 设置间隔单位  0:分钟 1:小时 2:天
     *
     * @param intervalUnit 间隔单位  0:分钟 1:小时 2:天
     */
    public void setIntervalUnit(Byte intervalUnit) {
        this.intervalUnit = intervalUnit;
    }

    /**
     * 获取任务周期名称
     *
     * @return name - 任务周期名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置任务周期名称
     *
     * @param name 任务周期名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}