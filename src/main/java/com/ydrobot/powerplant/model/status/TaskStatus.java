package com.ydrobot.powerplant.model.status;

public enum TaskStatus {
    /**
     * 已执行
     */
    ALREADY_EXECUTE(0),

    /**
     * 终止
     */
    END(1),

    /**
     * 暂停
     */
    PAUSE(2),

    /**
     * 正在执行
     */
    CURRENT_EXECUTE(3),

    /**
     * 未执行
     */
    UNEXECUTE(4),

    /**
     * 超期
     */
    OVERDUE(5),;

    private int value;

    public int getValue() {
        return value;
    }

    TaskStatus(int value) {
        this.value = value;
    }
}
