package com.ydrobot.powerplant.model.status;

public enum StatusEnum {
	ENABLED(1),
    DISABLED(2),
    ;

    private Integer code;

    StatusEnum(int code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
