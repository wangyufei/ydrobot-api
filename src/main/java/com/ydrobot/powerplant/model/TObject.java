package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "object")
public class TObject {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 对象名称
     */
    private String name;

    /**
     * 对象显示名称
     */
    @Column(name = "display_name")
    private String displayName;

    /**
     * x坐标
     */
    private Float x;

    /**
     * y坐标
     */
    private Float y;

    /**
     * z坐标
     */
    private Float z;

    /**
     * 上级id
     */
    @Column(name = "device_id")
    private Integer deviceId;

    /**
     * 设备类型id
     */
    @Column(name = "object_type_id")
    private Integer objectTypeId;
    
    /**
     * 异常运行时间
     */
    @Column(name = "abnormal_run_time")
    private Integer abnormalRunTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取对象名称
     *
     * @return name - 对象名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置对象名称
     *
     * @param name 对象名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取对象显示名称
     *
     * @return display_name - 对象显示名称
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 设置对象显示名称
     *
     * @param displayName 对象显示名称
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * 获取x坐标
     *
     * @return x - x坐标
     */
    public Float getX() {
        return x;
    }

    /**
     * 设置x坐标
     *
     * @param x x坐标
     */
    public void setX(Float x) {
        this.x = x;
    }

    /**
     * 获取y坐标
     *
     * @return y - y坐标
     */
    public Float getY() {
        return y;
    }

    /**
     * 设置y坐标
     *
     * @param y y坐标
     */
    public void setY(Float y) {
        this.y = y;
    }

    /**
     * 获取z坐标
     *
     * @return z - z坐标
     */
    public Float getZ() {
        return z;
    }

    /**
     * 设置z坐标
     *
     * @param z z坐标
     */
    public void setZ(Float z) {
        this.z = z;
    }

    /**
     * 获取上级id
     *
     * @return device_id - 上级id
     */
    public Integer getDeviceId() {
        return deviceId;
    }

    /**
     * 设置上级id
     *
     * @param deviceId 上级id
     */
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 获取设备类型id
     *
     * @return object_type_id - 设备类型id
     */
    public Integer getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * 设置设备类型id
     *
     * @param objectTypeId 设备类型id
     */
    public void setObjectTypeId(Integer objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

	public Integer getAbnormalRunTime() {
		return abnormalRunTime;
	}

	public void setAbnormalRunTime(Integer abnormalRunTime) {
		this.abnormalRunTime = abnormalRunTime;
	}
    
}