package com.ydrobot.powerplant.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ydrobot.powerplant.core.constant.ProjectConsts;
import com.ydrobot.powerplant.core.utils.ProjectUrlConvert;

@Table(name = "point_history")
public class PointHistory {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 任务执行记录id
     */
    @Column(name = "task_history_id")
    private Integer taskHistoryId;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 巡检点id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 可见光图片
     */
    @Column(name = "camera_pic")
    private String cameraPic;

    /**
     * 红外图片
     */
    @Column(name = "flir_pic")
    private String flirPic;

    /**
     * 音视频
     */
    private String sound;

    /**
     * 识别结果
     */
    private Float value;

    /**
     * 识别时间
     */
    @Column(name = "recon_time")
    private Date reconTime;

    /**
     * 观测位id
     */
    @Column(name = "watch_point_id")
    private Integer watchPointId;

    /**
     * 审核状态 0:未审核 1:已审核
     */
    @Column(name = "check_status")
    private Byte checkStatus;

    /**
     * 结果状态 0:识别正确 1:识别错误
     */
    @Column(name = "recon_status")
    private Byte reconStatus;

    /**
     * 人工判断结果
     */
    @Column(name = "modify_value")
    private Float modifyValue;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;
    
    /**
     * 审核人id
     */
    @Column(name = "operater_id")
    private Integer operaterId;
    
    /**
     * 最大温度位置
     */
    @Column(name = "max_temp_position")
    private String maxTempPosition;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取任务执行记录id
     *
     * @return task_history_id - 任务执行记录id
     */
    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    /**
     * 设置任务执行记录id
     *
     * @param taskHistoryId 任务执行记录id
     */
    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取巡检点id
     *
     * @return point_id - 巡检点id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置巡检点id
     *
     * @param pointId 巡检点id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取可见光图片
     *
     * @return camera_pic - 可见光图片
     */
    public String getCameraPic() {
    		return ProjectUrlConvert.covert2HttpUrl(cameraPic,ProjectConsts.PREFIX_RECON);
    }

    /**
     * 设置可见光图片
     *
     * @param cameraPic 可见光图片
     */
    public void setCameraPic(String cameraPic) {
        this.cameraPic = cameraPic;
    }

    /**
     * 获取红外图片
     *
     * @return flir_pic - 红外图片
     */
    public String getFlirPic() {
    		return ProjectUrlConvert.covert2HttpUrl(flirPic,ProjectConsts.PREFIX_RECON);
    }

    /**
     * 设置红外图片
     *
     * @param flirPic 红外图片
     */
    public void setFlirPic(String flirPic) {
        this.flirPic = flirPic;
    }

    /**
     * 获取音视频
     *
     * @return sound - 音视频
     */
    public String getSound() {
        return ProjectUrlConvert.covert2HttpUrl(sound);
    }

    /**
     * 设置音视频
     *
     * @param sound 音视频
     */
    public void setSound(String sound) {
        this.sound = sound;
    }

    /**
     * 获取识别结果
     *
     * @return value - 识别结果
     */
    public Float getValue() {
        return value;
    }

    /**
     * 设置识别结果
     *
     * @param value 识别结果
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     * 获取识别时间
     *
     * @return recon_time - 识别时间
     */
    public Date getReconTime() {
        return reconTime;
    }

    /**
     * 设置识别时间
     *
     * @param reconTime 识别时间
     */
    public void setReconTime(Date reconTime) {
        this.reconTime = reconTime;
    }

    /**
     * 获取审核状态 0:未审核 1:已审核
     *
     * @return check_status - 审核状态 0:未审核 1:已审核
     */
    public Byte getCheckStatus() {
        return checkStatus;
    }

    /**
     * 设置审核状态 0:未审核 1:已审核
     *
     * @param checkStatus 审核状态 0:未审核 1:已审核
     */
    public void setCheckStatus(Byte checkStatus) {
        this.checkStatus = checkStatus;
    }

    /**
     * 获取结果状态 0:识别正确 1:识别错误
     *
     * @return recon_status - 结果状态 0:识别正确 1:识别错误
     */
    public Byte getReconStatus() {
        return reconStatus;
    }

    /**
     * 设置结果状态 0:识别正确 1:识别错误
     *
     * @param reconStatus 结果状态 0:识别正确 1:识别错误
     */
    public void setReconStatus(Byte reconStatus) {
        this.reconStatus = reconStatus;
    }

    /**
     * 获取人工判断结果
     *
     * @return modify_value - 人工判断结果
     */
    public Float getModifyValue() {
        return modifyValue;
    }

    /**
     * 设置人工判断结果
     *
     * @param modifyValue 人工判断结果
     */
    public void setModifyValue(Float modifyValue) {
        this.modifyValue = modifyValue;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public Integer getWatchPointId() {
		return watchPointId;
	}

	public void setWatchPointId(Integer watchPointId) {
		this.watchPointId = watchPointId;
	}

	public Integer getOperaterId() {
		return operaterId;
	}

	public void setOperaterId(Integer operaterId) {
		this.operaterId = operaterId;
	}

	public String getMaxTempPosition() {
		return maxTempPosition;
	}

	public void setMaxTempPosition(String maxTempPosition) {
		this.maxTempPosition = maxTempPosition;
	}
	
}