package com.ydrobot.powerplant.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 基础DO类，提供toString快方法
 * Created by liwei on 2015/6/16.
 */
public class BaseDO implements Serializable {

    private static final long serialVersionUID = -1394589131426860408L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
