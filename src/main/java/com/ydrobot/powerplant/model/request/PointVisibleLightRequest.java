package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class PointVisibleLightRequest {

    @ApiModelProperty(name = "id", value = "主键id，更新数据时需要传递")
    private Integer id;

    @ApiModelProperty(name = "height", value = "可见光目标大小的高度")
    private Float height;

    @ApiModelProperty(name = "width", value = "可见光目标大小的宽度")
    private Float width;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }
}
