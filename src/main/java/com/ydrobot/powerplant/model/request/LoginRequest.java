package com.ydrobot.powerplant.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class LoginRequest {

    @ApiModelProperty(name = "account", value = "账号", required = true)
    @NotEmpty(message = "账号不能为空")
    private String account;

    @ApiModelProperty(name = "password", value = "账号", required = true)
    @NotEmpty(message = "密码不能为空")
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
