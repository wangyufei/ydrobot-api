package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class DeleteExecutePlanRequest {
    @ApiModelProperty(name = "id", value = "要删除的记录id")
    private Integer id;

    @ApiModelProperty(name = "taskType", value = "任务类型 1:历史任务 2:定期任务 3:周期任务 4:间隔任务")
    private Integer taskType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }
}
