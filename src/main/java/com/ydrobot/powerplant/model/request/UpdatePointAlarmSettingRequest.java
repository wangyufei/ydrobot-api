package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class UpdatePointAlarmSettingRequest {

    @ApiModelProperty(name = "pointId", value = "点位id")
    private Integer pointId;

    private List<PointAlarmSettingRequest> pointAlarmSettingRequests;

    public Integer getPointId() {
        return pointId;
    }

    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    public List<PointAlarmSettingRequest> getPointAlarmSettingRequests() {
        return pointAlarmSettingRequests;
    }

    public void setPointAlarmSettingRequests(List<PointAlarmSettingRequest> pointAlarmSettingRequests) {
        this.pointAlarmSettingRequests = pointAlarmSettingRequests;
    }

}
