package com.ydrobot.powerplant.model.request;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class TaskRegularRequest {
    @ApiModelProperty(name = "executeTime", value = "执行时间")
    private Date executeTime;

    public Date getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(Date executeTime) {
        this.executeTime = executeTime;
    }
}
