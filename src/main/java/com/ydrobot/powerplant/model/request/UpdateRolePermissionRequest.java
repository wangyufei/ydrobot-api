package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class UpdateRolePermissionRequest {

    @ApiModelProperty(name = "permissionIds", value = "权限id集合，多个用逗号分隔")
    private String permissionIds;

    public String getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(String permissionIds) {
        this.permissionIds = permissionIds;
    }

}
