package com.ydrobot.powerplant.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class ControlAuthRequest {

    @ApiModelProperty(name = "handleStatus", value = "0：否，1：是", required = true)
    @NotEmpty(message = "操作状态不能为空")
    private String handleStatus;

    public String getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(String handleStatus) {
        this.handleStatus = handleStatus;
    }

}
