package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class WatchPointRequest {

    @ApiModelProperty(name = "name", value = "角色名称")
    private String name;

    @ApiModelProperty(name = "displayName", value = "显示名称")
    private String displayName;

    @ApiModelProperty(name = "permissionIds", value = "权限id集合，多个用逗号分隔")
    private String permissionIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(String permissionIds) {
        this.permissionIds = permissionIds;
    }

}
