package com.ydrobot.powerplant.model.request;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class PointHistorySyncRequest {

    /**
     * 任务执行记录id
     */
    private Integer taskHistoryId;

    /**
     * 机器人id
     */
    private Integer robotId;

    /**
     * 巡检点id
     */
    private Integer pointId;

    /**
     * 可见光图片
     */
    private String cameraPic;

    /**
     * 红外图片
     */
    private String flirPic;

    /**
     * 音视频
     */
    private String sound;

    /**
     * 识别结果
     */
    private Float value;

    /**
     * 识别时间
     */
    private Date reconTime;

    /**
     * 观测位id
     */
    private Integer watchPointId;

    /**
     * 结果状态 0:识别正确 1:识别错误
     */
    private Byte reconStatus;
    
    /**
     * 最大温度位置
     */
    private String maxTempPosition;

    /**
     * 获取任务执行记录id
     *
     * @return task_history_id - 任务执行记录id
     */
    public Integer getTaskHistoryId() {
        return taskHistoryId;
    }

    /**
     * 设置任务执行记录id
     *
     * @param taskHistoryId 任务执行记录id
     */
    public void setTaskHistoryId(Integer taskHistoryId) {
        this.taskHistoryId = taskHistoryId;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取巡检点id
     *
     * @return point_id - 巡检点id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置巡检点id
     *
     * @param pointId 巡检点id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取可见光图片
     *
     * @return camera_pic - 可见光图片
     */
    public String getCameraPic() {
        return cameraPic;
    }

    /**
     * 设置可见光图片
     *
     * @param cameraPic 可见光图片
     */
    public void setCameraPic(String cameraPic) {
    		if (StringUtils.isNotBlank(cameraPic)) {
			cameraPic = "&&"+cameraPic;
		}
        this.cameraPic = cameraPic;
    }

    /**
     * 获取红外图片
     *
     * @return flir_pic - 红外图片
     */
    public String getFlirPic() {
    		return flirPic;
    }

    /**
     * 设置红外图片
     *
     * @param flirPic 红外图片
     */
    public void setFlirPic(String flirPic) {
    		if (StringUtils.isNotBlank(flirPic)) {
			flirPic = "&&"+flirPic;
		}
    	
        this.flirPic = flirPic;
    }

    /**
     * 获取音视频
     *
     * @return sound - 音视频
     */
    public String getSound() {
        return this.sound;
    }

    /**
     * 设置音视频
     *
     * @param sound 音视频
     */
    public void setSound(String sound) {
        this.sound = sound;
    }

    /**
     * 获取识别结果
     *
     * @return value - 识别结果
     */
    public Float getValue() {
        return value;
    }

    /**
     * 设置识别结果
     *
     * @param value 识别结果
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     * 获取识别时间
     *
     * @return recon_time - 识别时间
     */
    public Date getReconTime() {
        return reconTime;
    }

    /**
     * 设置识别时间
     *
     * @param reconTime 识别时间
     */
    public void setReconTime(Date reconTime) {
        this.reconTime = reconTime;
    }

    /**
     * 获取结果状态 0:识别正确 1:识别错误
     *
     * @return recon_status - 结果状态 0:识别正确 1:识别错误
     */
    public Byte getReconStatus() {
        return reconStatus;
    }

    /**
     * 设置结果状态 0:识别正确 1:识别错误
     *
     * @param reconStatus 结果状态 0:识别正确 1:识别错误
     */
    public void setReconStatus(Byte reconStatus) {
        this.reconStatus = reconStatus;
    }

	public Integer getWatchPointId() {
		return watchPointId;
	}

	public void setWatchPointId(Integer watchPointId) {
		this.watchPointId = watchPointId;
	}

	public String getMaxTempPosition() {
		return maxTempPosition;
	}

	public void setMaxTempPosition(String maxTempPosition) {
		this.maxTempPosition = maxTempPosition;
	}
	
}