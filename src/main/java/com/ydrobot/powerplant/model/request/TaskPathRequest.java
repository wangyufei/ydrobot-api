package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class TaskPathRequest {

    @ApiModelProperty(name = "path", value = "任务路径")
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
