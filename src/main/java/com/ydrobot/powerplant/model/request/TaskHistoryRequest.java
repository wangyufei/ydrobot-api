package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class TaskHistoryRequest {

    @ApiModelProperty(name = "checkDesc", value = "审核意见")
    private String checkDesc;

    public String getCheckDesc() {
        return checkDesc;
    }

    public void setCheckDesc(String checkDesc) {
        this.checkDesc = checkDesc;
    }

}
