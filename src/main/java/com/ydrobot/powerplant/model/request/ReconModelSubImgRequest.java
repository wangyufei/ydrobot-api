package com.ydrobot.powerplant.model.request;

import io.swagger.annotations.ApiModelProperty;

public class ReconModelSubImgRequest {

    @ApiModelProperty(name = "reconModelSubId", value = "识别子类模型id")
    private Integer reconModelSubId;

    @ApiModelProperty(name = "picture", value = "图片")
    private String picture;

    public Integer getReconModelSubId() {
        return reconModelSubId;
    }

    public void setReconModelSubId(Integer reconModelSubId) {
        this.reconModelSubId = reconModelSubId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
