package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PointRequest {

    @ApiModelProperty(name = "name", value = "点位名称")
    private String name;

    @ApiModelProperty(name = "displayName", value = "点位显示名称")
    private String displayName;

    @ApiModelProperty(name = "objectId", value = "设备id")
    private Integer objectId;

    @ApiModelProperty(name = "reconTypeId", value = "识别类型id")
    private Integer reconTypeId;

    @ApiModelProperty(name = "meterTypeId", value = "表计类型id")
    private Integer meterTypeId;

    @ApiModelProperty(name = "faceTypeId", value = "外观类型id")
    private Integer faceTypeId;

    @ApiModelProperty(name = "hotTypeId", value = "发热类型id")
    private Integer hotTypeId;

    @ApiModelProperty(name = "saveTypeId", value = "保存类型id")
    private Integer saveTypeId;

    @ApiModelProperty(name = "nis", value = "0:采集 1:计算")
    private Byte nis;

    @ApiModelProperty(name = "isAlarm", value = "0:不报警 1:报警")
    private Byte isAlarm;

    @ApiModelProperty(name = "unit", value = "单位")
    private String unit;

    @ApiModelProperty(name = "coef", value = "系数")
    private Integer coef;

    @ApiModelProperty(name = "isUse", value = "是否启用")
    private Byte isUse;

    @ApiModelProperty(name = "isShow", value = "0:前端不报警显示 1:前端报警显示")
    private Byte isShow;

    @ApiModelProperty(name = "isSave", value = "0:不保存 1:保存")
    private Byte isSave;

    @ApiModelProperty(name = "defaultValue", value = "默认值")
    private Float defaultValue;

    @ApiModelProperty(name = "x", value = "x坐标")
    private Float x;

    @ApiModelProperty(name = "y", value = "y坐标")
    private Float y;

    @ApiModelProperty(name = "z", value = "z坐标")
    private Float z;

    @ApiModelProperty(name = "reconModelSubId", value = "识别模型子类id")
    private Integer reconModelSubId;

    @ApiModelProperty(name = "meterPointInfoRequest", value = "点位表计信息")
    private MeterPointInfoRequest meterPointInfoRequest;

    @ApiModelProperty(name = "pointVisibleLightRequest", value = "点位可见光信息")
    private PointVisibleLightRequest pointVisibleLightRequest;
    
    @ApiModelProperty(name = "watchPointIds", value = "观察点")
    private List<Integer> watchPointIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getReconTypeId() {
        return reconTypeId;
    }

    public void setReconTypeId(Integer reconTypeId) {
        this.reconTypeId = reconTypeId;
    }

    public Integer getMeterTypeId() {
        return meterTypeId;
    }

    public void setMeterTypeId(Integer meterTypeId) {
        this.meterTypeId = meterTypeId;
    }

    public Integer getFaceTypeId() {
        return faceTypeId;
    }

    public void setFaceTypeId(Integer faceTypeId) {
        this.faceTypeId = faceTypeId;
    }

    public Integer getHotTypeId() {
        return hotTypeId;
    }

    public void setHotTypeId(Integer hotTypeId) {
        this.hotTypeId = hotTypeId;
    }

    public Integer getSaveTypeId() {
        return saveTypeId;
    }

    public void setSaveTypeId(Integer saveTypeId) {
        this.saveTypeId = saveTypeId;
    }

    public Byte getNis() {
        return nis;
    }

    public void setNis(Byte nis) {
        this.nis = nis;
    }

    public Byte getIsAlarm() {
        return isAlarm;
    }

    public void setIsAlarm(Byte isAlarm) {
        this.isAlarm = isAlarm;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getCoef() {
        return coef;
    }

    public void setCoef(Integer coef) {
        this.coef = coef;
    }

    public Byte getIsUse() {
        return isUse;
    }

    public void setIsUse(Byte isUse) {
        this.isUse = isUse;
    }

    public Byte getIsShow() {
        return isShow;
    }

    public void setIsShow(Byte isShow) {
        this.isShow = isShow;
    }

    public Byte getIsSave() {
        return isSave;
    }

    public void setIsSave(Byte isSave) {
        this.isSave = isSave;
    }

    public Float getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Float defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getZ() {
        return z;
    }

    public void setZ(Float z) {
        this.z = z;
    }

    public Integer getReconModelSubId() {
        return reconModelSubId;
    }

    public void setReconModelSubId(Integer reconModelSubId) {
        this.reconModelSubId = reconModelSubId;
    }

    public MeterPointInfoRequest getMeterPointInfoRequest() {
        return meterPointInfoRequest;
    }

    public void setMeterPointInfoRequest(MeterPointInfoRequest meterPointInfoRequest) {
        this.meterPointInfoRequest = meterPointInfoRequest;
    }

    public PointVisibleLightRequest getPointVisibleLightRequest() {
        return pointVisibleLightRequest;
    }

    public void setPointVisibleLightRequest(PointVisibleLightRequest pointVisibleLightRequest) {
        this.pointVisibleLightRequest = pointVisibleLightRequest;
    }

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public List<Integer> getWatchPointIds() {
		return watchPointIds;
	}

	public void setWatchPointIds(List<Integer> watchPointIds) {
		this.watchPointIds = watchPointIds;
	}

    
}
