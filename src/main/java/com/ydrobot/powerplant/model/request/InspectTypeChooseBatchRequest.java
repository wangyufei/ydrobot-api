package com.ydrobot.powerplant.model.request;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class InspectTypeChooseBatchRequest {

    @ApiModelProperty(name = "inspect_type_id", value = "巡检类型id")
    private Integer inspectTypeId;

    @ApiModelProperty(name = "inspectTypeChooseRequests", value = "巡检类型选项集合")
    private List<InspectTypeChooseRequest> inspectTypeChooseRequests;

    public Integer getInspectTypeId() {
        return inspectTypeId;
    }

    public void setInspectTypeId(Integer inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

    public List<InspectTypeChooseRequest> getInspectTypeChooseRequests() {
        return inspectTypeChooseRequests;
    }

    public void setInspectTypeChooseRequests(List<InspectTypeChooseRequest> inspectTypeChooseRequests) {
        this.inspectTypeChooseRequests = inspectTypeChooseRequests;
    }

}
