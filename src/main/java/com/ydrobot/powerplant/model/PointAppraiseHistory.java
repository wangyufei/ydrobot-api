package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "point_appraise_history")
public class PointAppraiseHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 部件id
     */
    @Column(name = "point_id")
    private Integer pointId;

    /**
     * 评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     */
    @Column(name = "appraise_status")
    private Integer appraiseStatus;

    /**
     * 评价状态对应记录id
     */
    @Column(name = "appraise_history_id")
    private Integer appraiseHistoryId;

    /**
     * 评价人
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 评价时间
     */
    @Column(name = "appraise_time")
    private Date appraiseTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取部件id
     *
     * @return point_id - 部件id
     */
    public Integer getPointId() {
        return pointId;
    }

    /**
     * 设置部件id
     *
     * @param pointId 部件id
     */
    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     *
     * @return appraise_status - 评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     */
    public Integer getAppraiseStatus() {
        return appraiseStatus;
    }

    /**
     * 设置评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     *
     * @param appraiseStatus 评价状态 0:正常 1:存在缺陷 2:设备出现异常趋势 3:需人工审核
     */
    public void setAppraiseStatus(Integer appraiseStatus) {
        this.appraiseStatus = appraiseStatus;
    }

    /**
     * 获取评价状态对应记录id
     *
     * @return appraise_history_id - 评价状态对应记录id
     */
    public Integer getAppraiseHistoryId() {
        return appraiseHistoryId;
    }

    /**
     * 设置评价状态对应记录id
     *
     * @param appraiseHistoryId 评价状态对应记录id
     */
    public void setAppraiseHistoryId(Integer appraiseHistoryId) {
        this.appraiseHistoryId = appraiseHistoryId;
    }

    /**
     * 获取评价人
     *
     * @return user_name - 评价人
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置评价人
     *
     * @param userName 评价人
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取评价时间
     *
     * @return appraise_time - 评价时间
     */
    public Date getAppraiseTime() {
        return appraiseTime;
    }

    /**
     * 设置评价时间
     *
     * @param appraiseTime 评价时间
     */
    public void setAppraiseTime(Date appraiseTime) {
        this.appraiseTime = appraiseTime;
    }
}