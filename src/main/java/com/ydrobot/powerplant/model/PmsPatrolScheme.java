package com.ydrobot.powerplant.model;

import javax.persistence.*;

@Table(name = "pms_patrol_scheme")
public class PmsPatrolScheme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 编制单位
     */
    @Column(name = "organization_unit")
    private String organizationUnit;

    /**
     * 专业
     */
    private String speciality;

    /**
     * 电站线路
     */
    @Column(name = "power_line")
    private String powerLine;

    /**
     * 电压等级
     */
    @Column(name = "voltage_grade")
    private String voltageGrade;

    /**
     * 是否停电
     */
    @Column(name = "is_power_cut")
    private String isPowerCut;

    /**
     * 停电范围
     */
    @Column(name = "power_cut_range")
    private String powerCutRange;

    /**
     * 工作类型
     */
    @Column(name = "work_type")
    private String workType;

    /**
     * 工作内容
     */
    @Column(name = "work_content")
    private String workContent;

    /**
     * 计划编制时间
     */
    @Column(name = "create_time")
    private String createTime;

    /**
     * 计划开始时间
     */
    @Column(name = "start_time")
    private String startTime;

    /**
     * 计划完成时间
     */
    @Column(name = "end_time")
    private String endTime;

    /**
     * 计划状态
     */
    private String status;

    /**
     * 来源
     */
    private String source;

    /**
     * 调度是否批复
     */
    @Column(name = "is_reply")
    private String isReply;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取编制单位
     *
     * @return organization_unit - 编制单位
     */
    public String getOrganizationUnit() {
        return organizationUnit;
    }

    /**
     * 设置编制单位
     *
     * @param organizationUnit 编制单位
     */
    public void setOrganizationUnit(String organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    /**
     * 获取专业
     *
     * @return speciality - 专业
     */
    public String getSpeciality() {
        return speciality;
    }

    /**
     * 设置专业
     *
     * @param speciality 专业
     */
    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    /**
     * 获取电站线路
     *
     * @return power_line - 电站线路
     */
    public String getPowerLine() {
        return powerLine;
    }

    /**
     * 设置电站线路
     *
     * @param powerLine 电站线路
     */
    public void setPowerLine(String powerLine) {
        this.powerLine = powerLine;
    }

    /**
     * 获取电压等级
     *
     * @return voltage_grade - 电压等级
     */
    public String getVoltageGrade() {
        return voltageGrade;
    }

    /**
     * 设置电压等级
     *
     * @param voltageGrade 电压等级
     */
    public void setVoltageGrade(String voltageGrade) {
        this.voltageGrade = voltageGrade;
    }

    /**
     * 获取是否停电
     *
     * @return is_power_cut - 是否停电
     */
    public String getIsPowerCut() {
        return isPowerCut;
    }

    /**
     * 设置是否停电
     *
     * @param isPowerCut 是否停电
     */
    public void setIsPowerCut(String isPowerCut) {
        this.isPowerCut = isPowerCut;
    }

    /**
     * 获取停电范围
     *
     * @return power_cut_range - 停电范围
     */
    public String getPowerCutRange() {
        return powerCutRange;
    }

    /**
     * 设置停电范围
     *
     * @param powerCutRange 停电范围
     */
    public void setPowerCutRange(String powerCutRange) {
        this.powerCutRange = powerCutRange;
    }

    /**
     * 获取工作类型
     *
     * @return work_type - 工作类型
     */
    public String getWorkType() {
        return workType;
    }

    /**
     * 设置工作类型
     *
     * @param workType 工作类型
     */
    public void setWorkType(String workType) {
        this.workType = workType;
    }

    /**
     * 获取工作内容
     *
     * @return work_content - 工作内容
     */
    public String getWorkContent() {
        return workContent;
    }

    /**
     * 设置工作内容
     *
     * @param workContent 工作内容
     */
    public void setWorkContent(String workContent) {
        this.workContent = workContent;
    }

    /**
     * 获取计划编制时间
     *
     * @return create_time - 计划编制时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置计划编制时间
     *
     * @param createTime 计划编制时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取计划开始时间
     *
     * @return start_time - 计划开始时间
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * 设置计划开始时间
     *
     * @param startTime 计划开始时间
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取计划完成时间
     *
     * @return end_time - 计划完成时间
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * 设置计划完成时间
     *
     * @param endTime 计划完成时间
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取计划状态
     *
     * @return status - 计划状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置计划状态
     *
     * @param status 计划状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取来源
     *
     * @return source - 来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 设置来源
     *
     * @param source 来源
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 获取调度是否批复
     *
     * @return is_reply - 调度是否批复
     */
    public String getIsReply() {
        return isReply;
    }

    /**
     * 设置调度是否批复
     *
     * @param isReply 调度是否批复
     */
    public void setIsReply(String isReply) {
        this.isReply = isReply;
    }
}