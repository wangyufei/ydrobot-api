package com.ydrobot.powerplant.model;

import java.util.Date;
import javax.persistence.*;

public class Task {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 机器人id
     */
    @Column(name = "robot_id")
    private Integer robotId;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 巡检类型id
     */
    @Column(name = "inspect_type_id")
    private Integer inspectTypeId;

    /**
     * 任务描述
     */
    private String description;

    /**
     * 是否更新 0:未更新 1:有更新
     */
    @Column(name = "is_update")
    private Byte isUpdate;

    /**
     * 是否自定义 0:正常 1:自定义
     */
    @Column(name = "is_custom")
    private Byte isCustom;

    /**
     * 编制人
     */
    @Column(name = "operater_id")
    private Integer operaterId;

    /**
     * 是否删除 0:正常 1:已删除
     */
    @Column(name = "is_delete")
    private Byte isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取机器人id
     *
     * @return robot_id - 机器人id
     */
    public Integer getRobotId() {
        return robotId;
    }

    /**
     * 设置机器人id
     *
     * @param robotId 机器人id
     */
    public void setRobotId(Integer robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取任务名称
     *
     * @return name - 任务名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置任务名称
     *
     * @param name 任务名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取巡检类型id
     *
     * @return inspect_type_id - 巡检类型id
     */
    public Integer getInspectTypeId() {
        return inspectTypeId;
    }

    /**
     * 设置巡检类型id
     *
     * @param inspectTypeId 巡检类型id
     */
    public void setInspectTypeId(Integer inspectTypeId) {
        this.inspectTypeId = inspectTypeId;
    }

    /**
     * 获取任务描述
     *
     * @return description - 任务描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置任务描述
     *
     * @param description 任务描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取是否更新 0:未更新 1:有更新
     *
     * @return is_update - 是否更新 0:未更新 1:有更新
     */
    public Byte getIsUpdate() {
        return isUpdate;
    }

    /**
     * 设置是否更新 0:未更新 1:有更新
     *
     * @param isUpdate 是否更新 0:未更新 1:有更新
     */
    public void setIsUpdate(Byte isUpdate) {
        this.isUpdate = isUpdate;
    }

    /**
     * 获取是否自定义 0:正常 1:自定义
     *
     * @return is_custom - 是否自定义 0:正常 1:自定义
     */
    public Byte getIsCustom() {
        return isCustom;
    }

    /**
     * 设置是否自定义 0:正常 1:自定义
     *
     * @param isCustom 是否自定义 0:正常 1:自定义
     */
    public void setIsCustom(Byte isCustom) {
        this.isCustom = isCustom;
    }

    /**
     * 获取编制人
     *
     * @return operater_id - 编制人
     */
    public Integer getOperaterId() {
        return operaterId;
    }

    /**
     * 设置编制人
     *
     * @param operaterId 编制人
     */
    public void setOperaterId(Integer operaterId) {
        this.operaterId = operaterId;
    }

    /**
     * 获取是否删除 0:正常 1:已删除
     *
     * @return is_delete - 是否删除 0:正常 1:已删除
     */
    public Byte getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除 0:正常 1:已删除
     *
     * @param isDelete 是否删除 0:正常 1:已删除
     */
    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}