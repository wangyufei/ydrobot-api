package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.Device;
import com.ydrobot.powerplant.model.OperationLog;
import com.ydrobot.powerplant.model.User;

public class OperationLogResult extends OperationLog{

	/**
	 * 变电站
	 */
	private Device device;
	
	/**
	 * 用户
	 */
	private User user;

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
