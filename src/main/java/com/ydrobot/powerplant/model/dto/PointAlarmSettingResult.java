package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.model.PointAlarmSetting;

public class PointAlarmSettingResult extends PointAlarmSetting{

	private AlarmType alarmType;

	public AlarmType getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(AlarmType alarmType) {
		this.alarmType = alarmType;
	}
}
