package com.ydrobot.powerplant.model.dto;

import java.util.List;

import com.ydrobot.powerplant.model.ReconModel;
import com.ydrobot.powerplant.model.ReconModelSub;

public class ReconModelResult extends ReconModel{

	private List<ReconModelSub> reconModelSubs;

	public List<ReconModelSub> getReconModelSubs() {
		return reconModelSubs;
	}

	public void setReconModelSubs(List<ReconModelSub> reconModelSubs) {
		this.reconModelSubs = reconModelSubs;
	}
	
}
