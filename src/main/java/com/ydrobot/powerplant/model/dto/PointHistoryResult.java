package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.ReconType;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.Task;
import com.ydrobot.powerplant.model.User;

public class PointHistoryResult extends PointHistory {
	
	/**
	 * 对象信息
	 */
	private TObject object;
	
	/**
	 * 部件信息
	 */
	private Point point;
	
	/**
	 * 识别类型
	 */
	private ReconType reconType;
	
	/**
	 * 任务信息
	 */
	private Task task;
	
	/**
	 * 用户信息
	 */
	private User user;
	
	/**
	 * 评价状态
	 */
	private Integer appraiseStatus;
	
	/**
	 * 二次确认任务执行记录id
	 */
	private Integer twiceConfirmTaskHistoryId;
	
	/**
	 * 二次确认任务id
	 */
	private Integer twiceConfirmTaskId;

	public TObject getObject() {
		return object;
	}

	public void setObject(TObject object) {
		this.object = object;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public ReconType getReconType() {
		return reconType;
	}

	public void setReconType(ReconType reconType) {
		this.reconType = reconType;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getAppraiseStatus() {
		return appraiseStatus;
	}

	public void setAppraiseStatus(Integer appraiseStatus) {
		this.appraiseStatus = appraiseStatus;
	}

	public Integer getTwiceConfirmTaskHistoryId() {
		return twiceConfirmTaskHistoryId;
	}

	public void setTwiceConfirmTaskHistoryId(Integer twiceConfirmTaskHistoryId) {
		this.twiceConfirmTaskHistoryId = twiceConfirmTaskHistoryId;
	}

	public Integer getTwiceConfirmTaskId() {
		return twiceConfirmTaskId;
	}

	public void setTwiceConfirmTaskId(Integer twiceConfirmTaskId) {
		this.twiceConfirmTaskId = twiceConfirmTaskId;
	}
	

}
