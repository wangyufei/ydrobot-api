package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.SaveType;

public class PointHistoryVerificationReportResult extends PointHistory {
	
	private AlarmType alarmType;
	private Integer alarmLevel;
	private SaveType saveType;
	private Point point;

	public SaveType getSaveType() {
		return saveType;
	}

	public void setSaveType(SaveType saveType) {
		this.saveType = saveType;
	}

	public AlarmType getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(AlarmType alarmType) {
		this.alarmType = alarmType;
	}

	public Integer getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(Integer alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}
}
