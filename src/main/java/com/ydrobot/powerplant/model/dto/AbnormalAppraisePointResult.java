package com.ydrobot.powerplant.model.dto;

import java.util.Date;

public class AbnormalAppraisePointResult {
	
	/**
	 * 对象名称
	 */
	private String objectName;
	
	/**
	 * 部件id
	 */
	private Integer pointId;
	
	/**
	 * 部件名称
	 */
	private String pointName;
	
	/**
	 * 评价状态
	 */
	private Integer appraiseStatus;
	
	/**
	 * 评价时间
	 */
	private Date appraiseTime;
	
	/**
	 * 评价记录id
	 */
	private Integer appraiseHistoryId;
	
	/**
	 * 对象id
	 */
	private Integer objectId;
	
	/**
	 * 任务记录id
	 */
	private Integer taskHistoryId;
	
	/**
	 * 最大温度位置
	 */
	private String maxTempPosition;
	
	/**
	 * 值
	 */
	private Float value;
	

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public Integer getPointId() {
		return pointId;
	}

	public void setPointId(Integer pointId) {
		this.pointId = pointId;
	}

	public String getPointName() {
		return pointName;
	}

	public void setPointName(String pointName) {
		this.pointName = pointName;
	}

	public Integer getAppraiseStatus() {
		return appraiseStatus;
	}

	public void setAppraiseStatus(Integer appraiseStatus) {
		this.appraiseStatus = appraiseStatus;
	}

	public Integer getAppraiseHistoryId() {
		return appraiseHistoryId;
	}

	public void setAppraiseHistoryId(Integer appraiseHistoryId) {
		this.appraiseHistoryId = appraiseHistoryId;
	}

	public Date getAppraiseTime() {
		return appraiseTime;
	}

	public void setAppraiseTime(Date appraiseTime) {
		this.appraiseTime = appraiseTime;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public Integer getTaskHistoryId() {
		return taskHistoryId;
	}

	public void setTaskHistoryId(Integer taskHistoryId) {
		this.taskHistoryId = taskHistoryId;
	}

	public String getMaxTempPosition() {
		return maxTempPosition;
	}

	public void setMaxTempPosition(String maxTempPosition) {
		this.maxTempPosition = maxTempPosition;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}
}
