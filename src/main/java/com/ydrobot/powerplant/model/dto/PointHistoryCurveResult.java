package com.ydrobot.powerplant.model.dto;

public class PointHistoryCurveResult {
	public String name;
	public Float value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

}
