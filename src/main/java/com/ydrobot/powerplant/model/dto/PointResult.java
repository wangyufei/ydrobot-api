package com.ydrobot.powerplant.model.dto;

import java.util.List;

import com.ydrobot.powerplant.model.MeterPointInfo;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointVisibleLight;
import com.ydrobot.powerplant.model.ReconModelSub;
import com.ydrobot.powerplant.model.WatchPoint;

public class PointResult extends Point{
    
	/**
	 * 点位表计信息
	 */
	private MeterPointInfo meterPointInfo;
	
	/**
	 * 点位可见光信息
	 */
	private PointVisibleLight pointVisibleLight;
	
	/**
	 * 识别模型子类信息
	 */
	private ReconModelSub reconModelSub;
	
	/**
	 * 点位观察点列表
	 */
	private List<WatchPoint> watchPoints;

	public MeterPointInfo getMeterPointInfo() {
		return meterPointInfo;
	}

	public void setMeterPointInfo(MeterPointInfo meterPointInfo) {
		this.meterPointInfo = meterPointInfo;
	}

	public PointVisibleLight getPointVisibleLight() {
		return pointVisibleLight;
	}

	public void setPointVisibleLight(PointVisibleLight pointVisibleLight) {
		this.pointVisibleLight = pointVisibleLight;
	}

	public ReconModelSub getReconModelSub() {
		return reconModelSub;
	}

	public void setReconModelSub(ReconModelSub reconModelSub) {
		this.reconModelSub = reconModelSub;
	}

	public List<WatchPoint> getWatchPoints() {
		return watchPoints;
	}

	public void setWatchPoints(List<WatchPoint> watchPoints) {
		this.watchPoints = watchPoints;
	}
	
}
