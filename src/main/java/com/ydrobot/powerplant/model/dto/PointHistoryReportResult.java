package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.model.FaceType;
import com.ydrobot.powerplant.model.HotType;
import com.ydrobot.powerplant.model.MeterType;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.ReconType;
import com.ydrobot.powerplant.model.SaveType;
import com.ydrobot.powerplant.model.TaskHistory;

public class PointHistoryReportResult extends PointHistory {
	
	private Point point;
	private ReconType reconType;
	private SaveType saveType;
	private AlarmType alarmType;
	private Integer alarmTypeId;
	private Integer alarmLevel;
	private TaskHistory taskHistory;
	private MeterType meterType;
	private FaceType faceType;
	private HotType hotType;
	private String deviceArea;
	private String interval;
	private String deviceName;
	private String deviceType;


	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public ReconType getReconType() {
		return reconType;
	}

	public void setReconType(ReconType reconType) {
		this.reconType = reconType;
	}

	public SaveType getSaveType() {
		return saveType;
	}

	public void setSaveType(SaveType saveType) {
		this.saveType = saveType;
	}

	public AlarmType getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(AlarmType alarmType) {
		this.alarmType = alarmType;
	}

	public TaskHistory getTaskHistory() {
		return taskHistory;
	}

	public void setTaskHistory(TaskHistory taskHistory) {
		this.taskHistory = taskHistory;
	}

	public MeterType getMeterType() {
		return meterType;
	}

	public void setMeterType(MeterType meterType) {
		this.meterType = meterType;
	}

	public FaceType getFaceType() {
		return faceType;
	}

	public void setFaceType(FaceType faceType) {
		this.faceType = faceType;
	}

	public HotType getHotType() {
		return hotType;
	}

	public void setHotType(HotType hotType) {
		this.hotType = hotType;
	}

	public String getDeviceArea() {
		return deviceArea;
	}

	public void setDeviceArea(String deviceArea) {
		this.deviceArea = deviceArea;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Integer getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(Integer alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public Integer getAlarmTypeId() {
		return alarmTypeId;
	}

	public void setAlarmTypeId(Integer alarmTypeId) {
		this.alarmTypeId = alarmTypeId;
	}

	
}
