package com.ydrobot.powerplant.model.dto;

public class RouteWithPosResult {
	private Integer routeId;
	private Float startX;
	private Float startY;
	private Float endX;
	private Float endY;

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public Float getStartX() {
		return startX;
	}

	public void setStartX(Float startX) {
		this.startX = startX;
	}

	public Float getStartY() {
		return startY;
	}

	public void setStartY(Float startY) {
		this.startY = startY;
	}

	public Float getEndX() {
		return endX;
	}

	public void setEndX(Float endX) {
		this.endX = endX;
	}

	public Float getEndY() {
		return endY;
	}

	public void setEndY(Float endY) {
		this.endY = endY;
	}

}
