package com.ydrobot.powerplant.model.dto;

import java.util.Date;

public class ObjectPatrolInfoResult {
	
	/**
	 * 对象id
	 */
	private Integer objectId;
	
	/**
	 * 对象名称
	 */
	private String objectName;
	
	/**
	 * 对象评价状态
	 */
	private Integer objectAppraiseStatus;
	
	/**
	 * 告警数
	 */
	private Integer alarmNum;
	
	/**
	 * 巡检时间
	 */
	private Date patrolTime;
	

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public Date getPatrolTime() {
		return patrolTime;
	}

	public void setPatrolTime(Date patrolTime) {
		this.patrolTime = patrolTime;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public Integer getObjectAppraiseStatus() {
		return objectAppraiseStatus;
	}

	public void setObjectAppraiseStatus(Integer objectAppraiseStatus) {
		this.objectAppraiseStatus = objectAppraiseStatus;
	}

	public Integer getAlarmNum() {
		return alarmNum;
	}

	public void setAlarmNum(Integer alarmNum) {
		this.alarmNum = alarmNum;
	}
}
