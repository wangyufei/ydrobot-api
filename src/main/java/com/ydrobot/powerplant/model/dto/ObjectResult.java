package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.ObjectType;
import com.ydrobot.powerplant.model.TObject;

public class ObjectResult extends TObject{
	
	/**
	 * 对象类型
	 */
	private ObjectType objectType;

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}
	
	
}
