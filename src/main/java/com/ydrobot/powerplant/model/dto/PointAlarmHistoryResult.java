package com.ydrobot.powerplant.model.dto;

import com.ydrobot.powerplant.model.AlarmType;
import com.ydrobot.powerplant.model.Point;
import com.ydrobot.powerplant.model.PointAlarmHistory;
import com.ydrobot.powerplant.model.PointHistory;
import com.ydrobot.powerplant.model.ReconType;
import com.ydrobot.powerplant.model.SaveType;
import com.ydrobot.powerplant.model.TObject;
import com.ydrobot.powerplant.model.Task;

public class PointAlarmHistoryResult extends PointAlarmHistory {

	/**
	 * 对象
	 */
	private TObject object;
	
	/**
	 * 点位记录
	 */
	private PointHistory pointHistory;
	
	/**
	 * 点位信息
	 */
	private Point point;
	
	/**
	 * 识别类型
	 */
	private ReconType reconType;
	
	/**
	 * 保存类型
	 */
	private SaveType saveType;
	
	/**
	 * 任务信息
	 */
	private Task task;
	
	/**
	 * 报警类型
	 */
	private AlarmType alarmType;
	
	/**
	 * 评价状态
	 */
	private Integer appraiseStatus;
	
	/**
	 * 二次确认任务执行记录id
	 */
	private Integer twiceConfirmTaskHistoryId;
	
	/**
	 * 二次确认任务id
	 */
	private Integer twiceConfirmTaskId;

	public TObject getObject() {
		return object;
	}

	public void setObject(TObject object) {
		this.object = object;
	}

	public PointHistory getPointHistory() {
		return pointHistory;
	}

	public void setPointHistory(PointHistory pointHistory) {
		this.pointHistory = pointHistory;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public ReconType getReconType() {
		return reconType;
	}

	public void setReconType(ReconType reconType) {
		this.reconType = reconType;
	}

	public SaveType getSaveType() {
		return saveType;
	}

	public void setSaveType(SaveType saveType) {
		this.saveType = saveType;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public AlarmType getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(AlarmType alarmType) {
		this.alarmType = alarmType;
	}

	public Integer getAppraiseStatus() {
		return appraiseStatus;
	}

	public void setAppraiseStatus(Integer appraiseStatus) {
		this.appraiseStatus = appraiseStatus;
	}

	public Integer getTwiceConfirmTaskHistoryId() {
		return twiceConfirmTaskHistoryId;
	}

	public void setTwiceConfirmTaskHistoryId(Integer twiceConfirmTaskHistoryId) {
		this.twiceConfirmTaskHistoryId = twiceConfirmTaskHistoryId;
	}

	public Integer getTwiceConfirmTaskId() {
		return twiceConfirmTaskId;
	}

	public void setTwiceConfirmTaskId(Integer twiceConfirmTaskId) {
		this.twiceConfirmTaskId = twiceConfirmTaskId;
	}
}
