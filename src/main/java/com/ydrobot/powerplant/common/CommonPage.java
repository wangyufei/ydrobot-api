package com.ydrobot.powerplant.common;

import java.util.List;

import com.github.pagehelper.PageInfo;

public class CommonPage<T> {
	private Long total;
	private List<T> list;

	/**
	 * 将SpringData分页后的list转为分页信息
	 */
	public static <T> CommonPage<T> restPage(PageInfo<T> pageInfo) {
		CommonPage<T> result = new CommonPage<T>();
		result.setTotal(pageInfo.getTotal());
		result.setList(pageInfo.getList());
		return result;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

}
