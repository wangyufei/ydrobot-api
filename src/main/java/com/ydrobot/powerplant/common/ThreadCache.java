package com.ydrobot.powerplant.common;

import com.ydrobot.powerplant.model.User;

/**
 * Created by Wyf on 2019/04/12.
 */
public class ThreadCache {
	private static final ThreadLocal<User> userThread = new ThreadLocal<>();

	public static void setUser(User user) {
		userThread.set(user);
	}

	public static User getUser() {
		return userThread.get();
	}
}
