# 易达图灵后台接口部署文档

## 环境搭建

### 操作系统
ubuntu16.04


### 开发环境

工具 | 版本号 |
----|----|
JDK | 1.8 |
Mysql | 5.7 | 
Redis | 3.0.6 |
tomcat | 8.5.42 |


### 环境搭建

第一步：更新软件源 


```
apt-get update
```


第二步：安装jdk 


```
apt-get install -y openjdk-8-jdk
```

第三步：安装mysql

```
apt-get install -y mysql-server
```

第四步：安装redis

```
apt-get install -y redis-server
```

修改redis.conf配置文件

```
requirepass foobared
改为：
requirepass skyware   指定密码skyware
```

保存后重启redis就可以了

第五步：下载tomcat


```
https://mirror.bit.edu.cn/apache/tomcat/tomcat-8/v8.5.53/bin/apache-tomcat-8.5.53.zip

将下载的tomcat解压放到电脑中

进入到tomcat文件中的bin文件下，执行./startup.sh命令

然后在浏览器中访问 http://127.0.0.1:8080 如果出现tomcat的界面则安装成功！
```
tomcat 资源目录映射配置

进入到tomcat文件中的conf目录下，打开server.xml，在</Host>上方加上如下配置：

```
<Context docBase="/opt" path="/ydrobot" reloadablee
="true" crossContext="true"></Context>
```

docBase是指图片资源所在路径，path指该项目访问的路径


#### Eclipse

1.克隆项目到本地 

```
git clone git@gitlab.com:liezu/ydrobot-api.git
```

2.导入项目

File->import->Maven->Existing Maven Projects

3.配置文件说明

```
application-dev.properties（开发环境）
application-test.properties（测试环境）
application-pro.properties（生产环境）
```

---


```
#mysql
spring.datasource.url=jdbc:mysql://192.168.1.110:3306/ydrobot?useUnicode=true&characterEncoding=UTF-8&useSSL=false
spring.datasource.username=ydrobot
spring.datasource.password=123456
spring.datasource.driver-class-name=com.mysql.jdbc.Driver

#redis
spring.redis.url=redis://:skyware@192.168.1.110:6379/3
spring.redis.pool.max-active=8
spring.redis.pool.max-wait=-1
spring.redis.pool.max-idle=500
spring.redis.pool.min-idle=0
spring.redis.timeout=10000

#文件资源地址配置
web.domain=http://192.168.1.110:8080/ydrobot/

#上传文件最大大小2M
file.max=2
#上传文件后缀限制
file.exts=.jpg,.jpeg,.png,.gif
#上传文件目录配置
file.path=/home/ydrobot/resources/
```

5.项目打包

右键项目->Export->WAR file

6.部署

将打包好的war包放到tomcat->webapps目录下，并修改包名为ydrobot-api.war，然后启动项目即可。
